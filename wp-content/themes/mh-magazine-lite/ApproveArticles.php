<?php /* Template Name: ApproveArticles */ ?>
<?php 
	include_once("analyticstracking.php");
	get_header(); 

?>
<div class="article-wrapper clearfix" id="article-wrapper" >
	
	<nav class="main-nav" style="width:85%;margin-left:7%;">
		<ul class="nav">
			
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=94">Approve Articles</a></li>
			
			<button type="button" onclick="approve()" name="approve" id="approve" style="float:right;">Approve</button>

		</ul>
	
	</nav>
	
	<aside class="sidebar article-sb-left left" id="sidebar-left" style="margin-top:5px;">
		<h4 style="margin-left:75px;padding:5px;"><i>The Africa Report</i></h4>
		<div id="sidebar-left-articles">
		
			<!-- start feedwind code -->
			<script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');
			</script>
			
			<script type="text/javascript">(function() {var params = {rssmikle_url: "http://www.theafricareport.com/index.php?option=com_obrss&task=feed&id=10?option=com_obrss&task=feed&id=10",rssmikle_frame_width: "300",rssmikle_frame_height: "600",frame_height_by_article: "0",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "14",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "on",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "15",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "",rssmikle_title_link: "",rssmikle_title_bgcolor: "#800000",rssmikle_title_color: "#FFFFFF",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#E66432",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M:%S %p",item_description_style: "text+tn",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();</script><!-- end feedwind code -->
			
			<div style="font-size:10px; text-align:center; width:300px;">
			<a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a><!--Please display the above link in your web page according to Terms of Service.-->
			</div>
		
		</div>
		
	</aside>
	
	
	<form class="news-content" id="news-content">
	
		<div class="box alert" id="noarticles">
			<p id="no-articles" >No un-submitted articles</p>
		</div>
		
		<?php if (isset($_SESSION["post-response"])) : ?>
			<div class="saveinfo" id="saveinfo" > <p id = "response"><?php echo $_SESSION["post-response"];
				unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
			</div>
		<?php endif; ?>
	
	</form>
				
	<script type="text/javascript">
	
		var articles = [new Array(),0,0,0];
		var countarticles = 0;
		var homeurl = "<?php echo home_url() ; ?>";
		
		/*Position: Description
		0: the projects we have shown in the index page from the array
		1: the column we are on
		2: the position of pictures in column
		category: the category of array currently being held
		*/
		
		//This function is the one below. For adverts more parameters are needed.
		getNews("yes","no","no","no","yes","no","backend/getarticles.php");
		
		//put adverts on the page
		getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
		
		
		function getNews(getsubmitted,getapproved,getcompanies,getcategory,getarticles,company,url) {
			
			(function (getsubmitted,getapproved,getcompanies,getcategory,getarticles,company,url) {$.ajax({
				type: "POST",
				url: url,
				
				data: {submitted: getsubmitted,approved: getapproved,companies:getcompanies,category:getcategory,iscompany:company}, 
				success: function (obj, textstatus) { 
					
					parsed = jQuery.parseJSON(obj);
				   
					articles[0] = parsed['articles'];
					fillnews(articles[0],articles[1],articles[2],articles[3],"news-content",getarticles,'backend/getArticle.php');
				},
				
				error: function(xhr, textStatus, error){
					  alert("here " +textStatus);
				}
			}); } (getsubmitted,getapproved,getcompanies,getcategory,getarticles,company,url));
			
		}
		
		function fillnews(projects,projectsposition,columnnumber,columnposition,listing,getarticles,url){
			var i = 0;
			var iterate = Math.min(projects.length,10);
			
			if (iterate > 0){
				document.getElementById('noarticles').style.display ="none";
			}
			
			for (i=0;i<iterate;i++){
				
				(function (i,url,getarticles,iterate) {$.ajax({
					type: "POST",
					url: url,
					data: {projectname: projects[projectsposition],articlename: projects[projectsposition]},

					success: function (obj, textstatus) { 
						  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
						 
						loadnews(parsed,listing,iterate);
						  
						   
					},
					
					error: function(xhr, textStatus, error){
						  alert("the err "+error);
					}
				});
				}(i,url,getarticles,iterate));
				
				projectsposition++;
			}
		}
		
		function loadnews(parsed,listing,iterate){
			var articlename = parsed['article'][0]['articlename'];
			var articlecategory = parsed['article'][0]['articlecategory'];
			var articletag = parsed['article'][0]['articletag'];
			var author = parsed['article'][0]['author'];
			var article = $("<p>"+parsed['article'][0]['article']+"</p>").text(); //remove html tags from the article
			var submitteddate = parsed['article'][0]['submitteddate'];
			
			var mainimg = "newsarticles/"+articlename+"/images/mainimage";
			var images = new Array();
			var count = 0;
			var fileextension = [".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF"];
			
			
			$.ajax({ 
				//This will retrieve the contents of the folder
				url: mainimg,
				success: function getimages(data) {
					
					$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
						var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
						var filename = mainimg + "/" + filename;
						images[count] = new Image();
						images[count].src = filename;
						
						count++;
					});
					
					putimage(parsed);
					
				}
			});
			
			
			function putimage(parsed) {
						
				//create the a element to put in
				var newsdiv = document.createElement("div");
				newsdiv.setAttribute("style","position:relative;display: inline-block;");
				newsdiv.setAttribute("class","news-article");
				newsdiv.setAttribute("name","div-"+articlename);
				
				//************* the approve button *****************
				var approvebtn = document.createElement("button");
				approvebtn.setAttribute("class","img-main");
				approvebtn.setAttribute("name","approve-"+articlename);
				approvebtn.setAttribute("type","button");
				approvebtn.setAttribute("title","approve this article");
				approvebtn.innerHTML = "A";
				
				approvebtn.setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
				
				approvebtn.onclick = function (event) {
					var filename = event.target.name;
					var textname = filename.substr(filename.indexOf('-')+1,filename.length);
					
					//make it blue background and remove the recent one
					var approve = document.getElementsByName(filename);
					approve[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;background-color:#0000FF");
					
					//put the article to be approved in the array
					var input = document.createElement("input");
					input.setAttribute("name","approvearticles[]")
					input.style.display = "none";
					
			
					input.value = textname;
					document.getElementById("news-content").appendChild(input);
					
				}
				
				//***************** end approve ****************************
				
				var articleimg = document.createElement("a");
				articleimg.setAttribute("class","article-img");
				articleimg.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename);
				
				if (images[0])
					articleimg.appendChild(images[0]);
				
				var articlenews = document.createElement("a");
				articlenews.setAttribute("class","article-news");
				articlenews.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename);
				
				var title =  document.createElement("h4");
				title.innerHTML = articletag;
				articlenews.appendChild(title);
				
				var articlesnip = document.createElement("p");
				articlesnip.setAttribute("class","article-snip");
				articlesnip.innerHTML = article;
				articlenews.appendChild(articlesnip);
				
				newsdiv.appendChild(articleimg);
				newsdiv.appendChild(articlenews);
				newsdiv.appendChild(approvebtn);
				
				document.getElementById(listing).appendChild(newsdiv);
					
			}
			
		
		}
		
		function approve(){
			var parsed;
			var form = document.getElementById('news-content');
			var formData = new FormData(form);
			
			
			$.ajax({
				type: "POST",
				url: './backend/approveArticles.php',
				data: formData,
				 async: false,
				success: function (obj, textstatus) { 
					  alert("article taken "+obj);
					  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
					  showresponse(parsed);
					  
					   
				},
				cache: false,
				contentType: false,
				processData: false				
			});
			
			function showresponse(parsed){
				alert('response');
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {response:parsed["message"]},
					async: false,
					success: function (obj, textstatus) {
						alert(obj);
						
						location.reload();
					},
					
				});
			}
			
		}
		
		function removealert(){
			document.getElementById("response").style.display = "none";
		}
	
	</script>
	
	<aside class="sidebar article-sb-right" id="sidebar-right" style="margin-top:10px;">
		<h4 style="margin-left:20px;"><i>Featured Companies </i></h4>
		<div id="sidebar-right-companies"> </div>
		
	</aside>
	
	
	
</div>


<?php get_footer(); ?>

<?php 
//get the contents and put them in the file
	//file_put_contents('./newsarticles/'.$_GET['article'].'/'.$_GET['article'].'.html',ob_get_contents());
?>