<?php /* Template Name: HITProfile */ ?>
<?php 
include_once("analyticstracking.php");
get_header(); ?>
<div class="wrapper clearfix">
	<nav class="main-nav">
		<ul class="nav">
			
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=86">Create your profile</a></li>
			<li class=""><a href = "http://www.Afroglobalinvestors.com/?page_id=88">Create your project</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=90">Create your company</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=92">Write an article</a></li>

		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget how-it-works">
			<h4>Follow these basic steps in creating your profile</h4>
			
			<div class="steps">
				<div class="step-number"><span>1</span></div>
				<p>Signup and submit your user details</p>
				<img src="./images/signup.PNG"></img>
			</div>
			
			<div class="steps">
				<div class="step-number"><span>2</span></div>
				<p>Fill in and <strong style="color:#800000">save</strong> your profile details</p>
				<img src="./images/myprofile.PNG"></img>
			</div>
			
			<div class="steps">
				<div class="step-number"><span>3</span></div>
				<p> <strong style="color:#800000">Save and View</strong> your profile page</p>
				<img src="./images/viewncreate.PNG"></img>
			</div>
		
		<script type="text/javascript">				
		</script>

				
	</section>
	
	<aside class="sidebar sb-right">
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:40px" >Username and email</h4>
			<ul class="nav">
				<li>The username and email must both be unique <br><a style="color:#800000;font-weight:bold"> *NB The username cannot be changed.</a></br></li>
				<li>An availability box will show you if these are available</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="">Password</h4>
			<ul class="nav">
				<li>The strength of your passeword is shown by the bar under Password input</li>
				<li>Your password should have a strength of good at the least</li>
				<li>It should also be 6 characters in length</li>
				<li>For a strong password do no repeat characters successively and use a mix of special characters, numbers and cases</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="" >Profile details</h4>
			<ul class="nav">
				<li>This is the information funders will see on your profile page</li>
				<li>Explain in greater detail what you are all about</li>
				<li>Choose a nice picture for your profile and <strong style="color:#800000;">VIEW IT</strong> to make sure its got the right feel and look</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="">Save and View</h4>
			<ul class="nav">
				<li>Do not forget to first save all your changes</li>
				<li>The responses with the <br> <strong class="saveinfo"><p style="margin-top:1px;">brown background</p></strong>  will tell you if your changes were saved or something needs to be corrected</li>
			</ul>
		</div>
		
	</aside>
	
	
</div>
<?php get_footer(); ?>