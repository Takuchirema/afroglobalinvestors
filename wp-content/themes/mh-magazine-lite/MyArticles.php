<?php /* Template Name: Myarticles */ ?>
<?php get_header(); ?>

<?php if (login_check($mysqli) == true) : ?>

<div class="wrapper clearfix" id="wrapper">
	<nav class="main-nav" style="width:1050px;">
		<ul class="nav">
		
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=58">My Profile</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=38">My Projects</a></li>
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=75">My Articles</a></li>
			
			<button name="save" id="save" onclick="createarticle()" style="float:right;">Create new article</button>

		</ul>
	
	</nav>
	
	<aside class="sidebar projects-sb-left left" id="sidebar-left">
		<h4 style="margin-bottom:10px;" ><i>Latest African Markets </i></h4>
		
		<!-- start feedwind code -->
		<script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');
		</script>
		
		<script type="text/javascript">(function() {var params = {rssmikle_url: "http://feeds.feedburner.com/africanmarkets/business",rssmikle_frame_width: "300",rssmikle_frame_height: "800",frame_height_by_article: "0",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "14",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "on",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "20",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "",rssmikle_title_link: "",rssmikle_title_bgcolor: "#800000",rssmikle_title_color: "#FFFFFF",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#E66432",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M:%S %p",item_description_style: "text+tn",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();
		</script>
				
		<div style="font-size:10px; text-align:center; width:300px;">
		<a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a><!--Please display the above link in your web page according to Terms of Service.-->
		</div><!-- end feedwind code -->
		
	</aside>
			
	<section class="articles-container" id="articles-container">
	
			<div class="box alert" id="noarticles">
				<p id="no-articles" >No articles</p>
			</div>
			
		
		<script type="text/javascript">
			var homeurl = "<?php echo home_url() ; ?>";
			
			var username = "<?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} else{echo "" ;} ?>";
			
			var parsed;
			var images = new Array();
			var countarticles = 0;
			
			getMyArticles();
			
			function getMyArticles(){
				//take the articles the user has created
				$.ajax({
					type: "POST",
					url: 'backend/getMyArticles.php',
					data: {username: username,approved:'no'},

					success: function (obj, textstatus) { 
						  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
						  fillarticles();
						  
						  
					},
					
					error: function(xhr, textStatus, error){
						  alert("the err "+error);
					}
				});
			}
			
			//after this is complete loop through the parsed and get the articles
			
			 function fillarticles(){
				if (parsed['success'] == 1){
					document.getElementById("no-articles").innerHTML = ""; 
				}else{
					getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
				}
				
				for (i=0; i<parsed['articles'].length; i++)
				{	
					getImage(parsed['articles'][i]['articlename'],parsed['articles'][i]['company'],i);
					
				}
				
				
			}
		  
			
			function getImage(articlename,company,index)
			{
				var dir = "newsarticles/"+articlename+"/images/mainimage";
				var fileextension = [".jpg",".png"];
				var count = 0;
				
				$.ajax({
					//This will retrieve the contents of the folder if the folder is configured as 'browsable'
					url: dir,
					success: function getimages(data) {
						
						$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
							var filename = dir + "/" + filename;
							images[count] = new Image();
							images[count].src = filename;
							count++;
					});
	 
						finished(index,articlename,company);
					},
					error: function showPorject(data) {
						finished(index,articlename);
					}
					
				});
				
				function finished(index,articlename,company){
					//then do all required to show the article
					
					//create the a element to put the thingsy in 
					var a = document.createElement("a");
					a.setAttribute("class","blog-list-project wow fadeInDown list-project");
					a.setAttribute("data-wow-delay","0.5s");
					a.setAttribute("style","-webkit-animation-delay: 0.5s; -webkit-animation-name: none; padding-top: 1cm");
					a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename);
					
					//create the img div
					var imgDiv = document.createElement("div");
					imgDiv.setAttribute("class","blog-image");
					
					if (images[0])
						imgDiv.appendChild(images[0]);
					
					//create the text div
					var txtDiv = document.createElement("div");
					txtDiv.setAttribute("class","blog-excerpt");
					 
					txtDiv.innerHTML = "<h3> "+parsed['articles'][index]['articlename']+"</h3> <h4 class=\"posted-date\"><i class=\"fa fa-calendar\"></i>"+parsed['articles'][index]['submitteddate']+"</h4> "+parsed['articles'][index]['articletag']+ "<br><a class=\"blogoption\" href=\"http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename+"\">Read More&nbsp;&nbsp;<i class=\"fa fa-angle-right\"></i> </a> <br> <a class=\"blogoption\" href=\"#\" style=\"margin-right:110px\"  onclick = \"editarticle('"+articlename+"')\" >Edit&nbsp;&nbsp;<i class=\"fa fa-angle-right\"></i></a>";
					
					//then put everything together
					a.appendChild(imgDiv);
					a.appendChild(txtDiv);
					
					document.getElementById("articles-container").appendChild(a);
					
					if (countarticles == parsed['articles'].length - 1){
						//get sidebar after the getting of projects
						getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
					}else{countarticles++;}
					
					//reset images
					images = new Array();
					
				}
			}
			
			function editarticle(articlename){
				
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"article",name:articlename},
					async: false,
					success: function (obj, textstatus) {
						location.href='http://www.Afroglobalinvestors.com/?page_id=73';
					
					},
					
				});
		
			}
			
			function createarticle(){
				//first unset all article variables that are lingering about
				<?php try { unset($_SESSION['article']); }catch(Exception $ex){} ?>
				<?php try { unset($_SESSION['company']); }catch(Exception $ex){} ?>
				
				location.href='http://www.Afroglobalinvestors.com/?page_id=73'
			}
			
			function createcompany(newcompany){
				alert('in co');
				if (newcompany =='new') {
				//first unset all article variables that are lingering about
				<?php try { unset($_SESSION['article']); }catch(Exception $ex){} ?>
				//then set a variable for company
				}
				alert("sending variable");
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"company",name:"yes"},
					async: false,
					success: function (obj, textstatus) {
						alert(obj);

						location.href='http://www.Afroglobalinvestors.com/?page_id=40';
					
					},
					
				});
				
			}
			

			</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar projects-sb-right" id="sidebar-right">
		
		<h4 style="margin-left:20px"><i>Featured Companies </i></h4>
		<div id="sidebar-right-companies"> </div>
				  
	</aside>
	
	<div class="previous" id="previous" style="display:block;">
		<a href="#page" title="scroll up">
			<img src="images/up.png"></img>
		</a>
	
	</div>
	
	<div class="next" id="next" style="display:block;">
		<a href="#page" title="scroll down">
			<img src="images/down.png"></img>
		</a>
	</div>
	
	
	
</div>

<?php get_footer(); ?>

<?php else : ?>
 
	<div class="saveinfo" id="saveinfo" style="margin-top:50px;">
		<p id = "response" style="font-size:18px;">
			You are not authorized to access this page. Please login first
		</p> 
	</div>
	
<?php endif; ?>