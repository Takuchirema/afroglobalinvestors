<?php /* Template Name: PrivacyAndCookies */ ?>
<?php 
	include_once("analyticstracking.php");
	
	get_header(); 
	
?>
<div class="wrapper clearfix" id="wrapper" style="width:100%;">
	
	<div class="about-content" id="about-content" style="width:100%;">
		<div style="border-bottom:3px solid rgba(210,105,30,0.7);padding-bottom:10px;">
			<p>
			
				<span Style="font-size:20px;font-weight:800;">PRIVACY AND COOKIE POLICY</span><br><br>
 
				<span Style="font-size:18px;font-weight:800;">PRIVACY POLICY</span><br><br>
			
				Afroglobal Investors Limited (“Afroglobal Investors” "we",”us”) are committed to protecting and respecting your privacy.
				This privacy policy applies to the website <a href='http://www.afroglobalinvestors.com'>www.afroglobalinvestors.com</a> owned and operated by Afroglobal Investors Limited. This privacy policy describes how Afroglobal Investors collects and uses the personal information you provide on <a href='http://www.afroglobalinvestors.com'>www.afroglobalinvestors.com</a>. It also describes the choices available to you regarding our use of your personal information and how you can access and update this information.<br><br>
				
				<span Style="font-size:16px;font-weight:800;">INFORMATION WE MAY COLLECT FROM YOU</span><br><br>
				
				 
				We may collect and process the following data about you provided at the time of requesting goods, services or information from us:<br><br>
				
				<ul>
				<li>Information that you provide to us by filling in forms on our site AFROGLOBALINVESTORS.COM (Our Site). This includes Contact Information such as name, email address, mailing address, phone number, and any other requested information as deemed appropriate. Unique Identifiers such as user name, account number, password, and Preferences Information such as favorites lists, transaction history, marketing preferences.  If you chose to list your company with us, we may ask for Information about your business such as company name, company size, business type and personal information such as a professional profile.
				</li>
				<li>Information that you provide to us when you write to us (including by email).</li>
				<li>Information that you provide to us when we speak to you by telephone. We may make and keep a record of the information you share with us.</li>
				<li>Information that you provide to us by completing surveys. </li>
				<li>Details of transactions you carry out through Our Site and of the fulfillment of your orders.</li>
				<li>Information obtained by us from third parties in accordance with this Privacy Policy.  For example, if you choose to list your business with us, we may obtain additional information about your business from other sources as a supplementary risk management action.  
				</li>
				</ul><br><br>
				
				As is true of most web sites, we gather certain information automatically and store it in log files.  This information includes internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and clickstream data.  We use this information, which does not identify individual users, to analyze trends, to administer the site, to track users’ movements around the site and to gather demographic information about our user base as a whole.  We do not link this automatically-collected data to personally identifiable information.<br><br>
				 
				<span Style="font-size:16px;font-weight:800;">USES MADE OF THE INFORMATION</span><br><br>
				 
				We use information held about you in the following ways:<br><br>
				
				<ul>
				<li>To ensure that content from Our Site is presented in the most effective manner for you and for your computer.
				</li>
				<li>To provide you with information, products or services that you request from us, where you have consented to be contacted for such purposes.
				</li>
				<li>To carry out our obligations arising from any contracts entered into between you and us.
				</li>
				<li>To notify you about changes to our goods and services.
				</li>
				</ul><br><br>
				
				We may also use your data, and/or permit selected third parties to use your data, to provide you with information about goods and services which may be of interest to you and we and/or they may contact you about these. If you are an existing customer/member, we will only contact you by electronic means (fax, email or SMS) with information about goods and services similar to those which were the subject of a previous sale to you. We and/or any third party that we permit to use your data will not otherwise contact you by electronic means to provide you with information about goods and services which may be of interest to you, unless you have consented to this. If you are a new customer/member, we and/or any third party that we permit to use your data will only contact you by electronic means to provide you with information about goods and services which may be of interest to you if you have consented to this. If you do not want us to use your data in one or more of the ways mentioned above, or to pass your details on to third parties for marketing purposes, please let us know by contacting us at support@afroglobalinvestors.com or refer to additional instructions in the “Access and Choice” portion of this policy.<br><br>
				


				<span Style="font-size:16px;font-weight:800;"> DISCLOSURE OF YOUR INFORMATION</span><br><br>
				 
				We will share your personal information with third parties only in the ways that are described in this privacy policy.  We do not sell your personal information to third parties.<br><br>
				We may disclose your personal information to any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries.<br><br>
				In addition to the above, we may disclose your personal information to third parties:<br><br>
				
				<ul>
				<li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.
				</li>
				<li>If AFROGLOBAL INVESTORS LIMITED or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.
				</li>
				<li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our Terms and Conditions of Website Use support@afroglobalinvestors.com. and other agreements; or to protect the rights, property, or safety of us, our customers, or others. This includes exchanging information with other companies and organizations for the purposes of fraud protection and credit risk reduction.
				</li>
				</ul><br><br>
				
				We may provide your personal information to companies that provide services to help us with our business activities such as shipping your order or offering customer service. These companies are authorized to use your personal information only as necessary to provide these services to us.<br><br>
				
				We do not disclose information about identifiable individuals to our advertisers, but we may provide them with aggregate information about our users (for example, we may inform them that 500 men aged under 30 have clicked on their advertisement on any given day). We may also use such aggregate information to help advertisers reach the kind of audience they want to target (for example, women in SW1). We may make use of the personal data we have collected from you to enable us to comply with our advertisers' wishes by displaying their advertisements to that target audience.<br><br>
				
				<span Style="font-size:16px;font-weight:800;">ACCESS TO INFORMATION </span><br><br>
				 
				If your personally information changes, or if you no longer desire our service, you may correct, update, amend, delete/remove or request it be deleted by making the changes in your member account settings page or by emailing our Customer Support at support@afroglobalinvestors.com or by contacting us by telephone or postal mail at the contact information listed below.  We will respond to your request to access within 30 days.  Any access request may be subject to a fee of USD15 (subject to change) to meet our costs in providing you with details of the information we hold about you.<br><br>
				
				We will retain your information for as long as your account is active or as needed to provide you services.  We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements, which in some cases involving the collection and processing of financial data may require a retention period of 7 years.<br><br>
				
				If you wish to subscribe to our newsletter(s), we will use your name and email address to send the newsletter to you.  Out of respect for your privacy, you may choose to stop receiving our newsletter or marketing emails by following the unsubscribe instructions included in these emails, accessing the email preferences in your account settings page or you can contact us at support@afroglobalinvestors.com.
				Similarly, you have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes, or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by contacting us at any time.<br><br>
				
				<span Style="font-size:16px;font-weight:800;">COOKIES and Tracking Technologies</span><br><br>
				
				We may collect information about your computer, including where available your IP address, operating system and browser type, for system administration. This is statistical data about our user's browsing actions and patterns, and does not identify any individual.<br><br>
				
				For the same reason, we may obtain information about your general internet usage by using a cookie file which is stored on the hard drive of your computer. Cookies contain information that is transferred to your computer's hard drive. They help us to improve Our Site and to deliver a better and more personalized service. They enable us:<br><br>
				
				<ul>
				<li>To estimate our audience size and usage pattern.
				</li>
				<li>To store information about your preferences, and so allow us to customize Our Site according to your individual interests.
				</li>
				<li>To speed up your searches.
				</li>
				<li>To recognize you when you return to Our Site.
				</li>
				</ul><br><br>
				
				You may refuse to accept cookies by activating the setting on your browser which allows you to refuse the setting of cookies. However, if you select this setting you may be unable to access certain parts of Our Site. Unless you have adjusted your browser setting so that it will refuse cookies, our system may issue cookies when you log on to Our Site.<br><br>
				
				The use of cookies by our partners, affiliates, advertisers or service providers is not covered by our privacy policy.  We do not have access or control over these cookies.<br><br>
				
				We partner with third party ad networks to either display advertising on our Web site or to manage our advertising on other sites.  Our ad network partner uses cookies and Web beacons to collect non-personally identifiable information about your activities on this and other Web sites to provide you targeted advertising based upon your interests.  If you wish to not have this information used for the purpose of serving you targeted ads, you may opt-out by advising us.  Please note this does not opt you out of being served advertising.  You will continue to receive generic ads.<br><br>
				
				<a href='http://www.networkadvertising.org/managing/opt_out.asp'>http://www.networkadvertising.org/managing/opt_out.asp</a><br><br>
				
				Additionally, we may or may not use an ad-publishing service to display 3rd party ads on this site.  In the event that we use an ad-publishing service, when you view or click on an ad a cookie will be set to help better provide advertisements that may be of interest to you on this and other Web sites.   You may opt-out of the use of this cookie by visiting the above tracking preference managers.<br><br>
				
				<span Style="font-size:16px;font-weight:800;">HOW WE STORE, PROCESS AND SECURE YOUR PERSONAL DATA</span><br><br>
				
				The data that we collect from you may be transferred to, and stored at, a destination inside or outside the Southern African Development Community area ("SADC"). It may also be processed by staffs operating inside or outside the SADC who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your orders, the processing of your payment details (if any) and the provision of support services. By submitting your personal data, you agree to this transfer, storing and/or processing. We will take such steps as we consider reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.<br><br>
				
				All information you provide to us via email or Our Site is stored on our secure servers. <br><br>
				
				Where we have given you (or where you have chosen) a password which enables you to access certain parts of Our Site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.<br><br>
				
				The security of your personal information is important to us.  When you enter sensitive information (such as bank account information) on our order forms, we encrypt the transmission of that information using secure socket layer technology (SSL).<br><br>
				
				We follow generally accepted standards to protect the personal information submitted to us, both during transmission and once we receive it.  No method of transmission over the Internet, or method of electronic storage, is 100% secure, however.  Therefore, we cannot guarantee its absolute security. If you have any questions about security on our Web site, you can contact us at support@afroglobalinvestors.com<br><br>
				
				<span Style="font-size:16px;font-weight:800;">ADDITIONAL PRIVACY INFORMATION</span><br><br>
				 
				 Blog / Forum<br><br>
				  
				Our Web site offers a publicly accessible blog and a community forum. You should be aware that any information you provide in these areas may be read, collected, and used by others who access them. To request removal of your personal information from our blog or community forum, contact us at support@afroglobalinvestors.com.  In some cases, we may not be able to remove your personal information, in which case we will let you know if we are unable to do so and why.<br><br>
				
				  <span Style="font-size:16px;font-weight:800;">REFERRALS</span><br><br>
				  
				If you choose to use our referral service to tell a friend about our site, we will ask you for your friend’s name and email address.  We will automatically send your friend a one-time email inviting him or her to visit the site.  Afroglobal Investors stores this information for the sole purpose of sending this one-time email and tracking the success of our referral program.  <br><br>
				
				Your friend may contact us at support@afroglobalinvestors.com to request that we remove this information from our database.<br><br>
				
				  <span Style="font-size:16px;font-weight:800;">Links to 3rd Party Sites</span><br><br>
				  
				Our Site may, from time to time, contain links to and from the websites of our suppliers, partner networks, advertisers, affiliates and other third parties. If you follow a link to any of these websites, please note that these websites should have their own privacy policies and we do not accept any responsibility or liability for these policies or the content or operation of these websites. Please check these policies and the terms of the websites before you submit any personal data to these websites.<br><br>
				
				 <span Style="font-size:16px;font-weight:800;"> Social Media Widgets</span><br><br>
				  
				Our Web site includes Social Media Features, such as the Facebook Like button.  These Features may collect your IP address, which page you are visiting on our site, and may set a cookie to enable the Feature to function properly. Social Media Features and Widgets are either hosted by a third party or hosted directly on our Site. Your interactions with these Features are governed by the privacy policy of the company providing it. <br><br>
				 
				 <span Style="font-size:16px;font-weight:800;"> Single Sign-On</span><br><br>
				  
				You can log in to our site using the sign-in service Facebook Connect. This service will authenticate your identity and provide you the option to share certain personal information with us such as your name and email address to pre-populate our sign up form.  Services like Facebook Connect give you the option to post information about your activities on this Web site to your profile page to share with others within your network. <br><br>
				
				<span Style="font-size:16px;font-weight:800;">CHANGES TO OUR PRIVACY POLICY</span><br><br>
				
				We may update this privacy policy to reflect changes to our information practices. If we make any material changes we will notify you by email (sent to the e-mail address specified in your account) or by means of a notice on this Site prior to the change becoming effective. We encourage you to periodically review this page for the latest information on our privacy practices.<br><br>
				

			
			</p>
		</div>
		
		
	</div>
	
	<script type="text/javascript">	
		function removealert(){
			document.getElementById("response").style.display = "none";
		}

	</script>
	
	

</div>
<?php get_footer(); ?>