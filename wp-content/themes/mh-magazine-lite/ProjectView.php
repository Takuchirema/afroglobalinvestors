<?php /* Template Name: ProjectView */ ?>
<?php
/*save the current page to the appropriate folder
 1. buffer the output*/
//start the buffering
	//ob_start();
?>
<?php 
include_once("analyticstracking.php");
get_header(); ?>
<div class="wrapper clearfix" style="min-height:550px">
	
	<nav class="main-nav" style="width:90%">
		<ul class="nav">
		
			<li class=""><a href="#" onclick="fundproject()" id ="fund-project"></a></li>

		</ul>
	
	</nav>
			
	<section style="width:60%;"class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget" id="project-details">
			
			<fieldset class ='entry-field' id="slide-images" style="position:relative;display: inline-block;border:none">
				<div width="260" height="194"><img src="images/logo.PNG" id="slide" style="width:100%;height:100%;object-fit: contain;"  /></div>
				<input type="image" src="images/left.PNG" id="slide-left" width="50" height="60" style="border-radius:50%;border:none" />
				<input type="image" src="images/right.PNG" id="slide-right" width="50" height="60" style="border-radius:50%"  />
			</fieldset>
			
			<div style="background: #111;margin-top:5px" class="project-bar">
				<button class="active" id="project-about">About</button>
				<button class="dormant" id = "the-picture">The Picture</button>
				<button type="button" class="dormant" id = "project-updates" onclick="showupdates()" >Latest Updates</button>
				<button type="button" class="dormant" id = "project-comments" onclick="showcomments()">Comments</button>
				<button type="button" class="dormant" id = "showfunding" onclick="showdetails()">Funding details</button>
			</div>
			
			<fieldset id="project-elements" class ="entry-field" style="margin-top: 5px">
			
				<?php if (isset($_SESSION["post-response"])) : ?>
					<div class="saveinfo" id="saveinfo" > <p id = "response"><?php echo $_SESSION["post-response"];
						unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
					</div>
				<?php endif; ?>
				
				<p id="project-text" ></p>
				
				<div class="entry sb-widget" style="display:none;" name="container">
					<h4 class="widget-title">Enter your comment</h4>
					<fieldset class ="entry-field" style="border: 1px solid #fff">
						<textarea rows="5" id="text-comment" name="text-comment" maxlength="1000" ></textarea>
						<button type="button" style="float:right;margin-right:5px;margin-top:5px" onclick="submitcomment();">submit</button>
					</fieldset>
					
					<h4 class="widget-title">Recent Comments</h4>
				</div>
				
			</fieldset>
			
			<script type="text/javascript">
				var projectname = "<?php echo $_GET['project']; ?>" ;
				
				var homeurl = "<?php echo home_url() ; ?>";
				
				var username = "<?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} else{echo "" ;} ?>";
				
				//alert("the projectname "+projectname);
				
				var dir = homeurl+"/projects/"+projectname+"/images"; //pick the directory of the project
				var fileextension = [".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF"];
				var images = new Array();
				var imagetags;// = new Array();
				var count = 0;
				var projectdetails;// = new Array();
				var project;
				
				
				setdefaults();

				getProjectDetails();
				
				getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
				
				function removealert(){
					document.getElementById("response").style.display = "none";
				}
					
				function setdefaults() {
					
					var slidel = document.getElementById('slide-left');
					var slider = document.getElementById('slide-right');
					
					slidel.src = homeurl+"/images/left.PNG";
					slider.src = homeurl+"/images/right.PNG";
					
					document.getElementById("fund-project").innerHTML = "Fund "+projectname;
					
					
					
				}
				
				function getProjectDetails(){
					$.ajax({
						type: "POST",
						url: homeurl+'/backend/getProject.php',
						data: {projectname: projectname},

						success: function (obj, textstatus) { 
							  projectdetails = jQuery.parseJSON(obj);//JSON.parse(obj);
							  
								metadata(projectdetails);
								
								sidebar(projectdetails);
								
								updates(projectdetails);
								
								getimages();
								
								piechart(projectdetails);
								
							   
						},
						
						error: function(xhr, textStatus, error){
							  alert("the err "+error+" "+textStatus);
						}
					});
				
				}
				
				function piechart(parsed){
					 
					 $.getScript("https://www.google.com/jsapi", function () {
						 google.load('visualization', '1', { 'callback': '', 'packages': ['corechart'] });
					});
				  
				  // Set a callback to run when the Google Visualization API is loaded.
				  google.setOnLoadCallback(drawChart);

				  // Callback that creates and populates a data table,
				  // instantiates the pie chart, passes in the data and
				  // draws it.
				  function drawChart() {

				  // Create the data table.
					var data = new google.visualization.DataTable();

					data.addColumn('string', 'Funding');
					data.addColumn('number', 'Amount');
					
					var equity = parseInt(parsed['project'][0]['equity']) ;
					var debt = parseInt(parsed['project'][0]['debt']) ;
					var donations = parseInt(parsed['project'][0]['donations']);
					
					
					data.addRows([
						['Equity',  {v: equity, f: '$USD'+equity}],
						['Debt', {v: debt, f: '$USD'+debt}],
						['Donations',{v: donations, f: '$USD'+donations}]
					]);
					
										
					// Set chart options
					var options = {'title':'Funding distribution',
								   'width':400,
								   'height':300};
					
					// Instantiate and draw our chart, passing in some options.
					var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
					chart.draw(data, options);
					
				  }
				
				}
				
				
				function metadata(parsed){
					document.getElementById("seo-title").innerHTML = parsed["project"][0]["projectname"] +" - Projects | Afroglobalinvestors";
					
					var metadesc = document.createElement("meta");
					metadesc.setAttribute("content",parsed["project"][0]["projecttag"]);
					metadesc.setAttribute("name","description");
					metadesc.setAttribute("itemprop","description");
					
					document.getElementsByTagName("head")[0].appendChild(metadesc);
				}
				
				function getimages() {
					//main image first
					$.ajax({
						url: dir+"/mainimage",
						success: function imagesdata(data) {
							var extensions = "";
							for (i=0;i<fileextension.length;i++){
								extensions = extensions +"a[href$='"+ (fileextension[i])+ "'],";
							}
							
							extensions = extensions.substring(0, extensions.length - 1);
							
							$(data).find(extensions).each(function () {
							//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
								var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
								var filename = dir + "/mainimage/" + filename;

								images[count] = new Image();
								images[count].src = filename;
								count++;
								
							});
						}
						
					});
					
					$.ajax({
						url: dir,
						success: function imagesdata(data) {
						
							var extensions = "";
							for (i=0;i<fileextension.length;i++){
								extensions = extensions +"a[href$='"+ (fileextension[i])+ "'],";
							}
							
							extensions = extensions.substring(0, extensions.length - 1);
							
							$(data).find(extensions).each(function () {
							//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
								var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
								var filename = dir + "/" + filename;

								images[count] = new Image();
								images[count].src = filename;
								count++;
								
							});
							
							getimagetags();
						}
						
					});
				}
				
				function getimagetags() {
					$.ajax({
						type: "POST",
						url: homeurl+'/backend/project_images.php',
						data: {projectname: projectname},

						success: function (obj, textstatus) { 
							  imagetags = jQuery.parseJSON(obj);//JSON.parse(obj);
							   imagesComplete(imagetags);
							   
						},
						
						error: function(xhr, textStatus, error){
							  alert("the err "+error);
						}
					});
				};
				
				
				var step=0;
					
				function slideit(){
					 if (!document.images)
					  return
					document.getElementById('slide').className += "fadeOut";
					setTimeout(function() {
						var imagename = images[step].src.substring (images[step].src.lastIndexOf ( '/' ) + 1 );
						
						document.getElementById('slide').src = images[step].src;
						
						//then get the tag of picture if any and put it on the div section
						//if (document.getElementById("project-about").className == "dormant"){
							//showImageDetail();
						//}
						
						document.getElementById('slide').className = "";
					},1000);
					
					 if(step> images.length -1) {
						step=0;
					 } else if ( step < 0){
						step = images.length -1;
					 }
				}
				
				function imagesComplete(parsed) {
					//prepare about content
					aboutcontent();
					
					getProjectComments( homeurl+'/backend/getProjectComments.php');
					
					//also get updates
					getUpdates();
					
					slideit();
					
					//put each of the pictures information in a hidden text area
					loop:
					for (i=0; i<images.length; i++)
					{	
						
						var imgname = images[i].src.substring (images[i].src.lastIndexOf ( '/' ) + 1 );
						
						var imagetext = document.createElement("textarea");
						imagetext.setAttribute("name","text-"+imgname);
						
						imagetext.setAttribute("style","display:none");
						
						if (parsed['images'][imgname] != undefined)
						{
							imagetext.value = parsed['images'][imgname];
							
						} else{
							imagetext.value =  "";
						}
						
						document.getElementById("project-details").appendChild(imagetext);
						
					}
					
				  
				};
				
				function aboutcontent(){
					var p = document.createElement("p");
					p.innerHTML = projectdetails['project'][0]['projectabout'];

					var images = p.getElementsByTagName("img");
					
					//put each of the pictures information in a hidden text area
					loop:
					for (i=0; i<images.length; i++)
					{	 
						
						
						var imagename = images[i].src.substring (images[i].src.lastIndexOf ( '/' ) + 1 );

						var imagetag = imagetags['images'][imagename];
						
						//create the div element to have the image and the title
						var imagediv = document.createElement('div');
						imagediv.setAttribute("style","display:inline-block;");
						imagediv.setAttribute("class","article-image "+images[i].className);
						
						var titlediv = document.createElement("div");
						titlediv.innerHTML =imagetag;
						titlediv.setAttribute("style","bottom:0px;position:absolute;width: 100%;");
						
						var imagecln = images[i].cloneNode(true);

						images[i].parentNode.replaceChild(imagediv, images[i]);
						imagediv.appendChild(imagecln);
						imagediv.appendChild(titlediv);
					}

					projectdetails['project'][0]['projectabout'] = p.innerHTML;

					document.getElementById('project-text').innerHTML = p.innerHTML;
				
				}
				
				$(document).ready(
					function(){
						$('#slide-left').click(function(){
							step = step - 1;
							slideit();
						});
						
						$('#slide-right').click(function(){
							step = step + 1;
							slideit();
						});
						
						//the about and picture buttons
						$('#project-about').click(function(){
							var comments = document.getElementsByName("container");
							comments[0].style.display="none";
							
							var updates = document.getElementsByName("container-updates");
							updates[0].style.display="none";
							
							var details = document.getElementsByName("container-showfunding");
							details[0].style.display="none";
							
							
							document.getElementById("project-comments").className = "dormant";
							document.getElementById("project-about").className = "active";
							document.getElementById("the-picture").className = "dormant";
							document.getElementById("project-updates").className = "dormant";
							document.getElementById("showfunding").className = "dormant";
							
							document.getElementById('project-text').innerHTML = projectdetails['project'][0]['projectabout'];

						});
						
						$('#the-picture').click(function(){
							var comments = document.getElementsByName("container");
							comments[0].style.display="none";
							
							var updates = document.getElementsByName("container-updates");
							updates[0].style.display="none";
							
							var details = document.getElementsByName("container-showfunding");
							details[0].style.display="none";

							
							document.getElementById("project-comments").className = "dormant";
							document.getElementById("project-about").className = "dormant";
							document.getElementById("project-updates").className = "dormant";
							document.getElementById("showfunding").className = "dormant";
							
							document.getElementById("the-picture").className = "active";
							
							showImageDetail();
						
						});
					}
				);
				
				function showImageDetail(){
				
					var imagename = images[step].src.substring (images[step].src.lastIndexOf ( '/' ) + 1 );
					
					var text = document.getElementsByName("text-"+imagename);
					
					document.getElementById('project-text').innerHTML = text[0].value;
				}
				
				function showcomments(){
					document.getElementById("project-text").innerHTML = "";
					//also hide the updates section
					var updates = document.getElementsByName("container-updates");
					updates[0].style.display="none";
					
					var details = document.getElementsByName("container-showfunding");
					details[0].style.display="none";
							
					var comments = document.getElementsByName("container");

					comments[0].style.display="";
					document.getElementById("project-comments").className = "active";
					
					document.getElementById("project-about").className = "dormant";
					document.getElementById("the-picture").className = "dormant";
					document.getElementById("project-updates").className = "dormant";
					document.getElementById("showfunding").className = "dormant";
					
				
				}
				
				function showupdates(){

					document.getElementById("project-text").innerHTML = "";

					//also hide the comments section
					var comments = document.getElementsByName("container");
					comments[0].style.display="none";
					
					var details = document.getElementsByName("container-showfunding");
					details[0].style.display="none";
					
					var updates = document.getElementsByName("container-updates");
					updates[0].style.display="";
					document.getElementById("project-updates").className = "active";
					
					//all other classes dormant
					document.getElementById("project-about").className = "dormant";
					document.getElementById("the-picture").className = "dormant";
					document.getElementById("project-comments").className = "dormant";
					document.getElementById("showfunding").className = "dormant";
				
				}
				
				function fundproject(){
					showdetails();
					document.getElementById('showfunding').focus();
				}
				
				function showdetails(){

					document.getElementById("project-text").innerHTML = "";
					//also hide the comments section
					var comments = document.getElementsByName("container");
					comments[0].style.display="none";
					
					
					var updates = document.getElementsByName("container-updates");

					updates[0].style.display="none";
					
					var details = document.getElementsByName("container-showfunding");

					details[0].style.display="";
					document.getElementById("showfunding").className = "active";
					
					
					
					//all other classes dormant
					document.getElementById("project-about").className = "dormant";
					document.getElementById("the-picture").className = "dormant";
					document.getElementById("project-comments").className = "dormant";
					document.getElementById("project-updates").className = "dormant";
					
				}
				
				
				
				function sidebar(parsed){
					//alert('user '+projectdetails['project'][0]['projectleader'][0]);
					//get projects can also be used to get a userprofile on right sidebar
					getProjects("no","yes","no","no",projectdetails['project'][0]['projectleader'][0],"no","backend/profile_values.php",homeurl);
					
					//fill the side bar					
					var enddate = parsed['project'][0]['date']+'';
					enddate = enddate.split("-");
			
					enddate = new Date(enddate[0], enddate[1] - 1, enddate[2]);
					
					var today = new Date();
					
					var timeDiff = Math.abs(enddate.getTime() - today.getTime());
					var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
					
					document.getElementById('days-left').innerHTML = ""+diffDays+" days left";
					
					try{
						document.getElementById('equity').innerHTML = "$ "+parsed['project'][0]['equity']+" @ $"+parsed['project'][0]['equityvalue']+" per share";
						document.getElementById('debt').innerHTML = "$ "+parsed['project'][0]['debt']+" @ "+parsed['project'][0]['debtinterest']+"% compound interest";
						document.getElementById('donations').innerHTML = "$ "+parsed['project'][0]['donations'];
					} catch(err){alert('fund err '+err.message);}
					
					
				}
				
				
				function updates(parsed){
					
					//create the container for funding details
					var detailscontainer = document.createElement("div");
					detailscontainer.setAttribute("class","entry sb-widget");
					detailscontainer.style.display = "none";
					detailscontainer.setAttribute("name","container-showfunding");
					detailscontainer.innerHTML = parsed["project"][0]["fundingdetails"];
					document.getElementById("project-elements").appendChild(detailscontainer);
				
					var container = document.createElement("div");
					container.setAttribute("class","entry sb-widget");
					container.style.display = "none";
					container.setAttribute("name","container-updates");
					
					//put the edit field only if the project leader is logged in
					if (parsed["project"][0]["projectleader"] == username) {
						var update = document.createElement("h4");
						update.setAttribute("class","widget-title");
						update.innerHTML = "Insert a new Update";
						
						var field = document.createElement("fieldset");
						field.setAttribute("class","entry-field");
						field.setAttribute("style","border: 1px solid #fff");
						
						var textarea = document.createElement("textarea");
						textarea.setAttribute("style","width: 550px; height: 200px;");
						textarea.setAttribute("name","text-update");
						textarea.setAttribute("maxlength","1000");
						textarea.id = "text-update";
						
						var submit = document.createElement("button");
						submit.setAttribute("type","button");
						submit.setAttribute("style","float:right;margin-right:5px;margin-top:5px");
						submit.setAttribute("onclick","submitupdate()");
						submit.innerHTML="submit";
						
						field.appendChild(textarea);
						field.appendChild(submit);
						
						container.appendChild(update);
						container.appendChild(field);
						
					}
					
					var updates = document.createElement("h4");
					updates .setAttribute("class","widget-title");
					updates .innerHTML = "Recent Updates";
					
					
					
					container.appendChild(updates);
					
					document.getElementById("project-elements").appendChild(container);
					
					
				}
				
				
				function submitupdate(){
					
					var updates = document.getElementsByName("text-update");
					var update = updates[0].value;
					var updatestatus = new Array();
					
					var today = new Date();
					var dd = today.getDate();
					var mm = today.getMonth()+1; 
					var yyyy = today.getFullYear();
					
					var date = yyyy+"-"+mm+"-"+dd;
					
					$.ajax({
						type: "POST",
						url: homeurl+'/backend/submitUpdate.php',
						data: {projectname: projectname,projectleader: username,date: date, update: update},

						success: function (obj, textstatus) { 
							 
							 updatestatus = jQuery.parseJSON(obj);
							 
							 postResponse(updatestatus,"","");
							 
						},
						
						error: function(xhr, textStatus, error){
							  alert("the err "+error);
						}
					});
					
				}
				
				function submitcomment(){
					
					if (username == ""){
					
						document.getElementById('nav-login').click();
						
						document.getElementById('login-header-response').style.display = "";
						document.getElementById('login-response').innerHTML = "Please login first to submit a comment";
						
					} else{
						
						var comments = document.getElementsByName("text-comment");

						var comment = comments[0].value;
						
						var today = new Date();
						var dd = today.getDate();
						var mm = today.getMonth()+1; 
						var yyyy = today.getFullYear();
						
						var postdate = yyyy+"-"+mm+"-"+dd;
						
						$.ajax({
							type: "POST",
							url: homeurl+'/backend/submitProjectComment.php',
							data: {projectname: projectname,postername: username,postdate: postdate, comment:comment},

							success: function (obj, textstatus) { 
							
								  commentsparsed = jQuery.parseJSON(obj);
								  
								  //found in forms.js
								  postResponse(commentsparsed,"","");
							},
							
							error: function(xhr, textStatus, error){
								  alert("the err "+error);
							}
						});
						
					}
					
				}
				
			</script>
			
		</div>
		
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar sb-right" style="width:30%;">
		
		<div class="sb-widget"><h4 class="widget-title">Project leader</h4>
			<a id="user-profile"></a>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title">Timeline</h4>
			<a id="days-left" style="background:#E66432;border-radius: 4px 4px 4px 4px;color:#fff;padding:5px" ></a>
			<a id="projectremaining" ></a>
		</div>
		
		<div class="sb-widget ">
			<h4 class="widget-title">Funding</h4>
			
			<div style="overflow:auto;">
			 <div id="chart_div" style="padding:none;margin:none;"></div>
			 
			 
			<table id="funding-side-bar" >
				<tr>
					<th>Funding</th>
					<th>Amount (USD)</th> 
				 </tr>
				 <tr id="equity-row">
					<td>Equity</th>
					<td id="equity">Amount</th> 
				 </tr>
				 <tr id="debt-row">
					<td>Debt</th>
					<td id="debt">Amount</th> 
				 </tr>
				 <tr id="donations-row">
					<td>Donations</th>
					<td id="donations"></th> 
				 </tr>
			</table>
			</div>
			
		</div>
		
		<div class="sb-widget" id="sidebar-right" >
			<h4 style="margin-left:1px"><i>Featured Companies </i></h4>
			<div id="sidebar-right-companies"> </div>
			
		</div>
		
	</aside>
	

</div>

<?php get_footer(); ?>

<?php 
//get the contents and put them in the file
	//file_put_contents('./projects/'.$_GET['project'].'/'.$_GET['project'].'.html',ob_get_contents());
?>