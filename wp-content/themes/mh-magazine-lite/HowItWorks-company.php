<?php /* Template Name: HITCompany */ ?>
<?php 
include_once("analyticstracking.php");
get_header(); ?>
<div class="wrapper clearfix">
	<nav class="main-nav">
		<ul class="nav">
			
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=86">Create your profile</a></li>
			<li class=""><a href = "http://www.Afroglobalinvestors.com/?page_id=88">Create your project</a></li>
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=90">Create your company</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=92">Write an article</a></li>


		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget how-it-works">
			<h4>Follow these basic steps in creating your company profile</h4>
			
			<div class="steps">
				<div class="step-number"><span>1</span></div>
				<p>Enter all your company basics, guidelines are on the right</p>
				<img src="images/companybasics.PNG"></img>
			</div>
			
			<div class="steps">
				<div class="step-number"><span>2</span></div>
				<p>Fill in and <strong style="color:#800000">save</strong> your company details</p>
				<img src="images/companydetails.PNG"></img>
			</div>
			
		
		<script type="text/javascript">				
		</script>

				
	</section>
	
	<aside class="sidebar sb-right">
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:40px" >Company basics</h4>
			<ul class="nav">
				<li>Enter a name that best decribes your company <br><a style="color:#800000;font-weight:bold">*NB The  name cannot be changed.</a></br></li>
				<li>Look for availability of the  name (right corner of entry box)</li>
				<li>All other instructions and tips will be on the right hand side as you fill in</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:150px">Company Details</h4>
			<ul class="nav">
				<li>This is what funders will see of your on your company site</li>
				<li>Upload the images and use them in your project description</li>
				
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title"  >Save</h4>
			<ul class="nav">
				<li>Do not forget to save/edit</li>
				<li>Unlike a project a company instantly becomes visible after creation </li>
				<li>In the future a small fee will be payable to advertise your company on Afroglobalinvestors</li>
			</ul>
		</div>
		
		
	</aside>
	
	
</div>
<?php get_footer(); ?>