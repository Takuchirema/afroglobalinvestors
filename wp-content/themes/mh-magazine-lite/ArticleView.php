<?php /* Template Name: ArticleView */ ?>
<?php 
include_once("analyticstracking.php");
get_header(); 
?>
<div class="article-wrapper clearfix" id="article-wrapper" style="min-height:550px;">
	
	<aside class="sidebar article-sb-left left" id="sidebar-left">
		<h4 ><i>Featured Articles </i></h4>
		<div id="sidebar-left-articles"> </div>
		
		<h4 style="padding-bottom: 10px;"><i>Africa stock markets</i></h4>
		
		<!-- start feedwind code -->
		<script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');
		</script>
		
		<script type="text/javascript">(function() {var params = {rssmikle_url: "http://feeds.feedburner.com/africanmarkets/stockmarkets",rssmikle_frame_width: "300",rssmikle_frame_height: "800",frame_height_by_article: "0",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "14",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "on",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "15",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "",rssmikle_title_link: "",rssmikle_title_bgcolor: "#800000",rssmikle_title_color: "#FFFFFF",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#E66432",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M:%S %p",item_description_style: "text+tn",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();
		</script>
	
		<div style="font-size:10px; text-align:center; width:300px;"><a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a><!--Please display the above link in your web page according to Terms of Service.-->
		</div><!-- end feedwind code -->
		
	</aside>
	
	
	<div class="article-content" id="article-content">
		
		<article class="post-64 post type-post status-publish format-standard hentry category-uncategorized">
			<header class="post-header">
				
				<h1 class="title" id="title">Testing</h1>
				<p class="meta post-meta">Posted on <span class=""><span class="fn"><a id="updated"></a></span></span> by <span class="vcard author"><span class="fn"><a href="#" title="Posts by Takunda" rel="author" id="author">Takunda</a></span></span> in <a href="#" rel="category" id="category">Uncategorized</a></p>
				
				<?php if (isset($_SESSION["post-response"])) : ?>
					<div class="saveinfo" id="saveinfo" > <p id = "response"><?php echo $_SESSION["post-response"];
						unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
					</div>
				<?php endif; ?>
				
			</header>
			
			<div class="entry clearfix">
					<p id="article">Your article here</p>
			</div>
			
			<fieldset id="project-elements" class ="entry-field" style="margin-top:5px;">
				<p id="project-text" ></p>
				
				<div class="entry sb-widget" name="container">
					<h4 class="widget-title">Enter your comment</h4>
					<fieldset class ="entry-field" style="border: 1px solid #fff">
						<textarea rows="5" id="text-comment" name="text-comment" maxlength="1000"></textarea>
						<button type="button" style="float:right;margin-right:5px;margin-top:5px" onclick="submitcomment();">submit</button>
					</fieldset>
					
					<h4 class="widget-title">Recent Comments</h4>
				</div>
				
			</fieldset>
		
		</article>	
		
	</div>
				
	<script type="text/javascript">
		var articlename = "<?php if(isset($_GET['article'])){echo $_GET['article'];}else{echo "";} ?>" ;//"The space";
		//alert('article name '+articlename);
		var homeurl = "<?php echo home_url() ; ?>";
		
		var username = "<?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} else{echo "" ;} ?>";
					
		var dir = homeurl+"/newsarticles/"+articlename+"/images"; //pick the directory of the article
		var fileextension = [".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF"];
		var articledetails;
		var imagetags;
		var count = 0;
		var parsed;
		var article;
		
		//values for the article
		getarticleDetails();
		
		
		function removealert(){
			document.getElementById("response").style.display = "none";
		}
		
				
		function getarticleDetails(){
			$.ajax({
				type: "POST",
				url: homeurl+'/backend/getArticle.php',
				data: {articlename: articlename},

				success: function (obj, textstatus) { 
				
					  articledetails = jQuery.parseJSON(obj);
					  
					  metadata();
						
					  getArticleComments( homeurl+'/backend/getArticleComments.php');
						
					  getimagetags();
					   
				},
				
				error: function(xhr, textStatus, error){
					 var err = eval("(" + xhr.statusText + ")");
					 alert(err.Message);
					 
				}
			});
		
		
		}
		
		function metadata(){
			//the bar for article
			
			<!-- <p class="meta post-meta">Posted on <span class=""><span class="fn"><a id="updated"></a></span></span> by <span class="vcard author"><span class="fn"><a href="http://www.Afroglobalinvestors.com/" title="Posts by Takunda" rel="author" id="author">Takunda</a></span></span> in <a href="http://www.Afroglobalinvestors.com/" rel="category" id="category">Uncategorized</a></p> -->
			var author = articledetails["article"][0]["author"];
			
			var authordetails = document.getElementById('author');
			authordetails.setAttribute('title','Posts by '+author);
			authordetails.setAttribute('href','http://www.Afroglobalinvestors.com/?page_id=99&username='+author);
			authordetails.innerHTML = author;

			
			document.getElementById("seo-title").innerHTML = articledetails["article"][0]["articlename"] +" - Articles | Afroglobalinvestors";
			
			var metadesc = document.createElement("meta");
			metadesc.setAttribute("content",articledetails["article"][0]["articletag"]);
			metadesc.setAttribute("name","description");
			metadesc.setAttribute("itemprop","description");
			
			document.getElementsByTagName("head")[0].appendChild(metadesc);
		}
		
		
		//now get the tags of the images from the database
		function getimagetags() {
			
			$.ajax({
				type: "POST",
				url: homeurl+'/backend/article_images.php',
				data: {articlename: articlename},

				success: function (obj, textstatus) { 
					  imagetags = jQuery.parseJSON(obj);//JSON.parse(obj);
					   imagesComplete();
					   
				},
				
				error: function(xhr, textStatus, error){
					alert("Sorry an error occurred");
				}
			});
		}
				
		function imagesComplete() {//processes the images in article by putting the description beneath
			var imageheights = 0;
			var count = 0;
			
			var p = document.getElementById('article');
			p.innerHTML = articledetails["article"][0]["article"];
			
			
			var images = p.getElementsByTagName("img");
			//put each of the pictures information in a hidden text area
			loop:
			for (i=0; i<images.length; i++)
			{	 
				
				
				var imagename = images[i].src.substring (images[i].src.lastIndexOf ( '/' ) + 1 );
				var imagetag = imagetags['images'][imagename];
				
				//create the div element to have the image and the title
				var imagediv = document.createElement('div');
				imagediv.setAttribute("style","display:inline-block;");
				imagediv.setAttribute("class","article-image "+images[i].className);
				
				var titlediv = document.createElement("div");
				titlediv.innerHTML =imagetag;
				titlediv.setAttribute("style","bottom:0px;position:absolute;width: 100%;");
				
				var imagecln = images[i].cloneNode(true);
				images[i].parentNode.replaceChild(imagediv, images[i]);
				imagediv.appendChild(imagecln);
				imagediv.appendChild(titlediv);
			}
			
			document.getElementById("title").innerHTML = articledetails["article"][0]["articlename"];

			document.getElementById("category").innerHTML = articledetails["article"][0]["articlecategory"];

			document.getElementById("article").innerHTML = p.innerHTML;
			document.getElementById("updated").innerHTML =articledetails["article"][0]["submitteddate"];
			
		  
		}		
		
		//*************************** Companies adverts, Randomly picked, All the code is in global.js ***************************//
		
		function submitcomment(){

			if (username == ""){
				document.getElementById('nav-login').click();
				
				document.getElementById('login-header-response').style.display = "";
				document.getElementById('login-response').innerHTML = "Please login first to submit a comment";
				
			} else{
			
				var comments = document.getElementsByName("text-comment");

				var comment = comments[0].value;
				
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				
				var postdate = yyyy+"-"+mm+"-"+dd;
				
				var response = new Array();
				
				$.ajax({
					type: "POST",
					url: homeurl+'/backend/submitArticleComment.php',
					data: {articlename: articlename,postername: username,postdate: postdate, comment:comment},

					success: function (obj, textstatus) { 

						commentsparsed = jQuery.parseJSON(obj);//JSON.parse(obj);
						  
						  //found in forms.js
						  postResponse(commentsparsed,"","");
						 
					},
					
					error: function(xhr, textStatus, error){
						  alert("the err "+error);
					}
				});
			
			}
		}

		//put adverts on the page
		getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
		
		getProjects("yes","no","no","yes","no","no","backend/getarticles.php",homeurl);
		
		
	</script>
	
	<aside class="sidebar article-sb-right" id="sidebar-right">
		<h4 style="margin-left:20px"><i>Featured Companies </i></h4>
		<div id="sidebar-right-companies"> </div>
		
	</aside>
	
	
</div>


<?php get_footer(); ?>

