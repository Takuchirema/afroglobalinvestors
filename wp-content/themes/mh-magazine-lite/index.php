<?php 
	
	get_header(); 
?>
<div id="content" class="site-content " style="padding-top: 80px;">
	
	<section id="main-slider" class="full-screen-yes">
		
 		<script type="text/javascript">
            jQuery(function($){
				$('#main-slider .bx-slider').bxSlider({
					adaptiveHeight: true,
					pager: true,
					controls: true,
					mode: 'fade',
					auto : 'true',
					pause: '5000',
					speed: '5000'
				});

								
			});
        </script>
        
            <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 568px;"><div class="bx-slider" style="width: auto; position: relative;">
				<div class="slides" style="float: none; list-style: none; position: absolute; width: 1349px; z-index: 0; display: none;"><div class="overlay"></div>
					<img src="./images/mainpage/slider1.jpg" alt="slider1">
					<div class="slider-caption" style="margin-top: -93.5px;">
						<div class="mid-content">
							<h1 class="caption-title">Welcome to Afroglobalinvestors!</h1>
							<h2 class="caption-description">
							<p>Showcase your project and meet investors </p>
							<p><a href="http://www.Afroglobalinvestors.com/?page_id=83">Read More</a></p>
							</h2>
						</div>
					</div>
				</div>
						
				<div class="slides" style="float: none; list-style: none; position: absolute; width: 1349px; z-index: 50; display: block;"><div class="overlay"></div>
					<img src="./images/mainpage/slider4.jpg" alt="slider2">
					<div class="slider-caption" style="margin-top: -93.5px;">
						<div class="ak-container">
							<h1 class="caption-title">Explore amazing projects in Africa</h1>
							<h2 class="caption-description">
							<p><span style='font-weight:600;font-size:30px;'>Crafts</span>, Tourism, Technology, Finance, Mining, Farming , Health, Education – and more . . .</p>
							<p><a href="http://www.Afroglobalinvestors.com/?page_id=83">Read More</a></p>
							</h2>
							</div>
					</div>
				</div>
				
				<div class="slides" style="float: none; list-style: none; position: absolute; width: 1349px; z-index: 50; display: block;"><div class="overlay"></div>
					<img src="./images/mainpage/slider5.jpg" alt="slider2">
					<div class="slider-caption" style="margin-top: -93.5px;">
						<div class="ak-container">
							<h1 class="caption-title">Explore amazing projects in Africa</h1>
							<h2 class="caption-description">
							<p><span style='font-weight:600;font-size:30px;'>Education</span>, Tourism, Technology, Finance, Mining, Farming , Health, Community – and more . . .</p>
							<p><a href="http://www.Afroglobalinvestors.com/?page_id=83">Read More</a></p>
							</h2>
							</div>
					</div>
				</div>
				
				<div class="slides" style="float: none; list-style: none; position: absolute; width: 1349px; z-index: 50; display: block;"><div class="overlay"></div>
					<img src="./images/mainpage/slider6.jpg" alt="slider2">
					<div class="slider-caption" style="margin-top: -93.5px;">
						<div class="ak-container">
							<h1 class="caption-title">Explore amazing projects in Africa</h1>
							<h2 class="caption-description">
							<p><span style='font-weight:600;font-size:30px;'>Mining</span>, Tourism, Technology, Finance, Energy, Farming , Health, Community – and more . . .</p>
							<p><a href="http://www.Afroglobalinvestors.com/?page_id=83">Read More</a></p>
							</h2>
							</div>
					</div>
				</div>
				
				<div class="slides" style="float: none; list-style: none; position: absolute; width: 1349px; z-index: 50; display: block;"><div class="overlay"></div>
					<img src="./images/mainpage/slider7.jpg" alt="slider2">
					<div class="slider-caption" style="margin-top: -93.5px;">
						<div class="ak-container">
							<h1 class="caption-title">Explore amazing projects in Africa</h1>
							<h2 class="caption-description">
							<p><span style='font-weight:600;font-size:30px;'>Tourism</span>, Retail, Technology, Finance, Energy, Farming , Health, Community – and more . . .</p>
							<p><a href="http://www.Afroglobalinvestors.com/?page_id=83">Read More</a></p>
							</h2>
							</div>
					</div>
				</div>
				
			</div></div>
			
			<!--<div class="bx-controls bx-has-pager bx-has-controls-direction">
				<div class="bx-pager bx-default-pager">
					<div class="bx-pager-item">
						<a href="#" data-slide-index="0" class="bx-pager-link">1</a>
					</div>
					<div class="bx-pager-item">
						<a href="#" data-slide-index="1" class="bx-pager-link active">2</a>
					</div>
				</div>
				<div class="bx-controls-direction">
					<a class="bx-prev" href="#">Prev</a><a class="bx-next" href="#">Next</a>
				</div>
			</div> -->
			
			</div>
	</section>
		
    <section class="parallax-section clearfix portfolio_template" id="portfolio">
        <div class="overlay"></div>
        <div class="mid-content">
            <h1><span>Explore Projects</span></h1>
            <div class="parallax-content">
                <div class="page-content">
                    <p style="text-align: center;">Invest, Showcase, Learn, Be informed</p>
                </div>
            </div> 
            <div class="portfolio-listing clearfix" id="project-listing">
				
				<div class="left-column" id="project-left-column" >
					<div class="home-project show"  >
						<div onclick="location.href='http://www.Afroglobalinvestors.com'" class=" portfolio-list wow fadeInUp" data-wow-delay="0.25s" style="-webkit-animation-delay: 2s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/tourism.jpg">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >Tourism</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
										
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="0.75s" style="visibility: hidden; -webkit-animation-delay: 0.75s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/manufacturing.JPG">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >manufacturing</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
				
				</div>
				
				<div class="center-column" id="project-center-column">
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="1s" style="visibility: hidden; -webkit-animation-delay: 1s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/education.jpg">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >education</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
										
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="1.5s" style="visibility: hidden; -webkit-animation-delay: 1.5s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/technology.jpg">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >technology</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
				
				</div>
				
				<div class="right-column" id="project-right-column">
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="1.75s" style="visibility: hidden; -webkit-animation-delay: 1.75s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/food.jpg">
							</div>
						
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >food</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
					
					<div class="home-project show" >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="2s" style="visibility: hidden; -webkit-animation-delay: 2s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/crafts.jpg">
							</div>
						
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date">crafts</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
				
				</div>
				
				<div class="jquery-popup" id="project-category-dialog" title="select a category" style="">
				
					<input id="project-close-category" type="button" value="x" onclick="" style="padding:1px;text-size:12px;float:right;colour:#111">
					
					<!--getProjects(getsubmitted,getcompanies,getcategory,getarticles,company,url)
						getProjects("yes","yes","no","no","no","backend/getprojects.php");					-->
						
					<ul class="category-nav" id="project-category-nav" style="margin-top:20px" >
						<li><a class="" href="#project-listing" id="deal-equity" onclick="getProjects('yes','no','yes','no','no','backend/getprojects.php')" >All</a></li>
						<li><a class="" href="#project-listing"  id="deal-equity" onclick="getProjects('no','no','Education','no','no','backend/getprojects.php')" >Education</a></li>
						<li><a class="" href="#project-listing"    id="deal-debt" onclick="getProjects('no','no','Crafts','no','no','backend/getprojects.php')">Crafts</a></li>
					   <li><a class="" href="#project-listing"   id="deal-donations" onclick="getProjects('no','no','Mining','no','no','backend/getprojects.php')">Mining</a></li>
					   <li><a class="" href="#project-listing"  id="deal-equity" onclick="getProjects('no','no','Farming','no','no','backend/getprojects.php')" >Farming</a></li>
						<li><a class="" href="#project-listing"    id="deal-debt" onclick="getProjects('no','no','Food','no','no','backend/getprojects.php')" >Food</a></li>
					   <li><a class="" href="#project-listing"   id="deal-donations" onclick="getProjects('no','no','Tourism','no','no','backend/getprojects.php')">Tourism</a></li>
						<li><a class="" href="#project-listing"    id="deal-debt" onclick="getProjects('no','no','Finance','no','no','backend/getprojects.php')">Finance & Banking</a></li>
					   <li><a class="" href="#project-listing"   id="deal-donations" onclick="getProjects('no','no','Retail','no','no','backend/getprojects.php')">Retail</a></li>
					   <li><a class="" href="#project-listing"    id="deal-debt" onclick="getProjects('no','no','Technology','no','no','backend/getprojects.php')">Technology</a></li>
					   <li><a class="" href="#project-listing"   id="deal-donations" onclick="getProjects('no','no','Manufacturing','no','no','backend/getprojects.php')">Manufacturing</a></li>
					   <li><a class="" href="#project-listing"   id="deal-donations" onclick="getProjects('no','no','Energy','no','no','backend/getprojects.php')">Electricity & Energy</a></li>
					    <li><a class="" href="#project-listing"   id="deal-donations" onclick="getProjects('no','no','Community','no','no','backend/getprojects.php')">Health & Community</a></li>
					</ul>

				</div><!-- end .form-container -->
				
				
            </div><!-- #primary -->
			
			<button id="project-category-popup" style="float:right;margin:100px 10px">Category</button>
			<button type="button" onclick="moreprojects();" style="float:right;margin-top:100px;">more</button>
			
			<h1 ><span style="margin-top:20px">Explore Companies</span></h1>
			
			 <div class="portfolio-listing clearfix" id="companies-listing">
				
				<div class="left-column" id="company-left-column" >
					<div class="home-project show "  >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class=" portfolio-list wow fadeInUp" data-wow-delay="0.25s" style="-webkit-animation-delay: 2s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/farming.jpg">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >farming</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
					
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="0.5s" style="visibility: hidden; -webkit-animation-delay: 0.5s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/finance.jpg">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >finance</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
									
				</div>
				
				<div class="center-column" id="company-center-column">
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="1s" style="visibility: hidden; -webkit-animation-delay: 1s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/retail.jpg">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >retail</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
					
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#' " class="portfolio-list wow fadeInUp" data-wow-delay="1.25s" style="visibility: hidden; -webkit-animation-delay: 1.25s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/community.jpg">
							</div>
							
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >community</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
									
				</div>
				
				<div class="right-column" id="company-right-column">
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="1.75s" style="visibility: hidden; -webkit-animation-delay: 1.75s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/health.jpg">
							</div>
						
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >health</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
					
					<div class="home-project show " >
						<div onclick="location.href='http://www.Afroglobalinvestors.com/#'" class="portfolio-list wow fadeInUp" data-wow-delay="2s" style="visibility: hidden; -webkit-animation-delay: 2s; -webkit-animation-name: none;width:100%;cursor:pointer">
							<a class="portfolio-overlay" href="http://www.Afroglobalinvestors.com/?page_id=86"><span>+</span></a>
							<div class="portfolio-image">
								<img src="./images/mainpage/mining.jpg">
							</div>
						
						</div>
						<div class="blog-excerpt" >
							<h3>Show your project</h3> <h4 class="posted-date" >mining</h4>
							Make your project known to the world<br><br>
						</div>
					</div>
				
				</div>
				
				<div class="jquery-popup" id="company-category-dialog" title="select a category" style="">
				
					<input id="company-close-category" type="button" value="x" onclick="" style="padding:1px;text-size:12px;float:right;colour:#111">
					
					<!--getProjects(getsubmitted,getcompanies,getcategory,getarticles,company,url)
						getProjects("yes","yes","no","no","no","backend/getprojects.php");					-->
					
					<ul class="category-nav" id="company-category-nav" style="margin-top:20px" >
						<li><a class="" href="#companies-listing" id="deal-equity" onclick="getProjects('no','yes','yes','no','no','backend/getprojects.php')" >All</a></li>
						<li><a class="" href="#companies-listing"  id="deal-equity" onclick="getProjects('no','no','Education','no','yes','backend/getprojects.php')" >Education</a></li>
						<li><a class="" href="#companies-listing"    id="deal-debt" onclick="getProjects('no','no','Crafts','no','yes','backend/getprojects.php')">Crafts</a></li>
					   <li><a class="" href="#companies-listing"   id="deal-donations" onclick="getProjects('no','no','Mining','no','yes','backend/getprojects.php')">Mining</a></li>
					   <li><a class="" href="#companies-listing"  id="deal-equity" onclick="getProjects('no','no','Farming','no','yes','backend/getprojects.php')" >Farming</a></li>
						<li><a class="" href="#companies-listing"    id="deal-debt" onclick="getProjects('no','no','Food','no','yes','backend/getprojects.php')" >Food</a></li>
					   <li><a class="" href="#companies-listing"   id="deal-donations" onclick="getProjects('no','no','Tourism','no','yes','backend/getprojects.php')">Tourism</a></li>
						<li><a class="" href="#companies-listing"    id="deal-debt" onclick="getProjects('no','no','Finance','no','yes','backend/getprojects.php')">Finance & Banking</a></li>
					   <li><a class="" href="#companies-listing"   id="deal-donations" onclick="getProjects('no','no','Retail','no','yes','backend/getprojects.php')">Retail</a></li>
					   <li><a class="" href="#companies-listing"    id="deal-debt" onclick="getProjects('no','no','Technology','no','yes','backend/getprojects.php')">Technology</a></li>
					   <li><a class="" href="#companies-listing"   id="deal-donations" onclick="getProjects('no','no','Manufacturing','no','yes','backend/getprojects.php')">Manufacturing</a></li>
					   <li><a class="" href="#companies-listing"   id="deal-donations" onclick="getProjects('no','no','Energy','no','yes','backend/getprojects.php')">Electricity & Energy</a></li>
					    <li><a class="" href="#companies-listing"   id="deal-donations" onclick="getProjects('no','no','Community','no','yes','backend/getprojects.php')">Health & Community</a></li>
			
					</ul>

					</div><!-- end .form-container -->
				</div><!-- end header-login -->
				
				<button id="company-category-popup" style="float:right;margin:100px 10px" >Category</button>
				<button type="button" onclick="morecompanies();" style="float:right;margin-top:100px;">more</button>
						
				
            </div><!-- #primary -->
			
			
        </div>
		
		<div id="go-top" style="display:none;">
			<a href="#">
				<i class="fa fa-angle-up"> </i>
			</a>
		</div>
    </section>
	
	<script type="text/javascript">
		var projects = [new Array(),0,0,0];
		var companies = [new Array(),0,0,0];
		var categoryprojects = [new Array(),0,0,0,"category"];
		var categorycompanies = [new Array(),0,0,0,"category"];
		var articles = [new Array(),0,0,0];
		
		
		//testing edit sitemap
		/*Position: Description
		0: the array of projects
		1: the projects we have shown in the index page from the array
		2: the column we are on
		3: the position of pictures in column
		category: the category of array currently being held
		*/
			
		//alert('in script');
		getProjects("yes","yes","no","no","no","backend/getprojects.php");
		
		//get articles after the projects and companies so they dont interfere with each other
		getProjects("yes","no","no","yes","no","backend/getarticles.php");
		
		
		function getProjects(getsubmitted,getcompanies,getcategory,getarticles,company,url) {
			//alert('in proj');
			(function (getsubmitted,getcompanies,getcategory,getarticles,company,url) {$.ajax({ //get all project names so there are no repeats
				type: "POST",
				url: url,
				// removed this since it gave a parse error //dataType: 'json',
				data: {submitted: getsubmitted,companies:getcompanies,category:getcategory,iscompany:company},   //got to put this value dynamically

				success: function (obj, textstatus) { 
					  //alert("success");
					  //var images_tags = new Array();
					  //alert("projects "+obj);
					  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
					  //alert("the parsed: "+parsed['projects']);
					  if (getarticles == "no") {
						  if (getsubmitted == "yes") {
							
							if (getcategory != "no"){
								projects = [new Array(),0,0,0];
							}
							
							projects[0] = parsed['projects'];
							//fillprojects(projects[0],projcolnumber,projcolposition,projposition,"project-listing");
							fillprojects(projects,"project-listing",getarticles,'backend/getProject.php',3);
						  }
						  
						  if (getcompanies =="yes") {
						  
							if (getcategory != "no"){
								companies = [new Array(),0,0,0];
							}
							
							 companies[0] = parsed['companies'];
							 //alert(companies);
							 //fillprojects(companies[0],cocolnumber,cocolposition,coposition,"companies-listing");
							 fillprojects(companies,"companies-listing",getarticles,'backend/getProject.php',3);
						  }
						  
						  if (getcategory != "no"){
							//alert('getcat if');
							if (company == "no"){
								//alert('to fill proj!!');
								projects = [new Array(),0,0,0];
								projects[0] = parsed['category'];
								fillprojects(projects,"project-listing",getarticles,'backend/getProject.php',3);

							} else{
								companies = [new Array(),0,0,0];
								companies[0] = parsed['category'];
								fillprojects(companies,"companies-listing",getarticles,'backend/getProject.php',3);
							}
							
						  }
					  } else{
						//alert("fill with articles!!");
						articles[0] = parsed['articles'];
							//fillprojects(projects[0],projcolnumber,projcolposition,projposition,"project-listing");
						fillprojects(articles,"articles-listing",getarticles,'backend/getArticle.php',3);
					  
					  }

					
					  //alert(parsed['images'].length);
					  //alert(parsed['projects']);  
				},
				
				error: function(xhr, textStatus, error){
					  alert(textStatus);
					  /* console.log(textStatus);
					  console.log(error); */
				}
			}); } (getsubmitted,getcompanies,getcategory,getarticles,company,url));
			
		}
		
		function moreprojects(){
			//alert('in moreprojs '+projects[1]);
			fillprojects(projects,'project-listing','no','backend/getProject.php',6);
		}
		
		function morecompanies(){
			fillprojects(companies,'companies-listing','no','backend/getProject.php',6);
		}
		
		function fillprojects(projects,listing,getarticles,url,number){
		
		//function fillprojects(projects,projectsposition - 0,columnnumber - 1,columnposition - 2,listing,getarticles,url) *prototype
		
			var i = 0;
			//alert("fill proj "+projects[0].length);
			var iterate = Math.min(projects[0].length - projects[1],number);
			
			for (i=0;i<iterate;i++){
				//alert("fill proj "+projects[0][projects[1]]);
				//alert("i iter "+ i);
				
				(function (url,getarticles) {$.ajax({
					type: "POST",
					url: url,
					// removed this since it gave a parse error //dataType: 'json',
					data: {projectname: projects[0][projects[1]],articlename: projects[0][projects[1]]},

					success: function (obj, textstatus) { 
						  //alert("success proj det");
						  //var images_tags = new Array();
						  //alert("article taken "+obj);
						  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
						  //alert('the p '+parsed['project'][0]['projectname']);
						  if (getarticles != "yes"){
							
							loadproject(parsed,projects[3],projects[2],listing);
						  } else {
							loadarticle(parsed,projects[3],projects[2],listing);
						  }
						  	  
						if (projects[2] == 2){
							projects[2] = 0;
							projects[3] ++;
						}else {
							//alert("inc col no");
							projects[2] ++;
						}
						   
					},
					
					error: function(xhr, textStatus, error){
						  alert("the err "+error);
						  /* console.log(textStatus);
						  console.log(error); */
					}
				});
				}(url,getarticles));
				
				projects[1]++;
			}
		}
		
		function loadproject(parsed,colposition,colnumber,listing){
			
			var projectname = parsed['project'][0]['projectname'];
			var projectcategory = parsed['project'][0]['projectcategory'];
			var projecttag = parsed['project'][0]['projecttag'];
			var projectleader = parsed['project'][0]['projectleader'];
			var projectcountry = parsed['project'][0]['projectcountry'];
			var projectregion = parsed['project'][0]['projectregion'];
			var company = parsed['project'][0]['company'];
			
			//alert('in load '+projectname+' cpos '+colposition+' cnom '+colnumber);
			var mainimg = "projects/"+projectname+"/images/mainimage";
			var images = new Array();
			var count = 0;
			var fileextension = [".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF"];
			
			//alert("load proj "+projectname);
			
			$.ajax({
				//This will retrieve the contents of the folder if the folder is configured as 'browsable'
				url: mainimg,
				success: function getimages(data) {
					var extensions = "";
					for (i=0;i<fileextension.length;i++){
						extensions = extensions +"a[href$='"+ (fileextension[i])+ "'],";
					}
					
					extensions = extensions.substring(0, extensions.length - 1);
					
					$(data).find(extensions).each(function () {
					//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
						var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
						//alert(this.href);
						//alert(filename);
						var filename = mainimg + "/" + filename;
						//alert("got main image: "+filename);
						images[count] = new Image();
						images[count].src = filename;
						
						count++;
					});
					
					putimage(parsed);
					
				}
			});

			
			function putimage(parsed) {
			
				//alert("put img "+projectname);
				//**********main a *******************
				var a = document.createElement("div");
				a.setAttribute("class","home-project");
				a.setAttribute("data-wow-delay","0.5s");
				a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=50&project="+projectname);
								
				//********************innera having the picture e.t.c *************************
				var innera = document.createElement('div');
				innera.setAttribute('onclick','location.href=\'http://www.Afroglobalinvestors.com/?page_id=50&project=\''+projectname);
				innera.setAttribute('class','portfolio-list wow fadeInUp');
				innera.setAttribute('style','-webkit-animation-delay: 2s; -webkit-animation-name: none;width:100%;cursor: pointer;');
				innera.setAttribute('data-wow-delay','2s');
				
				//alert('inner a 1');
				var overlay = document.createElement('a');
				overlay.setAttribute('class','project-overlay');
				overlay.setAttribute('style','width:100%;');
				overlay.innerHTML = "<span>created by: "+projectleader+"</span><br><span>located in: "+projectcountry+"</span>"+"<br><br><p>"+projecttag+"</p>";
				
				if (company == "no")
					overlay.setAttribute('href','http://www.Afroglobalinvestors.com/?page_id=50&project='+projectname);
				else
					overlay.setAttribute('href','http://www.Afroglobalinvestors.com/?page_id=101&project='+projectname);
				
				innera.appendChild(overlay);
				
				var image = document.createElement('div');
				image.setAttribute('class','portfolio-image');
				
				var img = document.createElement('img');
				img.setAttribute("src", images[0].src);
				
				//alert('inner a 2 '+images[0].src);
				image.appendChild(img);
				innera.appendChild(image);
				
				a.appendChild(innera);
				
				//**************************end of innera ************************
				
				//***********************create the info to the project **********
				var imgDiv = document.createElement("div");
				imgDiv.setAttribute("class","blog-image");
				
				//alert('outer a 2');
				//create the text div
				var txtDiv = document.createElement("div");
				txtDiv.setAttribute("class","blog-excerpt");
				 //alert(parsed['project']);
				txtDiv.innerHTML = "<h3> "+projectname+"</h3> <h4 class=\"posted-date\">"+projectcategory+"</h4> "+projecttag+ "<br><br>";
				
				//then put everything together
				a.appendChild(imgDiv);
				a.appendChild(txtDiv);
				
				//alert('outer a');
				
				//************after finishing in project *******
					
				//the items in the index page should be replaced;
				var columns = document.getElementById(listing).children;
				//alert("col num "+columnnumber+" columns "+columns.length+" name "+columns[0].textContent);
				var column = columns[colnumber];
				
				var list = column.children;
				//alert("nodes in col "+list.length+" column "+colnumber);
				//document.getElementById("left-column").appendChild(a);
				if (list[colposition]){
					column.replaceChild(a, list[colposition]);
				}else{
					column.appendChild(a);
				}
				//document.getElementById("project-listing").replaceChild(a, list[position]);
				
				//document.getElementById("project-listing").appendChild(a);
			
			}
			
		
		}
		
		function loadarticle(parsed,colposition,colnumber,listing){
			//alert('in load article!!');
			var articlename = parsed['article'][0]['articlename'];
			var articlecategory = parsed['article'][0]['articlecategory'];
			var articletag = parsed['article'][0]['articletag'];
			var author = parsed['article'][0]['author'];
			var submitteddate = parsed['article'][0]['submitteddate'];
			
			var mainimg = "newsarticles/"+articlename+"/images/mainimage";
			var images = new Array();
			var count = 0;
			var fileextension = [".jpg",".png"];
			
			//alert("load proj "+articlename);
			
			$.ajax({
				//This will retrieve the contents of the folder if the folder is configured as 'browsable'
				url: mainimg,
				success: function getimages(data) {
					
					$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
					//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
						var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
						//alert(this.href);
						//alert(filename);
						var filename = mainimg + "/" + filename;
						//alert("got main image: "+filename);
						images[count] = new Image();
						images[count].src = filename;
						
						count++;
					});
					
					putimage(parsed);
					
				}
			});
			
			
			function putimage(parsed) {
			
				//create the a element to put the thingsy in 
				var a = document.createElement("a");
				a.setAttribute("class","blog-list wow fadeInDown");
				a.setAttribute("data-wow-delay","0.5s");
				a.setAttribute("style","-webkit-animation-delay: 0.5s; -webkit-animation-name: none;width:90%;background:#fff;position:relative");
				a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename);
				
				//create the img div
				var imgDiv = document.createElement("div");
				imgDiv.setAttribute("class","blog-image");
				
				if (images[0])
					imgDiv.appendChild(images[0]);
				
				//create the text div
				var txtDiv = document.createElement("div");
				txtDiv.setAttribute("class","blog-excerpt");
				txtDiv.setAttribute("style","position:relative");
				 
				txtDiv.innerHTML = "<h3> "+articlename+"</h3> <h4 class=\"posted-date\">"+submitteddate+"</h4> "+articletag+ "<br><a class=\"blogoption\" href=\"http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename+"\">Read More&nbsp;&nbsp; </a> <br>";
				
				//then put everything together
				a.appendChild(imgDiv);
				a.appendChild(txtDiv);
				
				//the items in the index page should be replaced;
				var columns = document.getElementById(listing).children;
				//alert("col num "+columnnumber+" columns "+columns.length+" name "+columns[0].textContent);
				var column = columns[colnumber];
				
				var list = column.children;
				//alert("nodes in col "+list.length+" column "+colnumber);
				//document.getElementById("left-column").appendChild(a);
				column.replaceChild(a, list[colposition]);
					
			}
			
			//http://www.developerdrive.com/2012/03/a-simple-way-to-add-free-news-content-to-your-website/
		
		}
	
	
	</script>
 
 
    <section class="parallax-section clearfix blog_template" id="blog" >
        <div class="overlay"></div>
        <div class="mid-content" >
            <h1><span >News</span></h1>
            <div class="parallax-content">
                <div class="page-content">
                    <p style="text-align: center;">Read our latest News</p>
                </div>
            </div> 
			
			 <div class="portfolio-listing clearfix" id="articles-listing">
				
				<div class="left-column" id="company-left-column" >
					<a href="http://www.Afroglobalinvestors.com/?page_id=86" class="blog-list wow fadeInDown" data-wow-delay="0.25s" style="visibility: hidden; -webkit-animation-delay: 0.25s; -webkit-animation-name: none;width:90%;">
						<div class="blog-image">
							<img src="./images/mainpage/portfolio1.jpg" alt="Nulla pretium leo ac congue">
						</div>
						<div class="blog-excerpt">
							<h3>Interested in finance/business articles?</h3>
							<h4 class="posted-date">Afroglobalinvestors</h4>
							In this fast paced world information is the key.<br>
							<span>Read More&nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>
						</div>
					</a>
				</div>
				
				<div class="center-column" id="company-center-column" style="position:relative">
				
					<a href="http://www.Afroglobalinvestors.com/?page_id=86" class="blog-list wow fadeInDown" data-wow-delay="0.5s" style="visibility: hidden; -webkit-animation-delay: 0.5s; -webkit-animation-name: none;width:90%;">
						<div class="blog-image">
							<img src="./images/mainpage/portfolio5.jpg" alt="Pellentesque cursus dolor quis">
						</div>
						<div class="blog-excerpt">
							<h3>Post your articles here</h3>
							<h4 class="posted-date">Afroglobalinvestors</h4>
							Write about your local events in business and entrepreneurial projects. Make them known to the whole world. <br>
							<span>Read More&nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>
						</div>
					</a>
				</div>
				
				<div class="right-column" id="company-right-column" style="position:relative">
					<a href="http://www.Afroglobalinvestors.com/?page_id=86" class="blog-list wow fadeInDown" data-wow-delay="0.75s" style="visibility: hidden; -webkit-animation-delay: 0.75s; -webkit-animation-name: none;width:90%;">
						<div class="blog-image">
							<img src="./images/mainpage/portfolio7.jpg" alt="Lorem ipsum dolor">
						</div>
						<div class="blog-excerpt">
							<h3>Sign in and submit your own article!!</h3>
							<h4 class="posted-date">Afroglobalinvestors</h4>
							Investment is all about speculation and risk, tell us your predictions and forcasts, statistical analysis and economical shifts.<br>
							<span>Read More&nbsp;&nbsp;</span>
						</div>
					</a>
				</div>
				
			</div><!-- end header-login -->
				
          
            <div class="clearfix btn-wrap">
                <a class="btn" href="http://www.Afroglobalinvestors.com/?page_id=81">More</a>
            </div>
        </div>
    </section>
	
	<div class="previous" id="previous" style="display:block;">
		<a href="#page" title="scroll up">
			<img src="images/up.png"></img>
		</a>
	
	</div>
	
	<div class="next" id="next" style="display:block;">
		<a href="#page" title="scroll down">
			<img src="images/down.png"></img>
		</a>
	</div>
	
	
<link rel="stylesheet" id="dashicons-css" href="<?php echo get_template_directory_uri(); ?>/css/dashicons.min.css" type="text/css" media="all">
<link rel="stylesheet" id="admin-bar-css" href="<?php echo get_template_directory_uri(); ?>/css/admin-bar.min.css" type="text/css" media="all">
<link rel="stylesheet" id="wpsr-socialbuttons-css" href="<?php echo get_template_directory_uri(); ?>/css/wp-socializer-buttons-css.css" type="text/css" media="all">
<link rel="stylesheet" id="google-fonts-css" href="<?php echo get_template_directory_uri(); ?>/css/css(1)" type="text/css" media="all">
<link rel="stylesheet" id="accesspress_parallax-font-awesome-css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" type="text/css" media="all">
<link rel="stylesheet" id="accesspress_parallax-bx-slider-css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" type="text/css" media="all">
<link rel="stylesheet" id="accesspress_parallax-nivo-lightbox-css" href="<?php echo get_template_directory_uri(); ?>/css/nivo-lightbox.css" type="text/css" media="all">
<link rel="stylesheet" id="accesspress_parallax-animate-css" href="<?php echo get_template_directory_uri(); ?>/css/animate.css" type="text/css" media="all">
<link rel="stylesheet" id="accesspress_parallax-style-css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" media="all">
<link rel="stylesheet" id="accesspress_parallax-responsive-css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" type="text/css" media="all">

<script>
    jQuery(document).ready(function($){
       wow = new WOW(
        {
          offset:  200 
        }
      )
      wow.init();
    });
</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/admin-bar.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/wp-socializer-bookmark-js.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/wow.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/SmoothScroll.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/parallax.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/jquery.scrollTo.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/jquery.localScroll.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/jquery.nav.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/jquery.bxslider.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/jquery.easing.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/jquery.fitvids.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/jquery.actual.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/nivo-lightbox.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/skip-link-focus-fix.js"></script>




</div>
<?php get_footer(); ?>