<?php /* Template Name: ProjectDetails */ ?>
<?php get_header(); ?>

<?php if (login_check($mysqli) == true) : ?>

<div class="wrapper clearfix">
	<nav class="main-nav">
		<ul class="nav">
		
			<?php if (isset($_SESSION["company"])) : ?>
				<?php $create = "Company"; ?>
				<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=40">Company Basics</a></li>
				<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=43">Company Details</a></li>
				
				<button onclick="deleteproject()" style="" name="delete" title="delete the project" id="delete">delete</button>
				
				<a href="<?php echo "http://www.Afroglobalinvestors.com/?page_id=101&project=".$_SESSION["project"]; ?>" class="a-button" style="float:right;" name="save" id="view" onclick="">view</a>
				
			<?php else : ?>
				<?php $create = "Project"; ?>
				<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=40">Project Basics</a></li>
				<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=43">Project Details</a></li>
				<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=48">Project Funding</a></li>
				
			<?php endif; ?>
			
			<?php if (isset($_SESSION["project"])) : ?>
				<button style="float:right;" name="save" id="save" type="button" form="searchform" onclick="return    projdformhash(document.getElementById('searchform'),
																			   document.getElementById('searchform').funding,'<?php echo $_SESSION["project"]; ?>');">Save</button>
			<?php else : ?>
				<button name="save" id="save" type="submit" form="searchform" onclick="createproject()">save</button>
			<?php endif; ?>
		
		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget">
			<div class="box alert">
				<p>Please fill in below</p>
			</div>
			
			<?php if (isset($_SESSION["post-response"])) : ?>
				<div class="saveinfo" id="saveinfo" > <p id = "response"><?php echo $_SESSION["post-response"];
					unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
				</div>
			<?php else : ?>
			<?php endif; ?>
			
			<form  id="searchform" enctype="multipart/form-data">
			
			<h4 class="widget-title">Upload images</h4>
			
			<fieldset id="uploads-container"  class="entry-field">
				<!-- the div with pictures to be uploaded -->
				<div id="selectedFiles" style="width:100%;display:none;" >
					<div id="uploaded-files" style="display:inline-block"></div>
					<div id="upload-files" style="display:inline-block" ></div>
				</div>
				
				<p id="edit-image" ></p>
				<textarea id="main-image" name = "main-image" style="display: none" ></textarea>
				
				
				<div id="uploads" >
					<div class="selectedImage" id="selectedImage">
					
						<textarea  name="image-tag" placeholder="Enter a short story of the picture...." id="image-tag" class="image-tag" style="width:100%; height: 50px;margin-top:10px;" maxlength="255" ></textarea>
						
						
					</div>
								
				</div>
				
				 <input class="upload" type="file" name="fileToUpload[]" id="fileToUpload" onchange="readURL(this)" style="float:left" multiple>
				 
			</fieldset>
			
			<h4 class="widget-title">Tell us more about your <?php echo $create; ?></h4>
			
			<fieldset class="entry-field">
			
				<div id="wysihtml5-toolbar" >
						
					<button data-wysihtml5-command="bold" ><strong>B</strong></button>
					<button data-wysihtml5-command="italic"><i>IT</i></button>
					<button data-wysihtml5-command="insertUnorderedList">list</button>
					<button data-wysihtml5-command="underline" class="underline">U</button>	
					<button  data-wysihtml5-command="indent" >indent</button>
					<button  data-wysihtml5-command="outdent" >outdent</button>
					<button type="button"  data-wysihtml5-command="insertImage" >image</button>
					
					<div id="insertimage" class="insertimage" style="display:none;margin-left:1px;margin-top:20px;" data-wysihtml5-dialog="insertImage">
					
					  <div class="img-in-text" style="float:left;margin:5px;">
						<label>Image:</label>
						<input data-wysihtml5-dialog-field="src" id="imageload" value="http://">
					  </div>
					  
					  
					   <div class="img-in-text" style="float:right;margin-right:15%;">
							<label>Align:</label>
						<select data-wysihtml5-dialog-field="className">
						  <option value="">default</option>
						  <option value="wysiwyg-float-left">left</option>
						  <option value="wysiwyg-float-right">right</option>
						</select>
					  </div>
					  
					  <button type="button" data-wysihtml5-dialog-action="save">OK</button>
					  <button type="button" data-wysihtml5-dialog-action="cancel">Cancel</button>
					</div>
							
				</div>
				
				<textarea class="" cols="20" id="about" name="about" rows="30" maxlength="7000"></textarea>
				
				<script>
					var editor = new wysihtml5.Editor("about", { // id of textarea element
					toolbar:      "wysihtml5-toolbar", // id of toolbar element
					stylesheets:  "./wysihtml5/stylesheet.css", //for the align classes to work
					parserRules:  wysihtml5ParserRules // defined in parser rules set 
					});
					
					
				</script>
				
			</fieldset>
			
			<h4 class="widget-title">Links to your <?php echo $create; ?></h4>
			
			<fieldset class="entry-field">
				<label for="profile_manager_name">Link1</label>
				<input type="text" value="" name="link1" id="link1" rows="20" maxlength="55"/>
				<label for="profile_manager_name">Link2</label>
				<input type="text" value="" name="link2" id="link2" size="10" maxlength="55"/>
				<label for="profile_manager_name">Link 3</label>
				<input type="text" value="" name="link3" id="link3" rows="20" maxlength="55"/>
			</fieldset>
							
			</form>
			
		</div>
		
		
		<!-- for showing the selected files for upload -->
		<script>
			
			var projectname = "<?php if(isset($_SESSION['project'])){echo $_SESSION['project'];} else{echo "" ;} ?>";
			var homeurl = "<?php echo home_url() ; ?>";
			
			var dir = "projects/"+projectname+"/images"; //pick the directory of the project
			var mainimg = "projects/"+projectname+"/images/mainimage";
			var fileextension = [".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF"];
			var images = new Array();
			var imagetags = new Array();
			var projectdetails = new Array();
			var count = 0;
			var project;
			
			//name of the main image
			var mainname = "";
		
			if (projectname != ""){
				getProjectDetails();
				
			}else{
				location.href='http://www.Afroglobalinvestors.com/?page_id=38';
			}
			
			function removealert(){
				document.getElementById("response").style.display = "none";
			}
			
			$(document).ready(function() {

				$('div:empty').hide(); 
				$('#selectedImage').hide();
				
				$("#image-tag").on("keypress keyup keydown", function() {
					saveImageText();
				});
			
			});
			
			
			
			/* *************************CODE FOR GETTING ALREADY PICTURES and DETAILS *************************** */
			
			function getProjectDetails(){
					$.ajax({
						type: "POST",
						url: 'backend/getProject.php',
						data: {projectname: projectname},

						success: function (obj, textstatus) { 
							  projectdetails = jQuery.parseJSON(obj);//JSON.parse(obj);
							  
							  fillform(projectdetails);
							   
						},
						
						error: function(xhr, textStatus, error){
							  alert("the err "+error);
						}
					});
				
				$.ajax({
					url: mainimg,
					success: function getimages(data) {
						var extensions = "";
						for (i=0;i<fileextension.length;i++){
							extensions = extensions +"a[href$='"+ (fileextension[i])+ "'],";
						}
						
						extensions = extensions.substring(0, extensions.length - 1);
						
						$(data).find(extensions).each(function () {
						//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );

							mainname = filename;
							var filename = mainimg + "/" + filename;

							images[count] = new Image();
							images[count].src = filename;
							
							count++;
							
						});
						
					}
				});
				
				$.ajax({

					url: dir,
					success: function getimages(data) {
						//Select all <a> elements with a href attribute that ends with ".org":
						//$("a[href$='.org']")
						var extensions = "";
						for (i=0;i<fileextension.length;i++){
							extensions = extensions +"a[href$='"+ (fileextension[i])+ "'],";
						}
						
						extensions = extensions.substring(0, extensions.length - 1);
						
						$(data).find(extensions).each(function () {
						//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
							var filename = dir + "/" + filename;

							images[count] = new Image();
							images[count].src = filename;
							count++;
							
						});
						
						getimagetags();
					}
					
				});
				
				 
				function getimagetags() { 
					$.ajax({
						type: "POST",
						url: 'backend/project_images.php',

						data: {projectname: projectname},

						success: function (obj, textstatus) { 
							  imagetags = jQuery.parseJSON(obj);//JSON.parse(obj);

							  imagesComplete(imagetags);
							   
						},
						
						error: function(xhr, textStatus, error){
							  alert("the err "+error);
						}
					});
				};
			
			}
			
			function fillform(parsed){
				editor.setValue(parsed['project'][0]['projectabout']);
				
				document.getElementById("link1").value = parsed["project"][0]["link1"];
				document.getElementById("link2").value = parsed["project"][0]["link2"];
				document.getElementById("link3").value = parsed["project"][0]["link3"];
				
				getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
			
			}
			
			function imagesComplete(parsed){
			
				if (images.length > 0){ //check to see if there are images to put
					$('#selectedFiles').show();
					$('#upload-files').show();
					$('#uploaded-files').show();
					
					$('#selectedImage').show();
				}
				
				for (i=0;i<images.length;i++){
					
					var imagetag = "";
					var imagename = images[i].src.substring (images[i].src.lastIndexOf ( '/' ) + 1 );
					
					if (parsed['images'][imagename] != undefined){
						imagetag = parsed['images'][imagename];
					}
					
					putimage(imagename,imagetag,images[i].src,"uploaded-files");
				}
				
				//then set the main image if it is there.
				if (mainname != "") {
					setmain (mainname);
					
				}
			
			}
			
			/* *************************CODE FOR GETTING ALREADY PICTURES *************************** */
			
			function checkSelectedFiles(){
				
				if ($('#upload-files').is(':empty') && $('#uploaded-files').is(':empty')){
					$('#selectedFiles').hide();
					document.getElementById('edit-image').innerHTML="";
					
					$('#selectedImage').hide();
				}
			};
			
			
			function saveImageText()
			{
				//check that the edit-image p is not null
				var imageName = document.getElementById('edit-image').innerHTML;
				var textName = imageName.substr(0, imageName.lastIndexOf('.'));
				
				if (imageName)
				{//save the text into the textArea
					document.getElementById("text-"+textName).value = document.getElementById('image-tag').value;
				}
				
				return false;
			
			}
			
			function createproject(){
				
				location.href = 'http://www.Afroglobalinvestors.com/?page_id=40' ;
			}
			
			 function readURL(input) {
				var i;
				
				if (input.files && input.files[0]) {
				
					$('#selectedFiles').show();
					$('#upload-files').show();
					$('#uploaded-files').show();
					
					//empty the files
					document.getElementById('upload-files').innerHTML ="";
					
					$('#selectedImage').show();
					
					var count = 0;
					
					for(i=0;i<input.files.length;i++)
					{
						
						var reader = new FileReader();
						
						reader.fileName = input.files[count].name;
						
						count++;
						
						//note the name must be taken straight fromt the e somehow
						reader.onload = function (e) {							
							
							if (!document.getElementsByName(e.target.fileName)[0]){
								putimage(e.target.fileName,"",e.target.result,"upload-files");
							}
							
							
						};

						reader.readAsDataURL(input.files[i]);
					}
					
				}
			}
			
			function setmain(mainname){ // mainly done for the main image button
				var btn = document.getElementsByName("main-"+document.getElementById("main-image").value);
				btn[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");

				//make it blue background and remove the recent one
				var btnmain = document.getElementsByName("main-"+mainname);
				btnmain[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;background-color:#0000FF");
				
				document.getElementById("main-image").value = mainname;
			
			}
			
			function putimage(name,imagetag,src,container){

				var mainbtn = document.createElement("button");
				mainbtn.setAttribute("class","img-main");
				mainbtn.setAttribute("name","main-"+name);
				mainbtn.setAttribute("type","button");
				mainbtn.setAttribute("title","set this image as main project image");
				mainbtn.innerHTML = "M";
				
				//the main image is the first image by default
				if ($('#upload-files').is(':empty') && $('#uploaded-files').is(':empty')){
					document.getElementById("main-image").value = name;
					//the background color must be blue
					mainbtn.setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;background-color:#0000FF");
					
				} else{
					mainbtn.setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
				}
				
				mainbtn.onclick = function (event) {
					var filename = event.target.name;

					var textname = filename.substr(filename.indexOf('-')+1,filename.length);

					//remove blue from the other main image
					var btn = document.getElementsByName("main-"+document.getElementById("main-image").value);
					btn[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
					
					//make it blue background and remove the recent one
					var btnmain = document.getElementsByName(filename);
					btnmain[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;background-color:#0000FF");
					
					document.getElementById("main-image").value = textname;
					
				}
				
				var cancelbtn = document.createElement("button");
				cancelbtn.setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:0px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
				cancelbtn.setAttribute("class","img-cancel");
				cancelbtn.setAttribute("name","cancel-"+name);
				cancelbtn.setAttribute("type","button");
				cancelbtn.setAttribute("title","remove from upload");
				cancelbtn.innerHTML = "X";
				
				cancelbtn.onclick = function (event) {

					var filename = event.target.name;

					var textname = filename.substr(filename.indexOf('-')+1,filename.length);

					//put the items in the delimages array
					var input = document.createElement("input");
					input.setAttribute("name","delimages[]")
					input.style.display = "none";
					
					input.value = textname;
					document.getElementById("uploads").appendChild(input);
					
					var image = document.getElementsByName("div_"+textname);

					image[0].parentNode.removeChild(image[0]);
					
					
					//then delete the elements of the image
					var textArea = document.getElementById("text-"+textName);

					textArea.parentNode.removeChild(textArea);
					
					checkSelectedFiles();
					
					return false;
				}
				
				//create the div that will hold the image and buttons
				var imgdiv = document.createElement("div");
				imgdiv.setAttribute("style","position:relative;display: inline-block;");
				imgdiv.setAttribute("name","div_"+name);
				imgdiv.setAttribute("height", "130");
				imgdiv.setAttribute("width", "130");
				
				// create the image element to put the picture
				var elem = document.createElement("img");
				
				elem.setAttribute("src", src);
				elem.setAttribute("height", "130");
				elem.setAttribute("width", "130");
				elem.setAttribute("style", "padding: 5px");
				elem.setAttribute("class", "img-upload");
				elem.setAttribute("name",name);
				
				
				var fileName = name;
				var textName = fileName.substr(0, fileName.lastIndexOf('.'));
				
				//also create a hidden textarea to put the tag of the picture
				var text = document.createElement("textarea");

				text.id = "text-"+textName;
				text.name = "imagetext["+fileName+"]";
				text.value = imagetag;
				
				elem.onclick = function (event) {
					var filename = event.target.name;
					
					//remove border from previous selected img
					var imagediv = document.getElementsByName("div_"+document.getElementById("edit-image").innerHTML)[0];
					if (imagediv)
						imagediv.setAttribute('class','');
					
					//put a border on the selected picture
					selecteddiv = document.getElementsByName("div_"+filename)[0];
					selecteddiv.setAttribute('class','selected-img-div');
					
					document.getElementById("edit-image").innerHTML = filename;
					var textName = fileName.substr(0, fileName.lastIndexOf('.'));
					
					var textArea = document.getElementById("text-"+textName);
					document.getElementById('image-tag').value = textArea.value;
				
					//put the url of the pic in the use pic
					var index = window.location.href.lastIndexOf("/",window.location.href.length);
					var baseurl = window.location.href.substr(0,index);
					
					
					if (filename == document.getElementById('main-image').value){
						document.getElementById('imageload').value = baseurl+"/"+dir+"/mainimage/"+filename;
					}else{
						document.getElementById('imageload').value = baseurl+"/"+dir+"/"+filename;
					}
					
					
				};
				
				imgdiv.appendChild(elem);
				imgdiv.appendChild(cancelbtn);
				imgdiv.appendChild(mainbtn);

				document.getElementById(container).appendChild(imgdiv);
				document.getElementById("upload-files").appendChild(text);
				document.getElementById("text-"+textName).style.display = "none";
				
			}
			
			function deleteproject(){
				
				if (projectname != ""){
					var r = confirm("You are about to delete the project "+projectname );
					if (r == true) {
						
						$.ajax({
							type: 'POST',
							url: "./backend/deleteProject.php",
							data: {project:projectname},
							async: false,
							success: function (obj, textstatus) {
								parsed = jQuery.parseJSON(obj);
								
								deleteresponse(parsed);
								
							}
							
						});
						
					}
				}

			}
			
			function deleteresponse(parsed){
			
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {response:parsed['message'],unset:"project"},
					async: false,
					success: function (obj, textstatus) {
						//alert(parsed["message"]);
						location.href='http://www.Afroglobalinvestors.com/?page_id=108';
					},
					
				});
				
			}
			
		</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar sb-right">
		
		
		<div class="sb-widget"><h4 class="widget-title" >Images</h4>
			<ul class="nav">
				<li>Images makes your <?php echo $create; ?> interesting, give this your best shot!!</li>
				<li>Give a short story of the picture, make us relate to your <?php echo $create; ?></li>
				<li>Click on any of the uploaded images and use them to tell us more about your <?php echo $create; ?></li>
				<li> - First click 'image' in tell us more section <br> - Then click on any uploaded image <br> - The image url will be put in the url box<br> - You can use the image in your text by pressing ok</li>
				
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:55px" >More about the <?php echo $create; ?></h4>
			<ul class="nav">
				<li>This is the information funders will see on your <?php echo $create; ?> page</li>
				<li>Explain in greater detail what your <?php echo $create; ?> is all about</li>
				<li>Make use of the rich text (Bold, underline etc) to make it nice</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" ">Links</h4>
			<ul class="nav">
				<li>A website or youtube video</li>
			</ul>
		</div>
		
		<div class="sb-widget" id="sidebar-right" >
			<h4 style="margin-left:1px"><i>Featured Companies </i></h4>
			<div id="sidebar-right-companies"> </div>
			
		</div>
		
	</aside>
	
	<div class="previous" id="previous" style="display:block;">
		<a href="#page" title="scroll up">
			<img src="images/up.png"></img>
		</a>
	
	</div>
	
	<div class="next" id="next" style="display:block;">
		<a href="#page" title="scroll down">
			<img src="images/down.png"></img>
		</a>
	</div>
	
	
</div>

<?php get_footer(); ?>

<?php else : ?>
 
	<div class="saveinfo" id="saveinfo" style="margin-top:50px;">
		<p id = "response" style="font-size:18px;">
			You are not authorized to access this page. Please login first
		</p> 
	</div>
	
<?php endif; ?>