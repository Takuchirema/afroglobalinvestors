<?php /* Template Name: HITProject */ ?>
<?php 
include_once("analyticstracking.php");
get_header(); ?>
<div class="wrapper clearfix">
	<nav class="main-nav">
		<ul class="nav">
			
			<li class=""><a href="http://http://www.Afroglobalinvestors.com/?page_id=86">Create your profile</a></li>
			<li class="active"><a href = "http://www.Afroglobalinvestors.com/?page_id=88">Create your project</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=90">Create your company</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=92">Write an article</a></li>


		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget how-it-works">
			<h4>Follow these basic steps in creating your project</h4>
			
			<div class="steps">
				<div class="step-number"><span>1</span></div>
				<p>Enter all your project basics, guidelines are on the right</p>
				<img src="images/projectbasics.PNG"></img>
			</div>
			
			<div class="steps">
				<div class="step-number"><span>2</span></div>
				<p>Fill in and <strong style="color:#800000">save</strong> your project details</p>
				<img src="images/projectdetails.PNG"></img>
			</div>
			
			<div class="steps">
				<div class="step-number"><span>3</span></div>
				<p>Fill in funding details, <strong style="color:#800000">SAVE</strong> and submit </p>
				<img src="images/projectfunding.PNG"></img>
			</div>
		
		<script type="text/javascript">				
		</script>

				
	</section>
	
	<aside class="sidebar sb-right">
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:40px" >Project basics</h4>
			<ul class="nav">
				<li>Enter a name that best decribes your project <br><a style="color:#800000;font-weight:bold">*NB The  name cannot be changed.</a></br></li>
				<li>Look for availability of the  name (right corner of entry box)</li>
				<li>All other instructions and tips will be on the right hand side as you fill in</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:150px">Project Details</h4>
			<ul class="nav">
				<li>This is what funders will see of your on your project site</li>
				<li>Upload the images and use them in your project description</li>
				<li><a style="color:#800000;font-weight:bold">*NB To upload multiple files, select them all at once. You can put them in one folder then drag your mouse across them, or use control select</a></br></li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:215px" >Save and submit </h4>
			<ul class="nav">
				<li>Do not forget to save/edit before submitting</li>
				<li>A project can also be deleted before it is submitted after which it cannot be deleted</li>
				<li>View the project to proof read it</li>
			</ul>
		</div>
		
		
		
	</aside>
	
	
</div>
<?php get_footer(); ?>