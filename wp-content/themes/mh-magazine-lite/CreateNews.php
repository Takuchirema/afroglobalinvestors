<?php /* Template Name: CreateNews */ ?>
<?php get_header(); ?>

<?php if (login_check($mysqli) == true) : ?>

<div class="wrapper clearfix" style="padding-right: 25px; padding-left:100px; padding-top:100px;background: #fff;">
	<nav class="main-nav" style="width:1125px;">
		<ul class="nav">
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=73">News Article</a></li>
			
			<button onclick="deletearticle()" style="" name="delete" title="delete the project" id="delete">delete</button>
			
			<a class="a-button" href="<?php echo 'http://www.Afroglobalinvestors.com/?page_id=78&article='.$_SESSION['article']; ?>" name="view" id="view" type="button" >view</a>
			
			<button style="" name="submit" id="submit" type="button" onclick="submitarticle();">submit</button>

			<?php if (isset($_SESSION["article"])) : ?>
				<button name="save" id="save" type="button" form="searchform" onclick="return 							newsformhash(document.getElementById('searchform'),
																				   document.getElementById('searchform').title,
																				   document.getElementById('searchform').category,'true');">Save</button>
				
			<?php else : ?>
				<button style="" name="save" id="save" type="button" form="searchform" onclick="return 							newsformhash(document.getElementById('searchform'),
																				   document.getElementById('searchform').title,
																				   document.getElementById('searchform').category,'false');">Save</button>
			
			<?php endif; ?>
			
			

		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>" style="width:800px">
	
		<div class="entry sb-widget">
			<div class="box alert">
				<p>Please fill in below</p>
			</div>
			
			<?php if (isset($_SESSION["post-response"])) : ?>
				<div class="saveinfo" id="saveinfo" > <p id = "response"><?php echo $_SESSION["post-response"];
					unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
				</div>
			<?php endif; ?>
			
			<form  id="searchform" enctype="multipart/form-data">
			
			<h4 class="widget-title">News Article Title</h4>
			
			<fieldset class="entry-field">
				<input type="text" value="" name="title" id="title" rows="20" onchange="handler"  maxlength="55" />		
				<p class="check-article" name="check-article" id="check-article" style="float: right;margin-right:250px"></p>

			</fieldset>
			
			<h4 class="widget-title">Introductory phrase to your News Article</h4>
			
			<fieldset class="entry-field">
				<textarea class="" cols="10" id="introduction" name="introduction" rows="5" maxlength="255"></textarea>
			</fieldset>
						
			<h4 class="widget-title">Select News Article category</h4>
			
			<fieldset class="entry-field">
				<select class="autosave" id="category" name="category">
				<option value="Select">Select</option>
				<option value="Education">Education</option>
				<option value="Crafts">Crafts</option>
				<option value="Mining">Mining</option>
				<option value="Farming">Farming</option>
				<option value="Food">Food</option>
				<option value="Tourism">Tourism</option>
				<option value="Finance">Finance & Banking</option>
				<option value="Retail">Retail</option>
				<option value="Technology">Technology</option>
				<option value="Manufacturing">Manufacturing</option>
				<option value="Electricity">Electricity & Energy</option>

				</select>
				
			</fieldset>
			
			<h4 class="widget-title">Upload images</h4>
			
			<fieldset id="uploads-container"  class="entry-field">
				<!-- the div with pictures to be uploaded -->
				<div id="selectedFiles" style="width:100%;display:none;">
					<div id="uploaded-files" style="display:inline-block"></div>
					<div id="upload-files" style="display:inline-block" ></div>
				</div>
				
				<p id="edit-image" ></p>
				<textarea id="main-image" name = "main-image" style="display: none" ></textarea>
				
					
				 <input class="upload" type="file" name="fileToUpload[]" id="fileToUpload" onchange="readURL(this)" style="right:47%" multiple="true">
				 
				 <div id="image-discription" style="display:none;right:47%">
					 <textarea  name="image-tag" placeholder="Enter a short description of the picture...." id="image-tag" class="image-tag" style="width: 500px; height: 50px;margin-top:10px;right:47%" maxlength="55" ></textarea>
					 				 
				 </div>
				 
			</fieldset>
			
			<h4 class="widget-title">Tell us more about your News Article</h4>
			
			<fieldset class="entry-field" id="article-container">
			
				<div id="wysihtml5-toolbar" >
						
					<button data-wysihtml5-command="bold" ><strong>B</strong></button>
					<button data-wysihtml5-command="italic"><i>IT</i></button>
					<button data-wysihtml5-command="insertUnorderedList">list</button>
					<button data-wysihtml5-command="underline">underline</button>	
					<button  data-wysihtml5-command="indent" >indent</button>
					<button  data-wysihtml5-command="outdent" >outdent</button>
					<button type="button"  data-wysihtml5-command="insertImage" >image</button>
					<!-- <button data-wysihtml5-command="insertImage">insert image</button> -->
					
					
					<div id="insertimage" class="insertimage" style="display:none;margin-left:1px;margin-top:20px;" data-wysihtml5-dialog="insertImage">
					
					  <div class="img-in-text" style="float:left;margin:5px;">
						<label>Image:</label>
						<input data-wysihtml5-dialog-field="src" id="imageload" value="http://">
					  </div>
					  
					  
					   <div class="img-in-text" style="float:right;margin-right:15%;">
							<label>Align:</label>
						<select data-wysihtml5-dialog-field="className">
						  <option value="">default</option>
							<option value="wysiwyg-float-left">left</option>
							<option value="wysiwyg-float-right">right</option>
						</select>
					  </div>
					  
					  <button type="button" data-wysihtml5-dialog-action="save">OK</button>
					  <button type="button" data-wysihtml5-dialog-action="cancel">Cancel</button>
					</div>
							
				</div>
				
				<textarea class="" cols="20" id="about" name="about" rows="30" maxlength="7000"></textarea>
			
				<script>
					var editor = new wysihtml5.Editor("about", { // id of textarea element
					toolbar:      "wysihtml5-toolbar", // id of toolbar element
					stylesheets:  "./wysihtml5/stylesheet.css",
					parserRules:  wysihtml5ParserRules // defined in parser rules set 
					});
					
				</script>
				
			</fieldset>
							
			</form>
			
		</div>
		
		
		<!-- for showing the selected files for upload -->
		<script>
			
			var articlename = "<?php if(isset($_SESSION['article'])){echo $_SESSION['article'];} else{echo "" ;} ?>";
			
			var homeurl = "<?php echo home_url() ; ?>";
			
			var dir = "newsarticles/"+articlename+"/images"; //pick the directory of the project
			var mainimg = "newsarticles/"+articlename+"/images/mainimage";
			var fileextension = [".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF"];
			var images = new Array();
			var images_tags = new Array();
			var count = 0;
			var parsed;
			var project;
			
			//name of the main image
			var mainname = "";
			
			checkavailability();
			
		
			if (articlename != ""){
				getArticleDetails();
				
			}else{
				getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
			}
			
			function removealert(){
				document.getElementById("response").style.display = "none";
			}

			
			$(document).ready(function() {
				$('div:empty').hide(); 
				
				$("#image-tag").on("keypress keyup keydown", function() {
					saveImageText();
				});
			
			});
			
			function viewarticle() {
				location.href = 'http://www.Afroglobalinvestors.com/?page_id=78&article='+articlename;
			}
			
			function view(){
				location.href='http://www.Afroglobalinvestors.com/?page_id=78&article='+articlename;
			
			}
			
			/* *************************CODE FOR GETTING ALREADY PICTURES and DETAILS *************************** */
			
			function getArticleDetails(){
					$.ajax({
						type: "POST",
						url: 'backend/getArticle.php',
						data: {articlename: articlename},

						success: function (obj, textstatus) { 
							  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
							  
							  fillform(parsed);
							   
						},
						
						error: function(xhr, textStatus, error){
							  alert("the err "+error);
						}
					});
				
				$.ajax({
					//This will retrieve the contents of the folder if the folder is configured as 'browsable'
					url: mainimg,
					success: function getimages(data) {
						
						$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
							mainname = filename;
							var filename = mainimg + "/" + filename;
							images[count] = new Image();
							images[count].src = filename;
							
							count++;
							others();
							
						});
						
					}
				});
				
				function others() {
					$.ajax({
						//This will retrieve the contents of the folder if the folder is configured as 'browsable'
						url: dir,
						success: function getimages(data) {
							
							$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
							//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
								var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
								var filename = dir + "/" + filename;
								images[count] = new Image();
								images[count].src = filename;
								count++;
								
							});
							
							imagetags();
						}
						
					});
				}
				 
				function imagetags() { //now get the tags of the images from the database might put the jquery instead of dollar sign
					$.ajax({
						type: "POST",
						url: 'backend/article_images.php',
						data: {articlename: articlename},

						success: function (obj, textstatus) { 
							  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
							   imagesComplete(parsed);
							   
						},
						
						error: function(xhr, textStatus, error){
							  alert("the err "+error);
						}
					});
				};
			
			}
			
			function fillform(parsed){
				
				if (parsed['article'][0]['submitted'] == 'yes'){
					document.getElementById('submit').style.display='none';
					document.getElementById('delete').style.display='none';
				}
				
				document.getElementById("title").value = parsed["article"][0]["articlename"];
				document.getElementById("introduction").value = parsed["article"][0]["articletag"];
				document.getElementById("category").value = parsed["article"][0]["articlecategory"];
				editor.setValue(parsed["article"][0]["article"]);
				
				getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
			
			}
			
			function imagesComplete(parsed){
			
				if (images.length > 0){ //check to see if there are images to put
					$('#selectedFiles').show();
					$('#upload-files').show();
					$('#uploaded-files').show();
					
					
					$('#image-discription').show();
				}
			
				for (i=0;i<images.length;i++){
					
					var imagetag = "";
					var imagename = images[i].src.substring (images[i].src.lastIndexOf ( '/' ) + 1 );
					
					if (parsed['images'][imagename] != undefined)
						imagetag = parsed['images'][imagename];
					
					
					putimage(imagename,imagetag,images[i].src,"uploaded-files");
				}
				
				//then set the main image if it is there.
				if (mainname != ""){
					setmain (mainname);
				}else{
					alert('main not set');
				}
			
			}
			
			/* *************************CODE FOR GETTING ALREADY PICTURES *************************** */
			
			function checkSelectedFiles(){
				//alert('uploads changed');
				
				if ($('#upload-files').is(':empty') && $('#uploaded-files').is(':empty')){
					$('#selectedFiles').hide();
					$('#image-discription').hide();
					document.getElementById('edit-image').innerHTML="";
				}
			};
			
			
			function saveImageText()
			{
				//check that the edit-image p is not null
				var imageName = document.getElementById('edit-image').innerHTML;
				var textName = imageName.substr(0, imageName.lastIndexOf('.'));
				
				if (imageName)
				{//save the text into the textArea
					document.getElementById("text_"+textName).value = document.getElementById('image-tag').value;
				}
				
				return false;
			
			}
			
			function createproject(){
				//alert('please create a project to edit');
				
				location.href = 'http://www.Afroglobalinvestors.com/?page_id=40' ;
			}
			
			 function readURL(input) {
				var i;
				
				if (input.files && input.files[0]) {
					$('#selectedFiles').show();
					$('#upload-files').show();
					$('#uploaded-files').show();
					
					
					$('#image-discription').show();
					//empty the files
					document.getElementById('upload-files').innerHTML ="";
					
					
					var count = 0;
					
					for(i=0;i<input.files.length;i++)
					{
						
						var reader = new FileReader();
						
						reader.fileName = input.files[count].name;
						
						count++;
						
						//note the name must be taken straight fromt the e somehow
						reader.onload = function (e) {
						
							if (!document.getElementsByName(e.target.fileName)[0]){
								putimage(e.target.fileName,"",e.target.result,"upload-files");
							}
							
						};

						reader.readAsDataURL(input.files[i]);
					}
					
				}
			}
			
			function clearUploadFiles() {
				
			}
			
			function setmain(mainname){ // mainly done for the main image button
				var btn = document.getElementsByName("main-"+document.getElementById("main-image").value);
				btn[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
				
				//make it blue background and remove the recent one
				var btnmain = document.getElementsByName("main-"+mainname);
				btnmain[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;background-color:#0000FF");
				
				document.getElementById("main-image").value = mainname;
			
			}
			
			function putimage(name,imagetag,src,container){
				//first the main image button
				var mainbtn = document.createElement("button");
				mainbtn.setAttribute("class","img-main");
				mainbtn.setAttribute("name","main-"+name);
				mainbtn.setAttribute("type","button");
				mainbtn.setAttribute("title","set this image as main project image");
				mainbtn.innerHTML = "M";
				
				//the main image is the first image by default
				if ($('#upload-files').is(':empty') && $('#uploaded-files').is(':empty')){
					document.getElementById("main-image").value = name;
					//the background color must be blue
					mainbtn.setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;background-color:#0000FF");
					
				} else{
					mainbtn.setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
				}
				
				mainbtn.onclick = function (event) {
					var filename = event.target.name;
					var textname = filename.substr(filename.indexOf('-')+1,filename.length);

					//remove blue from the other main image
					var btn = document.getElementsByName("main-"+document.getElementById("main-image").value);
					//alert(btn.length);
					btn[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
					
					//make it blue background and remove the recent one
					var btnmain = document.getElementsByName(filename);
					btnmain[0].setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:25px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;background-color:#0000FF");
					
					document.getElementById("main-image").value = textname;
					
				}
				
				var cancelbtn = document.createElement("button");
				cancelbtn.setAttribute("style","position:absolute;width:24px;height:24px;float:left;right:0px;top:6px;text-align:justify;line-height:0px;font-size:15px;display:inline;padding: 5px 7px;");
				cancelbtn.setAttribute("class","img-cancel");
				cancelbtn.setAttribute("name","cancel-"+name);
				cancelbtn.setAttribute("type","button");
				cancelbtn.setAttribute("title","remove from upload");
				cancelbtn.innerHTML = "X";
				
				cancelbtn.onclick = function (event) {
					var filename = event.target.name;
					var textname = filename.substr(filename.indexOf('-')+1,filename.length);

					var input = document.createElement("input");
					input.setAttribute("name","delimages[]")
					input.style.display = "none";
					
					input.value = textname;
					document.getElementById("uploads-container").appendChild(input);
					
					var image = document.getElementsByName("div_"+textname);
					image[0].parentNode.removeChild(image[0]);
					
					
					//then delete the elements of the image
					var textArea = document.getElementById("text_"+textName);

					textArea.parentNode.removeChild(textArea);
					
					checkSelectedFiles();
					
					return false;
				}
				
				//create the div that will hold the image and buttons
				var imgdiv = document.createElement("div");
				imgdiv.setAttribute("style","position:relative;display: inline-block;");
				imgdiv.setAttribute("name","div_"+name);
				imgdiv.setAttribute("height", "130");
				imgdiv.setAttribute("width", "130");
				
				// create the image element to put the picture
				var elem = document.createElement("img");
				
				elem.setAttribute("src", src);
				elem.setAttribute("height", "130");
				elem.setAttribute("width", "130");
				elem.setAttribute("style", "padding: 5px");
				elem.setAttribute("class", "img-upload");
				elem.setAttribute("name",name);
				
				
				var fileName = name;
				var textName = fileName.substr(0, fileName.lastIndexOf('.'));
				
				//also create a hidden textarea to put the tag of the picture
				var text = document.createElement("textarea");

				text.id = "text_"+textName;
				text.name = "imagetext["+fileName+"]";
				text.value = imagetag;

				elem.onclick = function (event) {
					var filename = event.target.name;
					
					//remove border from previous selected img
					var imagediv = document.getElementsByName("div_"+document.getElementById("edit-image").innerHTML)[0];
					if (imagediv)
						imagediv.setAttribute('class','');
					
					//put a border on the selected picture
					selecteddiv = document.getElementsByName("div_"+filename)[0];
					selecteddiv.setAttribute('class','selected-img-div');
					
					document.getElementById("edit-image").innerHTML = filename;
					var textName = fileName.substr(0, fileName.lastIndexOf('.'));
					
					var textArea = document.getElementById("text_"+textName);

					document.getElementById('image-tag').value= textArea.value;
					
					var index = window.location.href.lastIndexOf("/",window.location.href.length);
					var baseurl = window.location.href.substr(0,index);
					
					if (filename == document.getElementById('main-image').value){
						document.getElementById('imageload').value = baseurl+"/"+dir+"/mainimage/"+filename;
					}else{
						document.getElementById('imageload').value = baseurl+"/"+dir+"/"+filename;
					}
										
					
				};
				
				imgdiv.appendChild(elem);
				imgdiv.appendChild(cancelbtn);
				imgdiv.appendChild(mainbtn);
				
				document.getElementById(container).appendChild(imgdiv);
				document.getElementById("upload-files").appendChild(text);
				document.getElementById("text_"+textName).style.display = "none";
				
			}
			
			var articles = new Array();
			
			function checkavailability(){
				$.ajax({ //get all project names so there are no repeats
					type: "POST",
					url: 'backend/getarticles.php',
					data: {all: 'yes'},  

					success: function (obj, textstatus) { 
						  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
						  availability(parsed);
						  
					},
					
					error: function(xhr, textStatus, error){
						  alert(textStatus);
					}
				});
				
				function availability(parsed) {
					
					if (parsed['success']==1) {
						for (i =0;i<parsed['articles'].length;i++)
						{
							articles[i] = parsed['articles'][i];
						}
					}
					
				  $("#title").on("keypress keyup keydown", function() {
						document.getElementById("check-article").innerHTML = "checking availability...";
						if (articles.indexOf(document.getElementById("title").value) > -1) {
							document.getElementById("check-article").innerHTML = "article name taken";
						} else {
							document.getElementById("check-article").innerHTML = "name is available";
						}

					});
					

				};
			}
			
			function submitarticle() {
				//********NB;;; ALWAYS CHECK IF THE ARTICLE IF SET FIRST OTHERWISE THE CODE IS GOING TO CRASH !!!!!!
				
				var articlename = "<?php if(isset($_SESSION['article'])){echo $_SESSION['article'];} else{echo "" ;} ?>";
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				
				var date = yyyy+"-"+mm+"-"+dd;
				
				$.ajax({
					type: 'POST',
					url: "./backend/submitArticle.php",
					data: {article:articlename,submitdate:date},
					async: false,
					success: function (obj, textstatus) {
						parsed = jQuery.parseJSON(obj);
						
						editSiteMap('http://www.afroglobalinvestors.com/article/'+articlename);
						
						submitresponse(parsed);
						
						location.reload();
					
					}
					
					
				});
			}
			
			function submitresponse(parsed){
		
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {response:parsed["message"]},
					async: false,
					success: function (obj, textstatus) {
						
						
						
					},
					
				});
			
			}
			
			function deletearticle(){
				
				if (articlename != ""){
					var r = confirm("You are about to delete the article "+articlename );
					
					if (r == true) {
						
						$.ajax({
							type: 'POST',
							url: "./backend/deleteArticle.php",
							data: {article:articlename},
							async: false,
							success: function (obj, textstatus) {
								parsed = jQuery.parseJSON(obj);
								
								deleteresponse(parsed);
								
							}
							
						});
						
					}
				}

			}
			
			function deleteresponse(parsed){
			
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {response:parsed['message'],unset:"article"},
					async: false,
					success: function (obj, textstatus) {
						//alert(parsed["message"]);
						location.href='http://www.Afroglobalinvestors.com/?page_id=108';
					},
					
				});
				
			}
			
						
		</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar sb-right" >
		
		
		<div class="sb-widget"><h4 class="widget-title" >Images</h4>
			<ul class="nav">
				<li>Images makes your News Article interesting, give this your best shot!!</li>
				<li>Give a short story of the picture, make us relate to your News Article</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" ">Links</h4>
			<ul class="nav">
				<li>A website or youtube video</li>
			</ul>
		</div>
		
		<div class="sb-widget" id="sidebar-right" >
			<h4 style="margin-left:1px"><i>Featured Companies </i></h4>
			<div id="sidebar-right-companies"> </div>
			
		</div>
		
	</aside>
	
	<div class="previous" id="previous" style="display:block;">
		<a href="#page" title="scroll up">
			<img src="images/up.png"></img>
		</a>
	
	</div>
	
	<div class="next" id="next" style="display:block;">
		<a href="#page" title="scroll down">
			<img src="images/down.png"></img>
		</a>
	</div>
	
	
</div>

<?php get_footer(); ?>

<?php else : ?>
 
	<div class="saveinfo" id="saveinfo" style="margin-top:50px;">
		<p id = "response" style="font-size:18px;">
			You are not authorized to access this page. Please login first
		</p> 
	</div>
	
<?php endif; ?>