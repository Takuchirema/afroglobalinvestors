
//had to put the global script call below the jquery to work I dont know why
$(document).ready(
	
	function(){

		$('#nav-login').click(function(){
					
			$( "#header-login" ).dialog({
		  dialogClass: "header-login",
		  width: 410,
		  draggable: false,
		  resizable: false,
		  modal: false,
		  open: function(event, ui) {
			clearErrorMessage();
			$(event.target).dialog('widget')
			  .css({ position: 'fixed' })
			  .position({ my: 'right+48 top+15', at: 'center top', of: "#nav-login" });
		  },
		 
		});
			return false;
		});
		
		$('#close_login').click(function(){
			//alert("clicked");
			//$("#header-login").removeClass( "header-login" );
			$(this).closest('.ui-dialog-content').dialog('close'); 
		});
		
		$('#nav-signup').click(function(){
					
			$( "#header-signup" ).dialog({
		  dialogClass: "header-signup",
		  width: 410,
		  draggable: false,
		  resizable: false,
		  modal: false,
		  open: function(event, ui) {
			clearErrorMessage();
			$(event.target).dialog('widget')
			   .css({ position: 'fixed' })
			  .position({ my: 'right+48 top+15', at: 'center bottom', of: "#nav-login" });
		  },
		 
		});
			return false;
		});
		
		$('#close_signup').click(function(){
			//alert("clicked");
			//$("#header-login").removeClass( "header-login" );
			$(this).closest('.ui-dialog-content').dialog('close'); 
		});
		
		$('#company-category-popup').click(function(){
				//alert('clicked');
				$( "#company-category-dialog" ).dialog({
				position: { my: 'left+10 top+1', at: "bottom bottom", of: "#company-category-popup" },
			  dialogClass: "header-login",
			  width: 450,
			  draggable: false,
			  resizable: false,
			  modal: false,
			  open: function(event, ui) {
				clearErrorMessage();
				$(event.target).dialog('widget')
				  .css({ position: 'fixed' })
				  
			  },
			 
			});
			return false;
		});
				
		
		$('#company-close-category').click(function(){
			//alert("clicked");
			//$("#header-login").removeClass( "header-login" );
			$(this).closest('.ui-dialog-content').dialog('close'); 
		});
		
		$('#project-category-popup').click(function(){
					
			$( "#project-category-dialog" ).dialog({
			position: { my: 'left+10 top+1', at: "bottom bottom", of: "#project-category-popup" },
		  dialogClass: "header-login",
		  width: 450,
		  draggable: false,
		  resizable: false,
		  modal: false,
		  open: function(event, ui) {
			clearErrorMessage();
			$(event.target).dialog('widget')
			  .css({ position: 'fixed' })
			  
		  },
		 
		});
			return false;
		});
		
		$('#project-close-category').click(function(){
			//alert("clicked");
			//$("#header-login").removeClass( "header-login" );
			$(this).closest('.ui-dialog-content').dialog('close'); 
		});
		
		$('#deal-equity').click(function(){
			
			//set all others to dormant
			document.getElementById("deal-equity").className = "active";
			document.getElementById("deal-debt").className = "dormant";
			document.getElementById("deal-donations").className = "dormant";
			
			//*****************************
			
			document.getElementById("equity-label").style.display = "block";
			document.getElementById("equity").style.display = "block";
			document.getElementById("equity-price-label").style.display = "block";
			document.getElementById("equityvalue").style.display = "block";
			
			
			document.getElementById("debt-label").style.display = "none";
			document.getElementById("debt").style.display = "none";
			document.getElementById("debt-interest-label").style.display = "none";
			document.getElementById("debtinterest").style.display = "none";
			
			
			document.getElementById("donations-label").style.display = "none";
			document.getElementById("donations").style.display = "none";
			
			window.scroll(0,findPos(document.getElementById("equity")));
			
		});
		
		$('#deal-debt').click(function(){
		
			//set all others to dormant
			document.getElementById("deal-equity").className = "dormant";
			document.getElementById("deal-debt").className = "active";
			document.getElementById("deal-donations").className = "dormant";
			
			//*****************************
		
			document.getElementById("equity-label").style.display = "none";
			document.getElementById("equity").style.display = "none";
			document.getElementById("equity-price-label").style.display = "none";
			document.getElementById("equityvalue").style.display = "none";
			
			
			document.getElementById("debt-label").style.display = "block";
			document.getElementById("debt").style.display = "block";
			document.getElementById("debt-interest-label").style.display = "block";
			document.getElementById("debtinterest").style.display = "block";
			
			
			document.getElementById("donations-label").style.display = "none";
			document.getElementById("donations").style.display = "none";
			
			window.scroll(0,findPos(document.getElementById("debt")));
			
		});
		
		$('#deal-donations').click(function(){
		
			//set all others to dormant
			document.getElementById("deal-equity").className = "dormant";
			document.getElementById("deal-debt").className = "dormant";
			document.getElementById("deal-donations").className = "active";
			
			//*****************************
		
			document.getElementById("equity-label").style.display = "none";
			document.getElementById("equity").style.display = "none";
			document.getElementById("equity-price-label").style.display = "none";
			document.getElementById("equityvalue").style.display = "none";
			
			
			document.getElementById("debt-label").style.display = "none";
			document.getElementById("debt").style.display = "none";
			document.getElementById("debt-interest-label").style.display = "none";
			document.getElementById("debtinterest").style.display = "none";
			
			
			document.getElementById("donations-label").style.display = "block";
			document.getElementById("donations").style.display = "block";
			
			window.scroll(0,findPos(document.getElementById("donations")));
		});
		
		//Check to see if the window is top if not then display button
		$(window).scroll(function() {
			if ($(this).scrollTop() > 675) {
				$('.previous').fadeIn(500);
			} else {        
				$('.previous').fadeOut(500);
			}
			
			if ($(this).scrollTop() < 2000) {
				$('.next').fadeIn(500);
			} else {        
				$('.next').fadeOut(500);
			}

		});
		
		 var $window = $(window);
		 $('.next').on('click', function(e){
			 e.preventDefault();
			var scrollheight = $(document).height() - $(window).height();
			
			var bottomspace = scrollheight - $window.scrollTop();
			
			var pos =  Math.ceil( (bottomspace / 1.5) + $window.scrollTop() );
			
			$('html, body').animate({
				scrollTop: pos
			},1000);
			
			return false;
				
		});
		
		$('.previous').click(function(e){
			e.preventDefault();
			
			var pos =  Math.ceil( ($window.scrollTop() / 5) );
			
			$('html, body').animate({
				scrollTop: pos
			},1000);
			
			return false;
		});
		
		//for all inputs show the max characters that are to be put in them
		$('#title').maxlength({maxCharacters:55,status:true,slider: true, statusText: "characters left",statusClass: "characters-left"});
		$('#introduction').maxlength({maxCharacters:255,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#region').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		
		$('#image-tag').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#picture').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#about').maxlength({maxCharacters:7000,status:true,slider: true,showAlert: true,statusText: "characters left",statusClass: "characters-left"});
		
		$('#username').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#email').maxlength({maxCharacters:255,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#myemail').maxlength({maxCharacters:255,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#website').maxlength({maxCharacters:255,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#facebook').maxlength({maxCharacters:50,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#twitter').maxlength({maxCharacters:50,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		
		$('#addr1').maxlength({maxCharacters:60,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#addr2').maxlength({maxCharacters:60,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#city').maxlength({maxCharacters:35,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#region').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#postalcode').maxlength({maxCharacters:11,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		
		$('#link1').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#link2').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#link3').maxlength({maxCharacters:55,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		
		$('#funding').maxlength({maxCharacters:11,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#fundingdetails').maxlength({maxCharacters:4000,status:true,slider: true,showAlert: true,statusText: "characters left",statusClass: "characters-left"});
		$('#equity').maxlength({maxCharacters:11,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#equityvalue').maxlength({maxCharacters:11,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#debt').maxlength({maxCharacters:11,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#debtinterest').maxlength({maxCharacters:3,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#donations').maxlength({maxCharacters:11,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		
		$('#text-comment').maxlength({maxCharacters:1000,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		$('#text-update').maxlength({maxCharacters:1000,status:true,slider: true,statusText: "characters left",statusClass: "characters-left"});
		
		/* $('.next').on('click', function(e){
			 e.preventDefault();
			$('section').each(function() {
				var pos = $(this).offset().top;   
				alert(''+$window.scrollTop()+' '+pos);
				if ($window.scrollTop() < pos) {
					$('html, body').animate({
						scrollTop: pos
					},1000);
					return false;
				}
			});
		} ); */
		
		/* $('.previous').click(function(e){
			e.preventDefault();
			$($('section').get().reverse()).each(function() {
				var pos = $(this).offset().top;   
				if ($window.scrollTop() > pos) {
					$('html, body').animate({
						scrollTop: pos
					}, 1000);
					return false;
				}
			});
		} ); */
				
	}
);

var companies = [new Array(),0,new Array()];
var articles = [new Array(),0,new Array()];
var homeurl;
/*Position: Description
0:the array with companies to advertise from which a company is randomly selected
1: the column we are on
2: the array of random numbers put so as not to repeat
*/

function getProjects(getsubmitted,getcompanies,getcategory,getarticles,getuser,company,url,homeurl) {
	homeurl = homeurl;
	var username = getuser;
	
	(function (getsubmitted,getcompanies,getcategory,getarticles,getuser,company,url,homeurl) {$.ajax({ //get all project names so there are no repeats
		type: "POST",
		url: homeurl+"/"+url,
		// removed this since it gave a parse error //dataType: 'json',
		data: {submitted: getsubmitted,companies:getcompanies,category:getcategory,iscompany:company,username:getuser},   //got to put this value dynamically

		success: function (obj, textstatus) { 
			  //alert("success");
			  //var images_tags = new Array();
			 // alert("the advs "+obj);
			  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
			  //alert("the parsed: "+parsed['projects']);
			  if (getarticles == "no" && getuser == "no") {
			  
				 companies[0] = parsed['companies'];
				 companies[2] = Array.apply(null, {length:companies[0].length}).map(Number.call, Number);//array with positions for random selection
				 
				 //alert(companies);
				 //fillprojects(companies[0],cocolnumber,cocolposition,coposition,"companies-listing");
				 fillprojects(companies[0],companies[1],companies[2],getarticles,homeurl+'/backend/getProject.php');
				  
			  } else if (getuser == "no"){
				//alert("fill with articles!!");
				articles[0] = parsed['articles'];
				articles[2] = Array.apply(null, {length:articles[0].length}).map(Number.call, Number);//array with positions for random selection
				 
				//fillprojects(projects[0],projcolnumber,projcolposition,projposition,"project-listing");
				fillprojects(articles[0],articles[1],articles[2],getarticles,homeurl+'/backend/getArticle.php');
			  
			  } else {
				loaduser(parsed,'user-profile');
			  }
			 
			  //alert(parsed['images'].length);
			  //alert(parsed['projects']);  
		},
		
		error: function(xhr, textStatus, error){
			  alert(textStatus);
			  /* console.log(textStatus);
			  console.log(error); */
		}
	}); } (getsubmitted,getcompanies,getcategory,getarticles,getuser,company,url,homeurl));
	
}

function fillprojects(projects,columnnumber,pickfrom,getarticles,url){
	var i = 0;
	var iterate = Math.min(projects.length,2);

	for (i=0;i<iterate;i++){
		//alert("fill proj "+projects[projectsposition]);
		//alert("the homy "+homeurl+" "+url);
		
		// Returns a random integer between min (included) and max (excluded)
		// Using Math.round() will give you a non-uniform distribution!
		var index = Math.floor(Math.random() * (pickfrom.length - 0)) + 0;
		var pickindex = pickfrom[index];
		
		//remove the number already picked
		 pickfrom.splice(index, 1);
		
		
		(function (i,url,getarticles,pickindex) {$.ajax({
			type: "POST",
			url: url,
			// removed this since it gave a parse error //dataType: 'json',
			data: {projectname: projects[pickindex],articlename: projects[pickindex]},

			success: function (obj, textstatus) { 
				  //alert("success proj det");
				  //var images_tags = new Array();
				  //alert("article taken "+obj+" ");
				  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
				  
				if (columnnumber == 1){
					if (getarticles == "no")
						loadproject(parsed,columnnumber,"sidebar-right-companies");
					else
						loadarticle(parsed,columnnumber,"sidebar-left-articles");
					columnnumber = 0;
				}else {
					if (getarticles == "no")
						loadproject(parsed,columnnumber,"sidebar-right-companies");
					else
						loadarticle(parsed,columnnumber,"sidebar-left-articles");
					columnnumber ++;
				}
				   
			},
			
			error: function(xhr, textStatus, error){
				  alert("the err "+error);
				  /* console.log(textStatus);
				  console.log(error); */
			}
		});
		}(i,url,getarticles,pickindex));
		
	}
}

function loaduser(parsed,listing){
	//alert('in load user global'+parsed['email']);
	var username = parsed['username'];
	var profileimg = homeurl+"/members/"+username+"/profile";
	var images = new Array();
	var count = 0;
	
	$.ajax({
		//This will retrieve the contents of the folder if the folder is configured as 'browsable'
		url: profileimg,
		success: function getimages(data) {
			
			$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
			//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
				var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
				//alert(this.href);
				//alert("profile image global "+filename);
				var filename = profileimg + "/" + filename;
				//alert("got main image: "+filename);
				images[count] = new Image();
				images[count].src = filename;
				
				count++;
			});
			
			putimage(parsed);
			
		}
	});
	
	function putimage(parsed) {
		//alert('putting image global');
		//create the a element to put the thingsy in 
		var a = document.createElement("a");
		a.setAttribute("class","blog-list-project wow fadeInDown");
		a.setAttribute("data-wow-delay","0.5s");
		a.setAttribute("style","-webkit-animation-delay: 0.5s; -webkit-animation-name: none;width:90%;padding-top:15px");
		a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=54&username="+username);
		
		//create the img div
		var imgDiv = document.createElement("div");
		imgDiv.setAttribute("class","blog-image");
		//alert("the image global "+images[0].src);
		if (images[0])
			imgDiv.appendChild(images[0]);
		
		//create the text div
		var txtDiv = document.createElement("div");
		txtDiv.setAttribute("class","blog-excerpt");
		txtDiv.setAttribute("style","padding: 0 5px 1px;margin-bottom:10px;");
		 
		txtDiv.innerHTML = "<h3> "+username+"</h3>"+parsed['email']+ "<br>";
		
		//then put everything together
		a.appendChild(imgDiv);
		a.appendChild(txtDiv);
		
		//the items in the index page should be replaced;
		document.getElementById(listing).appendChild(a);
		//alert('putted image profile global');
			
	}
}

function loadarticle(parsed,colnumber,listing){
	//alert('in load article!!');
	var articlename = parsed['article'][0]['articlename'];
	var articlecategory = parsed['article'][0]['articlecategory'];
	var articletag = parsed['article'][0]['articletag'];
	var author = parsed['article'][0]['author'];
	var submitteddate = parsed['article'][0]['submitteddate'];
	
	var mainimg = homeurl+"/newsarticles/"+articlename+"/images/mainimage";
	var images = new Array();
	var count = 0;
	var fileextension = [".jpg",".png"];
	
	//alert("load article "+articlename);
	
	$.ajax({
		//This will retrieve the contents of the folder if the folder is configured as 'browsable'
		url: mainimg,
		success: function getimages(data) {
			
			$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
			//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
				var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
				//alert(this.href);
				//alert(filename);
				var filename = mainimg + "/" + filename;
				//alert("got main image: "+filename);
				images[count] = new Image();
				images[count].src = filename;
				
				count++;
			});
			
			putimage(parsed);
			
		}
	});
	
	
	function putimage(parsed) {
	
		//create the a element to put the thingsy in 
		var a = document.createElement("a");
		a.setAttribute("class","blog-list-project wow fadeInDown");
		a.setAttribute("data-wow-delay","0.5s");
		a.setAttribute("style","-webkit-animation-delay: 0.5s; -webkit-animation-name: none;width:90%;padding-top:15px");
		a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename);
		
		//create the img div
		var imgDiv = document.createElement("div");
		imgDiv.setAttribute("class","blog-image");
		
		if (images[0])
			imgDiv.appendChild(images[0]);
		
		//create the text div
		var txtDiv = document.createElement("div");
		txtDiv.setAttribute("class","blog-excerpt");
		txtDiv.setAttribute("style","padding: 0 5px 1px;margin-bottom:10px;");
		 
		txtDiv.innerHTML = "<h3> "+articlename+"</h3>"+articletag+ "<br><a class=\"blogoption\" href=\"http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename+"\">Read More&nbsp;&nbsp;</a> <br>";
		
		//then put everything together
		a.appendChild(imgDiv);
		a.appendChild(txtDiv);
		
		//the items in the index page should be replaced;
		document.getElementById(listing).appendChild(a);
			
	}
	
	//http://www.developerdrive.com/2012/03/a-simple-way-to-add-free-news-content-to-your-website/

}

function loadproject(parsed,colnumber,listing){
	var projectname = parsed['project'][0]['projectname'];
	var projectcategory = parsed['project'][0]['projectcategory'];
	var projecttag = parsed['project'][0]['projecttag'];
	var projectleader = parsed['project'][0]['projectleader'];
	var projectcountry = parsed['project'][0]['projectcountry'];
	var projectregion = parsed['project'][0]['projectregion'];
	var company = parsed['project'][0]['company'];
	
	var mainimg = homeurl+"/projects/"+projectname+"/images/mainimage";
	var images = new Array();
	var count = 0;
	var fileextension = [".jpg",".png"];
	
	//alert("load proj "+projectname);
	
	$.ajax({
		//This will retrieve the contents of the folder if the folder is configured as 'browsable'
		url: mainimg,
		success: function getimages(data) {
			
			$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
			//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
				var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
				//alert(this.href);
				//alert(filename);
				var filename = mainimg + "/" + filename;
				//alert("got main image: "+filename);
				images[count] = new Image();
				images[count].src = filename;
				
				count++;
			});
			
			putimage(parsed);
			
		}
	});
	
	function putimage(parsed) {
	
		//alert("put img "+projectname);
		//**********main a *******************
		var a = document.createElement("div");
		a.setAttribute("class","home-project");
		a.setAttribute("data-wow-delay","0.5s");
		a.setAttribute('style','box-shadow:none;border-bottom: 2px solid #800000;padding-top:1px;margin-left:0px;');
		a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=50&project="+projectname);
						
		//********************innera having the picture e.t.c *************************
		var innera = document.createElement('div');
		innera.setAttribute('onclick','location.href=\'http://www.Afroglobalinvestors.com/?page_id=50&project=\''+projectname);
		innera.setAttribute('class','portfolio-list wow fadeInUp');
		innera.setAttribute('style','-webkit-animation-delay: 2s; -webkit-animation-name: none;width:100%;cursor: pointer;');
		innera.setAttribute('data-wow-delay','2s');
		
		//alert('inner a 1');
		var overlay = document.createElement('a');
		overlay.setAttribute('class','project-overlay');
		overlay.setAttribute('style','width:100%;');
		overlay.innerHTML = "<span>created by: "+projectleader+"</span><br><span>located in: "+projectcountry+"</span>"+"<br><br><p>"+projecttag+"</p>";
				
		if (company == "no")
			overlay.setAttribute('href','http://www.Afroglobalinvestors.com/?page_id=50&project='+projectname);
		else
			overlay.setAttribute('href','http://www.Afroglobalinvestors.com/?page_id=101&project='+projectname);
		
		innera.appendChild(overlay);
		
		var image = document.createElement('div');
		image.setAttribute('class','portfolio-image');
		
		var img = document.createElement('img');
		img.setAttribute("src", images[0].src);
		
		//alert('inner a 2 '+images[0].src);
		image.appendChild(img);
		innera.appendChild(image);
		
		a.appendChild(innera);
		
		//**************************end of innera ************************
		
		//***********************create the info to the project **********
		var imgDiv = document.createElement("div");
		imgDiv.setAttribute("class","blog-image");
		
		//alert('outer a 2');
		//create the text div
		var txtDiv = document.createElement("div");
		txtDiv.setAttribute("class","blog-excerpt");
		 //alert(parsed['project']);
		txtDiv.innerHTML = "<h3> "+projectname+"</h3> <h4 class='posted-date'>"+projectcategory+"</h4> "+projecttag+ "<br><br>";
		
		//then put everything together
		a.appendChild(imgDiv);
		a.appendChild(txtDiv);
							
		//the items in the index page should be replaced;
		document.getElementById(listing).appendChild(a);
	
	}
	
}

//************************ Code for getting comments for an article or project ********************************//

function getArticleComments(url){
	//alert("getting comms");
	$.ajax({
		type: "POST",
		url:url,
		// removed this since it gave a parse error //dataType: 'json',
		data: {articlename: articlename},

		success: function (obj, textstatus) { 
			  //alert("success");
			  //var images_tags = new Array();
			  //alert("comms "+obj);
			  commentsparsed = jQuery.parseJSON(obj);//JSON.parse(obj);
			  //var images = JSON.parse(obj["images"]);
			  //alert(parsed['images'].length);
			  //alert(parsed['comments'][0]['comment']);
			  
			  insertComments(commentsparsed);
			 
		},
		
		error: function(xhr, textStatus, error){
			  alert("the err "+error);
			  /* console.log(textStatus);
			  console.log(error); */
		}
	});
}

function getProjectComments(url){
	//alert("getting comms");
	$.ajax({
		type: "POST",
		url:url,
		// removed this since it gave a parse error //dataType: 'json',
		data: {projectname: projectname},

		success: function (obj, textstatus) { 
			  //alert("success");
			  //var images_tags = new Array();
			  //alert("comms "+obj);
			  commentsparsed = jQuery.parseJSON(obj);//JSON.parse(obj);
			  //var images = JSON.parse(obj["images"]);
			  //alert(parsed['images'].length);
			  //alert(parsed['comments'][0]['comment']);
			  
			  insertComments(commentsparsed);
			 
		},
		
		error: function(xhr, textStatus, error){
			  alert("the err "+error);
			  /* console.log(textStatus);
			  console.log(error); */
		}
	});
}

function insertComments(comments){
	//alert('insert comms '+comments['comments'][0]["postername"]);
	for (i=0;i<comments['comments'].length;i++){
		var commentfield = document.createElement("div");
		commentfield.setAttribute("class","post-field");
		
		var name = document.createElement("p");
		name.setAttribute("class","poster-name");
		name.innerHTML = comments['comments'][i]['postername'] ;
		
		var date = document.createElement("p");
		date.setAttribute("class","post-date");
		date.innerHTML = comments['comments'][i]['postdate'] ;
		
		var post = document.createElement("p");
		post.setAttribute("class","post-description");
		post.innerHTML = comments['comments'][i]['comment'] ;
		
		commentfield.appendChild(name);
		commentfield.appendChild(date);
		commentfield.appendChild(post);
		
		document.getElementsByName("container")[0].appendChild(commentfield);
	}

}

function getUpdates(){
	//alert("getting updates");
	$.ajax({
		type: "POST",
		url: homeurl+'/backend/getUpdates.php',
		// removed this since it gave a parse error //dataType: 'json',
		data: {projectname: projectname},

		success: function (obj, textstatus) { 
			  //alert("success got updates");
			  //var images_tags = new Array();
			 // alert(obj);
			  
			  updatesparsed = jQuery.parseJSON(obj);//JSON.parse(obj);
			  //var images = JSON.parse(obj["images"]);
			  //alert(parsed['images'].length);
			  //alert(parsed['comments'][0]['comment']);
			  
			  insertUpdates(updatesparsed);
			 
		},
		
		error: function(xhr, textStatus, error){
			  alert("the err updates "+error);
			  /* console.log(textStatus);
			  console.log(error); */
		}
	});
}

function insertUpdates(updates){
	//alert('insert updates '+updates['updates'][0]["projectleader"]);
	for (i=0;i<updates['updates'].length;i++){
		var updatefield = document.createElement("div");
		updatefield.setAttribute("class","post-field");
		
		var name = document.createElement("p");
		name.setAttribute("class","poster-name");
		name.innerHTML = updates['updates'][i]['projectleader'] ;
		
		var date = document.createElement("p");
		date.setAttribute("class","post-date");
		date.innerHTML = updates['updates'][i]['date'] ;
		
		var update = document.createElement("p");
		update.setAttribute("class","post-description");
		update.innerHTML = updates['updates'][i]['updatevalue'] ;
		
		updatefield.appendChild(name);
		updatefield.appendChild(date);
		updatefield.appendChild(update);
		
		document.getElementsByName("container-updates")[0].appendChild(updatefield);
	}
	//alert("inserted updtes ");

}

//***********************************************************************

function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;
	

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 80)
        return "strong";
    if (score > 60)
        return "good";
    if (score >= 30)
        return "weak";
	if( pass.length < 6 )
		return "weak";

    return "";
}





