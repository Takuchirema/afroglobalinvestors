

$(document).ready(function(){

	$('#nav-login').click(function(){
		$( "#header-login" ).dialog({
      dialogClass: "header-login",
      width: 400,
      draggable: false,
      resizable: false,
      modal: false,
	  open: function(event, ui) {
        clearErrorMessage();
        $(event.target).dialog('widget')
          .css({ position: 'fixed' })
          .position({ my: 'right+48 top+15', at: 'center bottom', of: "#nav-login" });
      },
      close: function(event,ui){
        clearErrorMessage();
		$( ".header-login" ).close();
      }
    });
		return false;
	});

});


