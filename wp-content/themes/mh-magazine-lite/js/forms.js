/* 
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function formhash(form, password,email) {
	var response = new Array();
	 // Check each field has a value
    if (email.value == '' || password.value == '') {
		response['success'] = 0;
		response['message'] = 'Please provide all the requested details.';
		showresponse(response);
		
        return false;
    }
	
    // Create a new element input, this will be our hashed password field. 
    var p = document.createElement("input");

    // Add the new element to our form. 
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
	//alert('new pass is: '.concat(p.value));
    // Make sure the plaintext password doesn't get sent. 
    password.value = "";
	
	var parsed;
	var formData = new FormData(form);
		
	$.ajax({
		type: "POST",
		url: './backend/includes/process_login.php',
		data: formData,
		 async: false,
		success: function (object, textstatus) { 
			  //var images_tags = new Array();
			  parsed = jQuery.parseJSON(object);//JSON.parse(obj);

			  showresponse(parsed);
			  
			   
		},
		cache: false,
		contentType: false,
		processData: false				
	});
	
	function showresponse(parsed){
		if (parsed['success'] == 0){
			document.getElementById('login-header-response').style.display = "";
			document.getElementById('login-response').innerHTML = parsed['message'];
		} else {
			location.reload();
		
		}
	}
	
    // Finally submit the form. 
    //form.submit();
}

function regformhash(form, uid, email, password, conf) {
    
	var response = new Array();
	// Check each field has a value
    if (uid.value == '' || email.value == '' || password.value == '' || conf.value == '') {
		response['success'] = 0;
		response['message'] = 'Please provide all the requested details.';
		showresponse(response);
        return false;
    }
    
    // Check the username
    re = /^\w+$/; 
    if(!re.test(form.username.value)) { 
		response['success'] = 0;
		response['message'] = 'Username must contain only letters, numbers and underscores. Please try again';
		showresponse(response);

        return false; 
    }
	
	var passwordstrength = document.getElementById("strength_human").innerHTML;
	
	if (passwordstrength == 'weak' || passwordstrength == ''){

		response['success'] = 0;
		response['message'] = 'Please provide a password that has strength good or better for your security';
		showresponse(response);
		return false;
	}
    
    // Check that the password is sufficiently long (min 6 chars)
    // The check is duplicated below, but this is included to give more
    // specific guidance to the user
   /*  if (password.value.length < 6) {
        alert('Passwords must be at least 6 characters long.  Please try again');
        form.password.focus();
        return false;
    }
    
    // At least one number, one lowercase and one uppercase letter 
    // At least six characters 
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
    if (!re.test(password.value)) {
        alert('Passwords must contain at least one number, one lowercase and one uppercase letter.  Please try again');
        return false;
    }
     */
	 
	 
    // Check password and confirmation are the same
    if (password.value != conf.value) {
		response['success'] = 0;
		response['message'] = 'Your password and confirmation do not match. Please try again';
		showresponse(response);
        return false;
    }
        
    // Create a new element input, this will be our hashed password field. 
    var p = document.createElement("input");

    // Add the new element to our form. 
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);
	
	//alert('new pass is: '.concat(p.value));
	
    // Make sure the plaintext password doesn't get sent. 
    password.value = "";
    conf.value = "";
	
	var parsed;
	var formData = new FormData(form);
		
	$.ajax({
		type: "POST",
		url: './backend/includes/register.inc.php',
		data: formData,
		 async: false,
		success: function (object, textstatus) { 
			  //alert("success proj det");
			  //var images_tags = new Array();
			  parsed = jQuery.parseJSON(object);//JSON.parse(obj);
			  //alert('the p '+parsed['project'][0]['projectname']);
			  showresponse(parsed,form.username.value);
			  
			   
		},
		cache: false,
		contentType: false,
		processData: false				
	});
	
	function showresponse(parsed,username){
		if (parsed['success'] == 0){
			document.getElementById('signup-header-response').style.display = "";
			document.getElementById('signup-response').innerHTML = parsed['message'];
			
			return false;
		} else {
			
			var message = "You have successfully Signed up! You can click on LEARN on the top task bar to learn more of creating projects or company profiles and articles.<br><br>Login with your details on the LOGIN button on the top task bar.<br><br>If you have any questions please feel free to contact the Afroglobalinvestors team by using the contact us on the footer page.<br><br>Make your projects a reality! Afroglobalinvestors";
			
			$.ajax({
				type: 'POST',
				url: "./backend/setVariable.php",
				data: {response:message},
				async: false,
				success: function (obj, textstatus) {
					//edit sitemap with persons profile page
					editSiteMap('http://www.afroglobalinvestors.com/profile/'+username);
					//alert(parsed["message"]);
					location.href='http://www.Afroglobalinvestors.com/?page_id=108';
				},
				
			});
				
					
		}
		
	}
	
    // Finally submit the form. 
   // form.submit();
    //return true;
}

function profformhash(form, uid, email, password, conf) {
	//alert('in profform');
	//check if the any of the personal user info has been changed
	if (document.getElementById("changed").innerHTML == "true"){
		// Check each field has a value
		if ((uid.value != '' || email.value != '' || password.value != '') && (conf.value == '' || uid.value == '' ) ) {
			
			document.getElementById('account-response').style.display = "";
			document.getElementById('update-account-response').innerHTML = 'You must provide your current password and username to change these fields';
			
			window.scroll(0,findPos(document.getElementById("update-account-title")));
			
			return false;
		}
    }
	
    // Check the username
    re = /^\w+$/; 
	if (uid.value != '' ) {
		if(!re.test(uid.value)) { 
		
			document.getElementById('account-response').style.display = "";
			document.getElementById('update-account-response').innerHTML = 'Name must contain only letters, numbers and underscores. Please try again';		
			window.scroll(0,findPos(document.getElementById("update-account-title")));
			
			return false; 
		}
	}
    
    // Check that the password is sufficiently long (min 6 chars)
    // The check is duplicated below, but this is included to give more
    // specific guidance to the user
	/* if (password.value != '') {
		if (password.value.length < 6) {
			alert('Passwords must be at least 6 characters long.  Please try again');
			form.password.focus();
			return false;
		}
		
		// At least one number, one lowercase and one uppercase letter 
		// At least six characters 
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
		if (!re.test(password.value)) {
			alert('Passwords must contain at least one number, one lowercase and one uppercase letter.  Please try again');
			return false;
		}
	} */
	
	var passwordstrength = document.getElementById("new-password-strength").innerHTML;
	
	if ( (passwordstrength == 'weak' || passwordstrength == '' ) && password.value != ''){
				
		document.getElementById('account-response').style.display = "";
		document.getElementById('update-account-response').innerHTML = 'Please provide a password that has strength good or better for your security';
		
		window.scroll(0,findPos(document.getElementById("update-account-title")));
		// the next line is required to work around a bug in WebKit (Chrome / Safari)
		//location.href = "#";
		//location.href = "#account-response"
		
		
		return false;
	}
	
   if ((uid.value != '' || email.value != '' || password.value != '') && (conf.value != '') )
   {
		var submit = "true";
		var oldpass = hex_sha512(conf.value);
		var success = 0;
		var parsed;
		var message;
		
		
		try{
		
		//alert('ajax start');
		$.ajax({
			type: "POST",
			url: 'backend/checkpwd.php',
			// removed this since it gave a parse error //dataType: 'json',
			data: {username: uid.value,
					password: oldpass},

			success: function (obj, textstatus) { 
				  //alert("success");
				  //var images_tags = new Array();
				  //alert(obj);
				  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
				  //var images = JSON.parse(obj["images"]);
				  //alert(parsed['images'].length);
				  //alert(parsed['images'][0]['logo.png']);
				  success = parsed['success'];
				  //alert('sucess is '.concat(success));
				  message = parsed['message'];
				  
				  changepassword();
				  
			},
			
			error: function(xhr, textStatus, error){
				  alert(textStatus);
				  /* console.log(textStatus);
				  console.log(error); */
			}
		});
		} catch(err){
			alert(err.message);
		}
		
		function changepassword() {

			if (success == 1)
			{
			
				if (password.value != '')
				{
					// Create a new element input, this will be our hashed password field. 
					var p = document.createElement("input");

					// Add the new element to our form. 
					form.appendChild(p);
					p.name = "p";
					p.type = "hidden";
					p.value = hex_sha512(password.value);
					
					//alert('new pass is: '.concat(p.value));
				
					// Make sure the plaintext password doesn't get sent. 
					password.value = "";
					conf.value = "";

				}
				
				// Finally submit the form. 
				
				//form.submit();
				submitform(form,"./backend/myprofile.php","");
				return true;
			} else 
			{
				
				document.getElementById('account-response').style.display = "";
				document.getElementById('update-account-response').innerHTML = message;
				
				window.scroll(0,findPos(document.getElementById("update-account-title")));
				// the next line is required to work around a bug in WebKit (Chrome / Safari)
				//location.href = "#";
				//location.href = "#account-response"
				return false;
			}
			
		}
		
		//return false;
	
	} else{
		//form.submit();
		submitform(form,"./backend/myprofile.php","","");
		return false;
	
	}
}

//Finds y value of given object
function findPos(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
    return [curtop];
    }
}

function submitform(form,url,projectname,variable) {
	//alert("submitform js");
	var formData = new FormData(form);

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        async: false,
        success: function (obj, textstatus) {
            //alert(obj);
			parsed = jQuery.parseJSON(obj);
			//alert('submit form');
			postResponse(parsed,projectname,variable);
        },
        cache: false,
        contentType: false,
        processData: false
    });
	
    return false;

}

function postResponse(parsed,projectname,variable){

	//document.getElementById("response").style.display = "block";
	//document.getElementById("response").innerHTML = "<a class=\"remove\" onclick=\"removealert()\"></a>" + parsed["message"];
	
	//alert(parsed["success"]+" message : "+parsed['message']);
	if (parsed["success"] == 1 && projectname != "")
	{
		//alert("sending variable");
		$.ajax({
			type: 'POST',
			url: "./backend/setVariable.php",
			data: {variable:variable,name:projectname,response:parsed["message"]},
			async: false,
			success: function (obj, textstatus) {
				//alert(obj);
				location.reload();
			
			},
			
		});
		
	} else {
	
		$.ajax({
			type: 'POST',
			url: "./backend/setVariable.php",
			data: {response:parsed["message"]},
			async: false,
			success: function (obj, textstatus) {
				//alert(obj);
				
				location.reload();
			
			},
			
		});
	
	}
}


function projbformhash(form, title,category,edit,company) { //projb for project basics
	
	var response = new Array();
	
	var categoryvalue = category.options[category.selectedIndex].value;
		
	if (document.getElementById('check_project').innerHTML == 'project name taken') {
		
		response['success'] = 0;
		response['message'] = 'Project name is taken. Please choose another name for your project';
		postResponse(response,"","");
   
        return false;
    }
	
	if ((title.value == '' || categoryvalue == 'Select' )) {
	
		response['success'] = 0;
		response['message'] = 'Please provide the project name and category';
		postResponse(response,"","");

        return false;
    }

	 // Check the project name
    re = /^[A-Za-z0-9 _]*$/; 
	ren = /^[0-9]*$/; 
	if (title.value != '' ) {
		if(!re.test(title.value)) { 
			
			response['success'] = 0;
			response['message'] = 'Project name must contain only letters, numbers';
			postResponse(response,"","");
			return false; 
		}else if(ren.test(title.value))
		{
			response['success'] = 0;
			response['message'] = 'Project name must contain at least one character';
			postResponse(response,"","");
			return false; 
		}
	}
	if (company == 'true'){
	
		if (edit != "true") {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();
			
			var date = yyyy+"-"+mm+"-"+dd;
			
			var createddate = document.createElement("input");
			
			form.appendChild(createddate);
			createddate.name = "datepicker";
			createddate.type = "hidden";
			createddate.value = date;
			
			editSiteMap('http://www.afroglobalinvestors.com/company/'+title.value);
		}
	
		var p = document.createElement("input");

		// Add the new element to our form. 
		form.appendChild(p);
		p.name = "company";
		p.type = "hidden";
		p.value = "yes";
		
		
	
	}
	
	if (edit == 'true'){ //then put another field edit and make title nill
		var p = document.createElement("input");

		// Add the new element to our form. 
		form.appendChild(p);
		p.name = "edit";
		p.type = "hidden";
		p.value = title.value;
		
		//alert('new pass is: '.concat(p.value));
	
		// Make sure the plaintext password doesn't get sent. 
		//title.value = "";
	}
	
	//form.submit();
	submitform(form,"./backend/myproject.php",title.value,"project");
	return true;

}

function projdformhash(form, funding,title) { //projd for project details
	//alert('proj d');
	var p = document.createElement("input");

	// Add the new element to our form. 
	form.appendChild(p);
	p.name = "edit";
	p.type = "hidden";
	p.value = title;

	//form.submit();
	submitform(form,"./backend/myproject.php",title,"project");
	return true;

}

function projrformhash(form,funding,equity,equityvalue,debt,debtinterest,donations,title) { //project for project rewards changed to funding
	
	var fundingNum =0;
	var equityNum=0;
	var equityvalueNum=0;
	var debtNum=0;
	var debtinterestNum=0;
	var donationsNum=0;
	
	ren = /^[0-9]*$/; 
	
	if (document.getElementById('funding').innerHTML == 'please enter a numerical value') {
		response['success'] = 0;
		response['message'] = 'Project enter a numerical value for funding';
		postResponse(response,"","");

        return false;
    }
	
	//first check if the fields exit because we clear all the fields on click new funding
	if (equity && equityvalue){
		if ((equity.value == '' && equityvalue.value != '' ) || (equity.value != '' && equityvalue.value == '' )) {
			response['success'] = 0;
			response['message'] = 'Project provide both equity amount and equity value';
			postResponse(response,"","");

			return false;
		} else if (!ren.test(equity.value) || !ren.test(equityvalue.value)){
			response['success'] = 0;
			response['message'] = 'All equity entries should be numeric';
			postResponse(response,"","");

			return false;
		} else if ( equity.value == '' && equityvalue.value == '' ){
			equityNum = 0;
			equityvalueNum = 0;
		} else {
			equityNum = equity.value;
			equityvalueNum = equityvalue.value;
		}
	}
	
	if (debt && debtinterest){
		if ((debt.value == '' && debtinterest.value != '' ) || (debt.value != '' && debtinterest.value == '' )) {
			response['success'] = 0;
			response['message'] = 'Please provide both debt amount and debt interest';
			postResponse(response,"","");
			
			return false;
		}else if (!ren.test(debt.value) || !ren.test(debtinterest.value)){
			response['success'] = 0;
			response['message'] = 'All debt entries should be numeric';
			postResponse(response,"","");
			
			return false;
		} else if (debt.value == '' && debtinterest.value == ''){
			debtNum = 0;
			debtinterestNum = 0;
		}else {
			debtNum = debt.value;
			debtinterestNum = debtinterest.value;
		}
	}
	
	if (donations){
		if (donations.value == ''){ donationsNum = 0;}
		else if (!ren.test(donations.value)){
		
			response['success'] = 0;
			response['message'] = 'Donations entry should be numeric';
			postResponse(response,"","");

			return false;
		} else{
			donationsNum = donations.value;
		}
	}
	
	if (funding.value == ""){
		fundingNum = 0;
	} else { fundingNum = funding.value;  }
	
	
	var fundingTotal =  parseInt(equityNum) + parseInt(debtNum) + parseInt(donationsNum) ;
		
	if (fundingTotal != fundingNum){
		
		response['success'] = 0;
		response['message'] = 'The funding values are not adding up. Total funding must equal the sum of equity, debt and donations';
		postResponse(response,"","");
		
		return false;
	}
	
	var p = document.createElement("input");

	// Add the new element to our form. 
	form.appendChild(p);
	p.name = "edit";
	p.type = "hidden";
	p.value = title;

	
	submitform(form,"./backend/myproject.php",title,"project");
	return true;

}

function newsformhash(form, title,category,edit) { //projb for project basics
	
	
	if (document.getElementById('check-article').innerHTML == 'article name taken') {
		
		response['success'] = 0;
		response['message'] = 'Please choose another name for your article, the current in taken';
		postResponse(response,"","");
        return false;
    }
	
	if ((title.value == '' || category.value == 'Select' )) {
	
		response['success'] = 0;
		response['message'] = 'Please provide article name and category';
		postResponse(response,"","");

        return false;
    }

	 // Check the project name
    re = /^[A-Za-z0-9 _]*$/; 
	ren = /^[0-9]*$/; 
	if (title.value != '' ) {
		//alert('not emp');
		if(!re.test(title.value)) { 
			response['success'] = 0;
			response['message'] = 'Article name must contain only letters, numbers';
			postResponse(response,"","");
			
			return false; 
		}else if(ren.test(title.value))
		{
			response['success'] = 0;
			response['message'] = 'Article name must contain at least one character';
			postResponse(response,"","");

			return false; 
		}
	}
	
	if (edit == 'true'){ //then put another field edit and make title nill
		var p = document.createElement("input");

		// Add the new element to our form. 
		form.appendChild(p);
		p.name = "edit";
		p.type = "hidden";
		p.value = title.value;
		
		//alert('new pass is: '.concat(p.value));
	
		// Make sure the plaintext password doesn't get sent. 
		//title.value = "";
	}
	
	//form.submit();
	submitform(form,"./backend/myNewsArticle.php",title.value,"article");
	return true;

}

function sendemail(form){
	
	submitform(form,"./backend/emailto.php","","");
	return true;


}

function resetpassword(form){

	var formData = new FormData(form);

    $.ajax({
        url: './backend/checkEmail.php',
        type: 'POST',
        data: formData,
        async: false,
        success: function (obj, textstatus) {
            //alert('check email resp '+obj);
			parsed = jQuery.parseJSON(obj);
			postResponse(parsed);
        },
        cache: false,
        contentType: false,
        processData: false
    });
	
	function postResponse(parsed){
	
		//alert(parsed["success"]);
		if (parsed["success"] == 1)
		{
			var username = document.createElement("input");

			// Add the new element to our form. 
			form.appendChild(username);
			username.name = "username";
			username.type = "hidden";
			username.value = parsed['username'];
			
			var newpassword = generatePassword();
			
			alert("**NB Still under TESTING. User found, generated new password "+newpassword);
			
			var p = document.createElement("input");

			// Add the new element to our form. 
			form.appendChild(p);
			p.name = "p";
			p.type = "hidden";
			p.value = hex_sha512(newpassword);
			
			//add also the understandeable password
			var newpwd = document.createElement("input");

			// Add the new element to our form. 
			form.appendChild(newpwd);
			newpwd.name = "newpassword";
			newpwd.type = "hidden";
			newpwd.value = newpassword;
			
			submitform(form,"./backend/emailResetPassword.php","","");
			
		} else {
		
			$.ajax({
				type: 'POST',
				url: "./backend/setVariable.php",
				data: {response:parsed["message"]},
				async: false,
				success: function (obj, textstatus) {
					//alert(obj);
					
					location.reload();//href='http://www.Afroglobalinvestors.com/?page_id=40';
				
				},
				
			});
		
		}
	}
    return false;

}

function generatePassword() {
    var length = 8,
        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

function editSiteMap(url){
	var url = url.split(' ').join('%20');
	
	xmlDoc=loadXMLDoc("sitemap.xml"); 
	
/* 	urlelement = xmlDoc.createElement('url');
	urlelement.setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
	urlelement.innerHTML = '<loc>http://www.afroglobalinvestors.com/project/Harvesters</loc>';
 */	
	urlset=xmlDoc.getElementsByTagName("urlset")[0];
	urlset.innerHTML= urlset.innerHTML+"\n<url xmlns ='http://www.sitemaps.org/schemas/sitemap/0.9'>\n<loc>"+url+"</loc>\n</url>\n";
	var xmlstring = new XMLSerializer().serializeToString(xmlDoc);
	
	sendXMLDoc('./backend/sitemap.php',xmlstring);

}

function sendXMLDoc(filename,file){

	 $.ajax({
        url: './backend/sitemap.php',
        type: 'POST',
        data: {sitemap: file},
        async: false,
        success: function (obj, textstatus) {

        },
		error: function (textstatus) {
            alert('error '+textstatus);
        }
    });
	

}

function loadXMLDoc(filename)
{
	if (window.XMLHttpRequest)
	  {
	  xhttp=new XMLHttpRequest();
	  }
	else // code for IE5 and IE6
	  {
	  xhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xhttp.open("POST",filename,false);
	xhttp.send();
	return xhttp.responseXML;
}



