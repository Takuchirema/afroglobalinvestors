<?php /* Template Name: ProjectRewards */ ?>
<?php get_header(); ?>
<div class="wrapper clearfix">
	<nav class="main-nav">
		<ul class="nav">
		
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=40">Project Basics</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=43">Project Details</a></li>
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=38">Project Rewards</a></li>
			
			<button name="save" id="save" onclick="return projrformhash(document.getElementById('searchform'),
																			   document.getElementById('searchform').equity,
																			   document.getElementById('searchform').equityvalue,
																			   document.getElementById('searchform').debt,
																			   document.getElementById('searchform').debtinterest,
																			   document.getElementById('searchform').donations);">save</button>
			<!-- get the name of the project thats currently being worked on and pass it as a parameter-->															   
			<button onclick="location.href='http://www.Afroglobalinvestors.com/?page_id=50&project=safenet'" name="next" id="next">save view</button>


		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget">
			<div class="box alert">
				<p>Please fill in below</p>
			</div>
			
			<form method="post" id="searchform" action="./backend/myproject.php">
			
			<h4 class="widget-title">Deal Type</h4>
			
			<fieldset class="entry-field" class ="rewards" id ="rewards" style="display: block">
			
				<ul class="deals-nav" id="deals-nav" >
					<li><a class="" href="#" id="deal-equity">Equity</a></li>
				    <li><a class=""  href="#"  id="deal-debt">Debt</a></li>
				   <li><a class="" href="#"  id="deal-donations">Donations</a></li>
		
				</ul>
				
			</fieldset>
						
			</form>
			
		</div>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	<aside class="sidebar sb-right">
		
		<div class="sb-widget"><h4 class="widget-title" >Equity</h4>
			<ul class="nav">
				<li>Enter the total amount to be raised by equity and the price for each share</li>
				<li>e.g $5000 at $5 per share means 1000 shares available</li>
				<li>Shares are one of the quickest way to get funding</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" >Debt</h4>
			<ul class="nav">
				<li>Enter the total amount to be raised by debt and the interest rate you are offering (compound interest)</li>
				<li>Aside from equity debt is one of the best ways to raise funding</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" >Donations</h4>
			<ul class="nav">
				<li>A donation leaves no obligation to the funder</li>
			</ul>
		</div>
		
		
	</aside>
</div>

<?php get_footer(); ?>