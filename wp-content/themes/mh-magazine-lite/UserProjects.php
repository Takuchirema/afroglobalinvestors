<?php /* Template Name: UserProjects */ ?>
<?php get_header(); ?>
<div class="wrapper clearfix" id="wrapper" >
	<nav class="main-nav" style="width:100%;">
		<ul class="nav">
		
			<li class="" ><a href="" id="user-profile"></a></li>
			<li class="active" ><a href="" id="user-projects">Projects</a></li>
			<li class="" ><a href="" id="user-articles">Articles</a></li>

		</ul>
	
	</nav>
	
	<aside class="sidebar projects-sb-left left" id="sidebar-left">
		<h4  ><i>Featured Content </i></h4>
		<div id="sidebar-left-articles"> </div>
		
	</aside>
			
	<section class="projects-container" id="projects-container">
	
		<div class="box alert" id="noprojects">
			<p id="no-projects" >No Projects</p>
		</div>
			
		<!-- <div class="projects-container" id="projects-container">
			<div class="box alert" id="noprojects">
				<p id="no-projects" >No Projects</p>
			</div>
			
		</div> -->
		
		<script type="text/javascript">
			var homeurl = "<?php echo home_url() ; ?>";
			
			var username = "<?php echo $_GET['username']; ?>" ;
			
			var myprojects;
			
			var images = new Array();
			
			var countprojects = 0;
			
			getMyProjects();
			
			metadata();
			
			
			
			function getMyProjects() {
				//take the projects the user has created
				$.ajax({
					type: "POST",
					url: 'backend/getMyProjects.php',
					data: {username: username,submitted:'yes'},

					success: function (obj, textstatus) { 

						  myprojects = jQuery.parseJSON(obj);//JSON.parse(obj);
						  
						  metadata();
						  
						  seturls();
						  
						  listprojects(myprojects);
						  
					},
					
					error: function(xhr, textStatus, error){
						  alert("the err "+error);
					}
				});
				
			}
			
			function metadata(){
				document.getElementById("seo-title").innerHTML = username +" - Projects | Afroglobalinvestors";
				
				var metadesc = document.createElement("meta");
				metadesc.setAttribute("content","View all projects created by "+username);
				metadesc.setAttribute("name","description");
				metadesc.setAttribute("itemprop","description");
				
				document.getElementsByTagName("head")[0].appendChild(metadesc);
			}
				
			function seturls(){
				document.getElementById("user-profile").innerHTML = "About "+username;
				document.getElementById("user-profile").href = "http://www.Afroglobalinvestors.com/?page_id=54&username="+username;
				document.getElementById("user-projects").href = "http://www.Afroglobalinvestors.com/?page_id=97&username="+username;
				document.getElementById("user-articles").href = "http://www.Afroglobalinvestors.com/?page_id=99&username="+username;
			
			}
			
			
			
			 function listprojects(parsed){
			 
				if (parsed['success'] == 1){
					document.getElementById("no-projects").innerHTML = ""; 
					
					for (i=0; i<parsed['projects'].length; i++)
					{	
						getImage(parsed['projects'][i]['projectname'],parsed['projects'][i]['company'],i);
						
					}
					
				}else{
					getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
				}
				
			
			}
		  
			function getImage(projectname,company,index)
			{	
			
				var dir = "projects/"+projectname+"/images/mainimage";
				var fileextension = [".jpg",".png"];
				var count = 0;
				
				$.ajax({
					url: dir,
					success: function getimages(data) {
						
						$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
						//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
							//alert(this.href);
							var filename = dir + "/" + filename;
							images[count] = new Image();
							images[count].src = filename;
							count++;
						});
	 
						finished(index,projectname,company);
					},
					error: function showPorject(data) {
						finished(index,projectname);
					}
					
				});
				
				
				function finished(index,projectname,company){
					//********** make the height of the container match the projects *******
					
					var img = new Image();
					img.onload = function() {
						var projectnum = myprojects['projects'].length - 1;

					  if (countprojects == (projectnum)) {
						
						//get adverts after the getting of projects
						getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
						}
						countprojects++;
					}
					img.src = images[0].src;
					
					//**************************************
					
					var a = document.createElement("a");
					a.setAttribute("class","blog-list-project wow fadeInDown");
					a.setAttribute("data-wow-delay","0.5s");
					a.setAttribute("style","-webkit-animation-delay: 0.5s; -webkit-animation-name: none; padding-top: 1cm;width:100%;");
					a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=50&project="+projectname);
					
					//create the img div
					var imgDiv = document.createElement("div");
					imgDiv.setAttribute("class","blog-image");
					
					if (images[0])
						imgDiv.appendChild(images[0]);
					
					//create the text div
					var txtDiv = document.createElement("div");
					txtDiv.setAttribute("class","blog-excerpt");
					 
					txtDiv.innerHTML = "<h3> "+myprojects['projects'][index]['projectname']+"</h3> <h4 class=\"posted-date\"><i class=\"fa fa-calendar\"></i>"+myprojects['projects'][index]['date']+"</h4> "+myprojects['projects'][index]['projecttag']+ "<br><a class=\"blogoption\" href=\"http://www.Afroglobalinvestors.com/?page_id=50&project="+projectname+"\">Read More&nbsp;&nbsp;<i class=\"fa fa-angle-right\"></i> </a>";
					
					//then put everything together
					a.appendChild(imgDiv);
					a.appendChild(txtDiv);
					
					document.getElementById("projects-container").appendChild(a);
					
					//reset images
					images = new Array();
										
				}
			}
			
			function editproject(projectname,company){
				
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"project",name:projectname},
					async: false,
					success: function (obj, textstatus) {
						if (company == 'yes')
							createcompany("not-new");
						else
							location.href='http://www.Afroglobalinvestors.com/?page_id=40';
					
					},
					
				});
		
			}
			
			function createproject(){
				//first unset all project variables that are lingering about
				<?php try { unset($_SESSION['project']); }catch(Exception $ex){} ?>
				<?php try { unset($_SESSION['company']); }catch(Exception $ex){} ?>
				
				location.href='http://www.Afroglobalinvestors.com/?page_id=40'
			}
			
			function createcompany(newcompany){
				alert('in co');
				if (newcompany =='new') {
				//first unset all project variables that are lingering about
				<?php try { unset($_SESSION['project']); }catch(Exception $ex){} ?>
				//then set a variable for company
				}
				alert("sending variable");
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"company",name:"yes"},
					async: false,
					success: function (obj, textstatus) {
						alert(obj);

						location.href='http://www.Afroglobalinvestors.com/?page_id=40';
					
					},
					
				});
				
			}

			</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar projects-sb-right right" id="sidebar-right">
		
		<h4 style="margin-left:20px"><i>Featured Companies </i></h4>
		<div id="sidebar-right-companies"> </div>
				  
	</aside>
			

</div>
<?php get_footer(); ?>