<?php /* Template Name: HITArticle */ ?>
<?php 
include_once("analyticstracking.php");
get_header(); ?>
<div class="wrapper clearfix">
	<nav class="main-nav">
		<ul class="nav">
			
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=86">Create your profile</a></li>
			<li class=""><a href = "http://www.Afroglobalinvestors.com/?page_id=88">Create your project</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=90">Create your company</a></li>
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=92">Write an article</a></li>

							

		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget how-it-works">
			<h4>Follow these basic steps in Writing your article</h4>
			
			<div class="steps">
				<div class="step-number"><span>1</span></div>
				<p>Enter your article title, article introduction and category</p>
				<img src="images/newsarticle.PNG"></img>
			</div>
			
			<div class="steps">
				<div class="step-number"><span>2</span></div>
				<p>Upload images to use in the article and <strong style="color:#800000">Save (top button)</strong></p>
				<img src="images/moreaboutarticle.PNG"></img>
			</div>
			
			<div class="steps">
				<div class="step-number"><span>3</span></div>
				<p>After writing your article <strong style="color:#800000">Save and submit</strong></p>
				<img src="images/submitarticle.PNG"></img>
			</div>
			
		
		<script type="text/javascript">				
		</script>

				
	</section>
	
	<aside class="sidebar sb-right">
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:40px" >Title</h4>
			<ul class="nav">
				<li>Enter a name that best decribes your article <br><a style="color:#800000;font-weight:bold">*NB The  name cannot be changed.</a></br></li>
				<li>Look for availability of the  name (right corner of entry box)</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:150px">Upload images for article</h4>
			<ul class="nav">
				<li>To use images in your article upload them and give them titles</li>
				<li>Make the titles catchy, short and precise</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" style="margin-top:215px" >Save and Submit</h4>
			<ul class="nav">
				<li> Save all changes you made to the article</li>
				<li>View how your article will look like</li>
				<li>Submit the article to be viewed by Afroglobalinvestors visitors</li>
			</ul>
		</div>
		
		
	</aside>
	
	
</div>
<?php get_footer(); ?>