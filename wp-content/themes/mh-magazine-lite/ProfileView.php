<?php /* Template Name: ProfileView */ ?>
<?php get_header(); ?>
<div class="wrapper clearfix" style="min-height:550px">
	
	<nav class="main-nav">
		<ul class="nav">
		
			<li class="active"><a href="" id="user-profile"></a></li>
			<li class=""><a href="" id="user-projects">Projects</a></li>
			<li class=""><a href="" id="user-articles">Articles</a></li>
			
		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget" >
			
			<fieldset class ="entry-field" style="border:none;">
				<!-- the default image -->
				<img src="members/logo.PNG" id="slide" width="260" height="194" style="float:left" />
				
				<h4 class="widget-title expand"  title="show" id="thepicture" style="border-bottom:0;cursor: pointer;text-size:14;margin-top:5px" onclick="showpic()"><img id="expand-img" src="images/right.PNG" style="width:8%;margin-right:10px;">The picture</h4>
				<div style="display:none" class="sidebar_posts" id = "picture-post"> </div>

			</fieldset>
			
			<div class="sb-widget"><h4 class="widget-title">My Story</h4>
				<p  style="border-bottom: 1px solid #dddddd;padding-bottom: 10px;" id = "profile-post"> </p>
			</div>
			
			<script type="text/javascript">
				
				var homeurl = "<?php echo home_url() ; ?>";
				
				var username = "<?php echo $_GET['username']; ?>" ;
			
				if (username == "")
					var username = 'Takunda';
					
				var dir = homeurl+'/members/'+username+'/profile';
				var fileextension = [".PNG", ".jpg"];
				var images = new Array();
				var images_tags = new Array();
				var count = 0;
				var parsed;
				
				
				var picture_tag="";
				var profile_tag="";
				var facebook="";
				var twitter="";
				var website="";
				
				seturls();
				
				function seturls(){
					document.getElementById("user-profile").innerHTML = "About "+username;
					document.getElementById("user-profile").href = "http://www.Afroglobalinvestors.com/?page_id=54&username="+username;
					document.getElementById("user-projects").href = "http://www.Afroglobalinvestors.com/?page_id=97&username="+username;
					document.getElementById("user-articles").href = "http://www.Afroglobalinvestors.com/?page_id=99&username="+username;
				
				}
				
				function showpic(){

					if (document.getElementById("picture-post").style.display == "none"){
						document.getElementById("picture-post").style.display ="";
						document.getElementById("thepicture").setAttribute("title","hide");
						document.getElementById("expand-img").setAttribute("src","images/bottom.PNG");
						document.getElementById("expand-img").setAttribute("style","width:15%;margin-right:10px;");
					}else{
						document.getElementById("picture-post").style.display ="none";
						document.getElementById("thepicture").setAttribute("title","show");
						document.getElementById("expand-img").setAttribute("src","images/right.PNG");
						document.getElementById("expand-img").setAttribute("style","width:8%;margin-right:10px;");
					}
				}
				
				$.ajax({
					//This will retrieve the contents of the folder if the folder is configured as 'browsable'
					url: dir,
					success: function getimages(data) {
						$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () { // 
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
							var filename = dir + "/" + filename;

							images[count] = new Image();
							images[count].src = filename;
							count++;
	 					});
					},
					
					error: function(xhr, textStatus, error){
						  alert(textStatus);
					}
					
				});
				
				try{
				
				$.ajax({
					type: "POST",
					url: homeurl+'/backend/profile_values.php',
					data: {username: username},

					success: function (obj, textstatus) { 
						  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
						  metadata(parsed);
						  
						  profile_tag = parsed['profile'];
						  picture_tag = parsed['imagetag'];
						  facebook = parsed['facebook'];
						  twitter=parsed['twitter'];
						  website=parsed['website'];
						  
					},
					
					error: function(xhr, textStatus, error){
						  alert(textStatus);
					}
				});
				} catch(err){
					alert(err.message);
				}
				
				function metadata(parsed){
					document.getElementById("seo-title").innerHTML = parsed["username"] +" - Profile | Afroglobalinvestors";
					
					var metadesc = document.createElement("meta");
					metadesc.setAttribute("content",parsed["profile"]);
					metadesc.setAttribute("name","description");
					metadesc.setAttribute("itemprop","description");
					
					document.getElementsByTagName("head")[0].appendChild(metadesc);
				}
				
				var step=0
					
				function slideit(){
					 if (!document.images)
					  return
					document.getElementById('slide').className += "fadeOut";
					setTimeout(function() {
						var imagename = images[step].src.substring (images[step].src.lastIndexOf ( '/' ) + 1 );
						
						document.getElementById('slide').src = images[step].src;
						
						
						document.getElementById('slide').className = "";
					},1000);
					
					 if(step> images.length -1) {
						step=0
					 } else if ( step < 0){
						step = images.length -1;
					 }
				}
				
				$(document).ajaxComplete(function() {
				  slideit();
					document.getElementById('profile-post').innerHTML = profile_tag;
					document.getElementById('picture-post').innerHTML = picture_tag;
					
					document.getElementById('facebook-link').innerHTML = facebook;
					document.getElementById('facebook-link').href = facebook;
					
					document.getElementById('twitter-link').innerHTML = twitter;
					document.getElementById('twitter-link').href = twitter;
					
					document.getElementById('website-link').innerHTML = website;
					document.getElementById('website-link').href = website;

				});
				
				$(document).ready(
					function(){

					}
				);
			</script>
			
		</div>
		
		
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar sb-right">
		
		<div class="sb-widget" style="float:center;margin-top:13px;border-bottom: 1px solid #dddddd;padding-bottom: 10px;"><h4 style="float:center;margin-top:2px;"class="widget-title">Links</h4>
			<div class='link'><a class='a-link' href="" id = "facebook-link"> </a> </div>
			<div class='link' ><a class="a-link" href="" id = "twitter-link"> </a> </div>
			<div class='link'><a class="a-link" href="" id = "website-link"> </a> </div>

		</div>
		
		
	</aside>
	
	
</div>

<?php get_footer(); ?>
