<?php /* Template Name: ForgotPassword */ ?>
<?php get_header(); ?>
<div class="wrapper clearfix" id="wrapper">
	
	<div class="about-content" id="about-content">
		
		<div class="entry sb-widget">
			<h4 class="widget-title" Style="font-size:18px;margin-left:35%;width:30%;">Reset your password</h4>
			
			<?php if (isset($_SESSION["post-response"])) : ?>
				<div class="saveinfo" id="saveinfo" style="margin-top:10px;"> <p id = "response"><?php echo $_SESSION["post-response"];
					unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
				</div>
			<?php else : ?>
			<?php endif; ?>
		
			<form  id="searchform" enctype="text/plain">
				<fieldset class="entry-field" style="">
					
					Please enter your E-mail to reset your password:<br>
					<input type="text" name="email" placeholder="your email"><br>
					
					<button type="button" onclick="return resetpassword(document.getElementById('searchform'));" value="Send" style="border-radius:4px;">Reset</button>
					

				</fieldset>
				
			</form>
			
		</div>
		
	</div>
	
	
	
	<aside class="sidebar " style="margin:20px;background:#E66432;border-radius:5px;color:#fff;width:25%;">
		<h4 style="margin:10px;color:#fff;"><i> Sign Up and Explore Africa </i></h4>
		<ul class="nav">
			<li>Raise funds for your project</li><br>
			<li>Advertise your company</li><br>
			<li>Write and read articles on African finance</li><br>
			<li>Agriculture, mining, tourism and much more</li><br>
		</ul>
		
		<div  style="border-bottom:2px solid #800000;margin-bottom:30px;padding-bottom:10px;width:50%;margin-left:20%;"></div>
		
		<button type="button" onclick="document.getElementById('nav-signup').click();" style="margin-left:35%;background:#800000;border-radius:4px;margin-bottom:10px;">Sign up</button>
		
	</aside>
	
	<script type="text/javascript">	
		function removealert(){
			document.getElementById("response").style.display = "none";
		}

	</script>
	
	

</div>
<?php get_footer(); ?>