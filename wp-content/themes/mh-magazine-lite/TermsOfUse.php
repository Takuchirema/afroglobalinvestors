<?php /* Template Name: TermsOfUse */ ?>
<?php 
	include_once("analyticstracking.php");
	
	get_header(); 
	
?>
<div class="wrapper clearfix" id="wrapper" style="width:100%;">
	
	<div class="about-content" id="about-content" style="width:100%;">
		<div style="border-bottom:3px solid rgba(210,105,30,0.7);padding-bottom:10px;">
			<p>
			
			<span Style="font-size:20px;font-weight:800;">TERMS OF USE</span><br><br>
 
			<span Style="font-size:18px;font-weight:800;">TERMS OF WEBSITE USE</span><br><br>
			 
			This page tells you the terms on which you may use this website (http://www.afroglobalinvestors.com). By using this Site, you indicate that you accept these terms of use and that you agree to abide by them. If you do not agree to these terms of use, please refrain from using this Site.<br><br>
			 

			<span Style="font-size:16px;font-weight:800;">ACCESSING OUR SITE</span><br><br>
			 
			Access to this Site is permitted on a temporary basis, and we reserve the right to withdraw or amend the service we provide on this Site without notice. We will not be liable if for any reason our site is unavailable at any time or for any period.
			 
			From time to time, we may restrict your access to some or this entire Site.
			 
			You are responsible for making all arrangements necessary for you to have access to this Site. You are also responsible for ensuring that all persons who access this Site through your internet connection are aware of these terms, and that they comply with them.<br><br>
			 
			<span Style="font-size:16px;font-weight:800;">INTELLECTUAL PROPERTY RIGHTS</span><br><br>
			 
			We are the owner or the licensee of all intellectual property rights in this Site, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.<br><br>
			 
			You must not print off copies or download any extracts from any part of the Site unless expressly authorised by us to do so.
			 
			If you print off, copy or download any part of our site in breach of these terms of use, your right to use our site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.<br><br>
			 
			<span Style="font-size:16px;font-weight:800;">RELIANCE ON INFORMATION POSTED</span><br><br>
			 
			Commentary and other materials posted on this Site are not intended to amount to advice. We are not liable or responsible for any reliance placed on such materials by you or anyone who you may inform of any of its contents.<br><br>
			 
			<span Style="font-size:16px;font-weight:800;">CHANGES TO THE SITE</span><br><br>
			 
			We aim to update this Site regularly, and may change the content at any time. If the need arises, we may suspend access to the Site, or close it indefinitely. In the event of closure, we will advise all visitors well in advance, though we do not see reason of such extreme action in the foreseeable future. Any of the material on this Site may be out of date at any given time, and we are under no obligation to update such material.<br><br>
			 
			<span Style="font-size:16px;font-weight:800;">OUR LIABILITY</span><br><br>
			 
			The material displayed on this Site is provided without any guarantees, conditions or warranties as to its accuracy. To the extent permitted by law, we, other members of our group of companies and third parties connected to us hereby expressly exclude:<br><br>
			 
			All conditions, warranties and other terms which might otherwise be implied by statute, common law or the law of equity.
			 
			Any liability for any direct, indirect or consequential loss or damage incurred by any user in connection with our site or in connection with the use, inability to use, or results or the use of this Site, any websites linked to it and any materials posted on it, including, without limitation any liability for:<br><br>
			 
			<ul style='font-size:16px;'>
				<li>Loss of income or revenue;</li>
				<li>Loss of business;</li>
				<li>Loss of profits or contracts;</li>
				<li>Loss of anticipated savings;</li>
				<li>Loss of data;</li>
				<li>Loss of goodwill;</li>
				<li>Wasted management or office time;</li>
				 
				<li>Any other loss or damage of any kind, however arising and whether caused by tort (including negligence), breach of contract or otherwise, even if foreseeable.</li>
			</ul> <br>
			
			<span style='font-size:16px;'>
			
			This does not affect our liability for death or personal injury arising from our negligence, nor our liability for fraud or fraudulent misrepresentation, nor any other liability which cannot be excluded or limited under applicable law. <br><br>
			 
			<span Style="font-size:16px;font-weight:800;">INFORMATION ABOUT YOU AND YOUR VISITS TO OUR SITE</span> <br><br>
			 
			We process information about you in accordance with our Privacy Policy <a href='http://www.afroglobalinvestors.com/?page_id=121'>http://www.afroglobalinvestors.com/?page_id=121</a>. By using this Site, you consent to such processing and you warrant that all data provided by you is accurate. <br><br>
			 
			<span Style="font-size:16px;font-weight:800;">VIRSUSES, HACKING AND OTHER OFFENCES</span> <br><br>
			 
			You must not misuse this Site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful (together “Viruses”). You must not attempt to gain unauthorised access to our site, the server on which our site is stored or any server, computer or database connected to this Site. You must not attack this Site via a denial-of-service attack. <br><br>
			 
			By breaching this provision, you may commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your rights to use this Site will cease immediately. <br><br>
			 
			We will not be liable for any loss or damage caused by a denial-of-service attack or Viruses that may infect your computer equipment, computer programs, data or other proprietary material due to your use of this Site or to your downloading of any material posted on it, or on any website linked to it. <br><br>
			 
			<span Style="font-size:16px;font-weight:800;">LINKING TO OUR SITE</span> <br><br>
			 
			You may not link to this Site without our prior written permission. <br><br>
			 
			This site must not be framed on any other site without our prior written permission. <br><br>
			 
			We reserve the right to withdraw linking and framing permission without notice.  <br><br>
			 
			If you wish to make any use of material on this Site other than that set out above, please address your request to : <br><br>support@afroglobalinvestors.com. <br><br>
			 
			<span Style="font-size:16px;font-weight:800;">LINKS FROM OUR SITE</span> <br><br>
			 
			Where this Site links to other sites and resources provided by third parties, these links are provided for your information only. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them. <br><br>
			 
			<span Style="font-size:16px;font-weight:800;">JURISDICTION AND APPLICABLE LAW</span> <br><br>
			 
			The English courts will have non-exclusive jurisdiction over any claim arising from, or related to, a visit to our site although we retain the right to bring proceedings against you for breach of these conditions in your country of residence or any other relevant country. These terms of use are governed by English law. <br><br>
			 

			<span Style="font-size:16px;font-weight:800;">VARIATIONS</span> <br><br>
			 
			We may revise these terms of use at any time by amending this page. You are expected to check this page from time to time to take notice of any changes we make, as they are binding on you.  <br><br>
			 
			<span Style="font-size:16px;font-weight:800;">YOUR CONCERNS</span> <br><br>
			 
			If you have any concerns about material which appears on our site, please contact us on the email: <br>support@afroglobalinvestors.com <br><br>
			 
			Thank you for visiting our site. <br><br>
			
			</span>
			
			</p>
		</div>
		
		
	</div>
	
	<script type="text/javascript">	
		function removealert(){
			document.getElementById("response").style.display = "none";
		}

	</script>
	
	

</div>
<?php get_footer(); ?>