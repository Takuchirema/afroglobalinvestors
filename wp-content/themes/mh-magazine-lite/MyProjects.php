<?php /* Template Name: MyProjects */ ?>
<?php get_header(); ?>

<?php if (login_check($mysqli) == true) : ?>

<div class="wrapper clearfix" id="wrapper" >
	<nav class="main-nav" style="width:90%;">
		<ul class="nav">
		
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=58">My Profile</a></li>
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=38">My Projects</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=75">My Articles</a></li>
			
			<button name="create-project" id="create-project" onclick="createproject()">Create new Project</button>
			<button name="create-company" id="create-company" onclick="createcompany('new')">Create company profile</button>

		</ul>
	
	</nav>
	
	<aside class="sidebar projects-sb-left left" id="sidebar-left">
		<h4  ><i>Featured Content</i></h4>
		<div id="sidebar-left-articles"> </div>
		
	</aside>
			
	<section class="projects-container" id="projects-container">
	
		<div class="box alert" id="noprojects">
			<p id="no-projects" >No Projects</p>
		</div>
			
		<!-- <div class="projects-container" id="projects-container">
			<div class="box alert" id="noprojects">
				<p id="no-projects" >No Projects</p>
			</div>
			
		</div> -->
		
		<script type="text/javascript">
			var homeurl = "<?php echo home_url() ; ?>";
			
			var parsed;
			var images = new Array();
			var countprojects = 0;
			
			var username = "<?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} else{echo "" ;} ?>";
				
			//take the projects the user has created
			$.ajax({
				type: "POST",
				url: 'backend/getMyProjects.php',
				data: {username: username,submitted:'no'},

				success: function (obj, textstatus) { 
					  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
					  listprojects();
					  
				},
				
				error: function(xhr, textStatus, error){
					  alert("the err "+error);
				}
			});
			
			//after this is complete loop through the parsed and get the projects
			 function listprojects(){
				if (parsed['success'] == 1)
				{	
					document.getElementById("no-projects").innerHTML = ""; 
					
					for (i=0; i<parsed['projects'].length; i++)
					{
						getImage(parsed['projects'][i]['projectname'],parsed['projects'][i]['company'],i);
						
					}
					
					
					
				} else{
					//get sidebar after the getting of projects
					getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
				}
				
				
			}
		  
			function getImage(projectname,company,index)
			{
				var dir = "projects/"+projectname+"/images/mainimage";
				var fileextension = [".jpg",".png",".jpeg",".gif",".JPG",".PNG",".JPEG",".GIF"];
				var count = 0;
				
				$.ajax({
					url: dir,
					success: function getimages(data) {
						//Select all <a> elements with a href attribute that ends with ".org":
						//$("a[href$='.org']")
						var extensions = "";
						
						for (i=0;i<fileextension.length;i++){
							extensions = extensions +"a[href$='"+ (fileextension[i])+ "'],";
						}
						
						extensions = extensions.substring(0, extensions.length - 1);
						
						$(data).find(extensions).each(function () {
						//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );
							//alert(this.href);
							var filename = dir + "/" + filename;
							//alert("getting for "+projectname+" "+filename);
							images[count] = new Image();
							images[count].src = filename;
							count++;
	 					});
	 
						finished(index,projectname,company);
					},
					error: function showPorject(data) {
						finished(index,projectname);
					}
					
				});
				
				
				function finished(index,projectname,company){
					var viewurl = "";
					
					if (company == "no"){
						viewurl = "http://www.Afroglobalinvestors.com/?page_id=50&project="+projectname;
						viewclass = "blog-list-project";
					} else{
						viewurl = "http://www.Afroglobalinvestors.com/?page_id=101&project="+projectname;
						viewclass = "blog-list-company";
					}
					
					//********** make the height of the container match the projects *******
										
					//**************************************
					
					//create the a element to put the thingsy in 
					var a = document.createElement("a");
					a.setAttribute("class",viewclass+" wow fadeInDown list-project");
					a.setAttribute("data-wow-delay","0.5s");
					a.setAttribute("style","-webkit-animation-delay: 0.5s; -webkit-animation-name: none; padding-top: 1cm;width:100%;");
					a.setAttribute("href",viewurl);
					
					//create the img div
					var imgDiv = document.createElement("div");
					imgDiv.setAttribute("class","blog-image");
					
					if (images[0])
						imgDiv.appendChild(images[0]);
					
					//create the text div
					var txtDiv = document.createElement("div");
					txtDiv.setAttribute("class","blog-excerpt");
					 
					txtDiv.innerHTML = "<h3> "+parsed['projects'][index]['projectname']+"</h3> <h4 class=\"posted-date\"><i class=\"fa fa-calendar\"></i>"+parsed['projects'][index]['date']+"</h4> "+parsed['projects'][index]['projecttag']+ "<br><a class=\"blogoption\" href='"+viewurl+"'>Read More&nbsp;&nbsp;<i class=\"fa fa-angle-right\"></i> </a> <br> <a class=\"blogoption\" href=\"#\" style=\"margin-right:110px\"  onclick = \"editproject('"+projectname+"','"+company+"')\" >Edit&nbsp;&nbsp;<i class=\"fa fa-angle-right\"></i></a>";
					
					//then put everything together
					a.appendChild(imgDiv);
					a.appendChild(txtDiv);
					
					document.getElementById("projects-container").appendChild(a);
					
					
					if (countprojects == parsed['projects'].length - 1){
						//get sidebar after the getting of projects
						getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
					}else {countprojects++;}
					
					//reset images
					images = new Array();
										
				}
			}
			
			function editproject(projectname,company){
				
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"project",name:projectname},
					async: false,
					success: function (obj, textstatus) {
						if (company == 'yes')
							createcompany("not-new");
						else
							location.href='http://www.Afroglobalinvestors.com/?page_id=40';
					
					},
					
				});
		
			}
			
			function createproject(){
				//first unset all project variables that are lingering about
				<?php try { unset($_SESSION['project']); }catch(Exception $ex){} ?>
				<?php try { unset($_SESSION['company']); }catch(Exception $ex){} ?>
				
				location.href='http://www.Afroglobalinvestors.com/?page_id=40'
			}
			
			function createcompany(newcompany){
				if (newcompany =='new') {
				//first unset all project variables that are lingering about
				<?php try { unset($_SESSION['project']); }catch(Exception $ex){} ?>
				//then set a variable for company
				}
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"company",name:"yes"},
					async: false,
					success: function (obj, textstatus) {

						location.href='http://www.Afroglobalinvestors.com/?page_id=40';
					
					},
					
				});
				
			}
			
			

			</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar projects-sb-right " id="sidebar-right">
		
		<h4 style="margin-left:20px"><i>Featured Companies </i></h4>
		<div id="sidebar-right-companies"> </div>
				  
	</aside>
	
	<div class="previous" id="previous" style="display:block;">
		<a href="#page" title="scroll up">
			<img src="images/up.png"></img>
		</a>
	
	</div>
	
	<div class="next" id="next" style="display:block;">
		<a href="#page" title="scroll down">
			<img src="images/down.png"></img>
		</a>
	</div>
			

</div>

<?php get_footer(); ?>

<?php else : ?>
 
	<div class="saveinfo" id="saveinfo" style="margin-top:50px;">
		<p id = "response" style="font-size:18px;">
			You are not authorized to access this page. Please login first
		</p> 
	</div>
	
<?php endif; ?>