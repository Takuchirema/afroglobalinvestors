<?php /* Template Name: MyProfile */ ?>
<?php 
	// Turn off error reporting
	//error_reporting(0);
	//require ('./backend/myprofile.php');
	get_header(); 
?>

<?php if (login_check($mysqli) == true) : ?>

<div class="wrapper clearfix">
	<nav class="main-nav">
				<ul class="nav">
				
					<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=58">My Profile</a></li>
					<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=38">My Projects</a></li>
					<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=75">My Articles</a></li>
					
					<a class="a-button" href="<?php echo 'http://www.Afroglobalinvestors.com/?page_id=54&username='.$_SESSION['username']; ?>"  name="view-profile" id="view-profile">view</a>
					
					<button name="save" id="save" type="button" form="searchform" onclick="return profformhash(document.getElementById('searchform'),
																			   document.getElementById('searchform').name,
																			   document.getElementById('searchform').email,
																			   document.getElementById('searchform').newpassword,
																			   document.getElementById('searchform').currentpwd);">save</button>					

				</ul>
			
			</nav>
	<section class="content <?php mh_content_class(); ?>">
			
		<div class="entry sb-widget">
			<div class="box alert">
				<p>Please enter your profile details.</p>
			</div>
			
			<?php if (isset($_SESSION["post-response"])) : ?>
				<div class="saveinfo" id="saveinfo" > <p id = "response"><?php echo $_SESSION["post-response"];
					unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
				</div>
			<?php else : ?>
			<?php endif; ?>
			
			
			<form id="searchform" enctype="multipart/form-data">
			
			<h4 class="widget-title">Tell us a bit about yourself</h4>
			<fieldset class="entry-field">
				<div id="wysihtml5-toolbar" >
					
					<button data-wysihtml5-command="bold" >bold</button>
					<button data-wysihtml5-command="italic">italic</button>
					<button data-wysihtml5-command="insertUnorderedList">list</button>
					<button data-wysihtml5-command="underline">underline</button>	
					<button  data-wysihtml5-command="indent" >indent</button>
					<button  data-wysihtml5-command="outdent" >outdent</button>
					
					<!-- Some wysihtml5 commands require extra parameters 
					<a data-wysihtml5-command="foreColor" data-wysihtml5-command-value="red">red</a>
					  <a data-wysihtml5-command="foreColor" data-wysihtml5-command-value="green">green</a>
					  <a data-wysihtml5-command="foreColor" data-wysihtml5-command-value="blue">blue</a> -->

					  <!-- Some wysihtml5 commands like 'createLink' require extra paramaters specified by the user (eg. href) 
					  <a data-wysihtml5-command="createLink">insert link</a> 
					  <div data-wysihtml5-dialog="createLink" style="display: none;">
						<label>
						  Link:
						  <input data-wysihtml5-dialog-field="href" value="http://" class="text">
						</label>
						<a data-wysihtml5-dialog-action="save">OK</a> <a data-wysihtml5-dialog-action="cancel">Cancel</a>
					  </div> -->
				</div>
				
				<textarea class=""  id="about" name="about" style="width: 1000px; height: 300px;" maxlength="4000" ></textarea>
				
				<script>
					var editor = new wysihtml5.Editor("about", { // id of textarea element
					toolbar:      "wysihtml5-toolbar", // id of toolbar element
					parserRules:  wysihtml5ParserRules // defined in parser rules set 
					});
				</script>
				<input type="submit" id="searchsubmit" value="save" />
			</fieldset>
			
			<h4 class="widget-title">upload a profile image</h4>
			
			<fieldset class="entry-field" id="image-container">
				<img src="" id="slide" width="260" height="194" style="float:left;display:none" />

				<h4 class="" style="float:right;margin-top:1px;margin-right:55px">About the image</h4>
				 <input class="upload" type="file" name="fileToUpload" id="fileToUpload" onchange="readURL(this)" style="width: 250px; height: 65px;float: left;margin-top:45px"/>
				<textarea class=""  id="picture" name="picture" maxlength= "255" style="width: 300px; height: 75px; float: right;" ></textarea>
			</fieldset>
			
			<h4 class="widget-title" id="update-account-title">Update your account info</h4>
			
			<div class="saveinfo"  > <p id = "account-response" style="display:none;"><a style="color:#fff;" id="update-account-response"></a><a class="remove" onclick="removealert()"></a></p> </div>
			
			<fieldset class="entry-field" onchange="changed()">
				<p id="changed" style="display:none"></p>
				<label for="profile_manager_name">Name</label>
				<input type="text" value="" name="name" id="name" rows="20" readonly />
				<label for="profile_manager_name">Email</label>
				<input type="text" value="" name="myemail" id="myemail" rows="20" maxlength="255" />
				<label for="profile_manager_name">Change your password</label>
				<input type="password" value="" name="newpassword" id="newpassword" rows="20"/>
				
				<div style="">
					<div class="color" style="display:inline-block;" >
						<label style="float:left;">Strength:</label>
						<div class="" id="new-password-strength" style="float:right;margin-left:5px;margin-top:2px;"></div>    
					</div>
					
					<div class="password-strength-meter" id="new-password-score" style="width:50%;"></div>
				</div>
					
				<label for="profile_manager_name">Current password</label>
				<input type="password" value="" name="currentpwd" id="currentpwd" rows="20"/>

				
			</fieldset>
			
			<h4 class="widget-title">Websites and social networks</h4>
			
			<fieldset class="entry-field">
				<label for="profile_manager_name">Website</label>
				<input type="text" value="" name="website" id="website" rows="20" maxlength="255"/>
				<label for="profile_manager_name">Facebook page</label>
				<input type="text" value="" name="facebook" id="facebook" rows="10" maxlength="50" />
				<label for="profile_manager_name">Twitter account</label>
				<input type="text" value="" name="twitter" id="twitter" rows="20" maxlength="50"/>
			</fieldset>
			
			<h4 class="widget-title">mailing address</h4>
			
			<fieldset class="entry-field">
				<label for="profile_manager_name">Address Line 1</label>
				<input type="text" value="" name="addr1" id="addr1" rows="20" maxlength="60" />
				<label for="profile_manager_name">Address Line 2</label>
				<input type="text" value="" name="addr2" id="addr2" rows="10" maxlength="60" />
				<label for="profile_manager_name">City</label>
				<input type="text" value="" name="city" id="city" rows="20" maxlength="35"/>
				<label for="profile_manager_name">State/Region</label>
				<input type="text" value="" name="region" id="region" rows="20" maxlength="55" />
				<label for="profile_manager_name">Zip/Postal Code</label>
				<input type="text" value="" name="postalcode" id="postalcode" rows="10" maxlength="11" />
				<label for="profile_manager_name">Country</label>
				
			<select name="country" id="country">
				<option value="">Country...</option>
				<option value="Afganistan">Afghanistan</option>
				<option value="Albania">Albania</option>
				<option value="Algeria">Algeria</option>
				<option value="American Samoa">American Samoa</option>
				<option value="Andorra">Andorra</option>
				<option value="Angola">Angola</option>
				<option value="Anguilla">Anguilla</option>
				<option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
				<option value="Argentina">Argentina</option>
				<option value="Armenia">Armenia</option>
				<option value="Aruba">Aruba</option>
				<option value="Australia">Australia</option>
				<option value="Austria">Austria</option>
				<option value="Azerbaijan">Azerbaijan</option>
				<option value="Bahamas">Bahamas</option>
				<option value="Bahrain">Bahrain</option>
				<option value="Bangladesh">Bangladesh</option>
				<option value="Barbados">Barbados</option>
				<option value="Belarus">Belarus</option>
				<option value="Belgium">Belgium</option>
				<option value="Belize">Belize</option>
				<option value="Benin">Benin</option>
				<option value="Bermuda">Bermuda</option>
				<option value="Bhutan">Bhutan</option>
				<option value="Bolivia">Bolivia</option>
				<option value="Bonaire">Bonaire</option>
				<option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
				<option value="Botswana">Botswana</option>
				<option value="Brazil">Brazil</option>
				<option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
				<option value="Brunei">Brunei</option>
				<option value="Bulgaria">Bulgaria</option>
				<option value="Burkina Faso">Burkina Faso</option>
				<option value="Burundi">Burundi</option>
				<option value="Cambodia">Cambodia</option>
				<option value="Cameroon">Cameroon</option>
				<option value="Canada">Canada</option>
				<option value="Canary Islands">Canary Islands</option>
				<option value="Cape Verde">Cape Verde</option>
				<option value="Cayman Islands">Cayman Islands</option>
				<option value="Central African Republic">Central African Republic</option>
				<option value="Chad">Chad</option>
				<option value="Channel Islands">Channel Islands</option>
				<option value="Chile">Chile</option>
				<option value="China">China</option>
				<option value="Christmas Island">Christmas Island</option>
				<option value="Cocos Island">Cocos Island</option>
				<option value="Colombia">Colombia</option>
				<option value="Comoros">Comoros</option>
				<option value="Congo">Congo</option>
				<option value="Cook Islands">Cook Islands</option>
				<option value="Costa Rica">Costa Rica</option>
				<option value="Cote DIvoire">Cote D'Ivoire</option>
				<option value="Croatia">Croatia</option>
				<option value="Cuba">Cuba</option>
				<option value="Curaco">Curacao</option>
				<option value="Cyprus">Cyprus</option>
				<option value="Czech Republic">Czech Republic</option>
				<option value="Denmark">Denmark</option>
				<option value="Djibouti">Djibouti</option>
				<option value="Dominica">Dominica</option>
				<option value="Dominican Republic">Dominican Republic</option>
				<option value="East Timor">East Timor</option>
				<option value="Ecuador">Ecuador</option>
				<option value="Egypt">Egypt</option>
				<option value="El Salvador">El Salvador</option>
				<option value="Equatorial Guinea">Equatorial Guinea</option>
				<option value="Eritrea">Eritrea</option>
				<option value="Estonia">Estonia</option>
				<option value="Ethiopia">Ethiopia</option>
				<option value="Falkland Islands">Falkland Islands</option>
				<option value="Faroe Islands">Faroe Islands</option>
				<option value="Fiji">Fiji</option>
				<option value="Finland">Finland</option>
				<option value="France">France</option>
				<option value="French Guiana">French Guiana</option>
				<option value="French Polynesia">French Polynesia</option>
				<option value="French Southern Ter">French Southern Ter</option>
				<option value="Gabon">Gabon</option>
				<option value="Gambia">Gambia</option>
				<option value="Georgia">Georgia</option>
				<option value="Germany">Germany</option>
				<option value="Ghana">Ghana</option>
				<option value="Gibraltar">Gibraltar</option>
				<option value="Great Britain">Great Britain</option>
				<option value="Greece">Greece</option>
				<option value="Greenland">Greenland</option>
				<option value="Grenada">Grenada</option>
				<option value="Guadeloupe">Guadeloupe</option>
				<option value="Guam">Guam</option>
				<option value="Guatemala">Guatemala</option>
				<option value="Guinea">Guinea</option>
				<option value="Guyana">Guyana</option>
				<option value="Haiti">Haiti</option>
				<option value="Hawaii">Hawaii</option>
				<option value="Honduras">Honduras</option>
				<option value="Hong Kong">Hong Kong</option>
				<option value="Hungary">Hungary</option>
				<option value="Iceland">Iceland</option>
				<option value="India">India</option>
				<option value="Indonesia">Indonesia</option>
				<option value="Iran">Iran</option>
				<option value="Iraq">Iraq</option>
				<option value="Ireland">Ireland</option>
				<option value="Isle of Man">Isle of Man</option>
				<option value="Israel">Israel</option>
				<option value="Italy">Italy</option>
				<option value="Jamaica">Jamaica</option>
				<option value="Japan">Japan</option>
				<option value="Jordan">Jordan</option>
				<option value="Kazakhstan">Kazakhstan</option>
				<option value="Kenya">Kenya</option>
				<option value="Kiribati">Kiribati</option>
				<option value="Korea North">Korea North</option>
				<option value="Korea Sout">Korea South</option>
				<option value="Kuwait">Kuwait</option>
				<option value="Kyrgyzstan">Kyrgyzstan</option>
				<option value="Laos">Laos</option>
				<option value="Latvia">Latvia</option>
				<option value="Lebanon">Lebanon</option>
				<option value="Lesotho">Lesotho</option>
				<option value="Liberia">Liberia</option>
				<option value="Libya">Libya</option>
				<option value="Liechtenstein">Liechtenstein</option>
				<option value="Lithuania">Lithuania</option>
				<option value="Luxembourg">Luxembourg</option>
				<option value="Macau">Macau</option>
				<option value="Macedonia">Macedonia</option>
				<option value="Madagascar">Madagascar</option>
				<option value="Malaysia">Malaysia</option>
				<option value="Malawi">Malawi</option>
				<option value="Maldives">Maldives</option>
				<option value="Mali">Mali</option>
				<option value="Malta">Malta</option>
				<option value="Marshall Islands">Marshall Islands</option>
				<option value="Martinique">Martinique</option>
				<option value="Mauritania">Mauritania</option>
				<option value="Mauritius">Mauritius</option>
				<option value="Mayotte">Mayotte</option>
				<option value="Mexico">Mexico</option>
				<option value="Midway Islands">Midway Islands</option>
				<option value="Moldova">Moldova</option>
				<option value="Monaco">Monaco</option>
				<option value="Mongolia">Mongolia</option>
				<option value="Montserrat">Montserrat</option>
				<option value="Morocco">Morocco</option>
				<option value="Mozambique">Mozambique</option>
				<option value="Myanmar">Myanmar</option>
				<option value="Nambia">Nambia</option>
				<option value="Nauru">Nauru</option>
				<option value="Nepal">Nepal</option>
				<option value="Netherland Antilles">Netherland Antilles</option>
				<option value="Netherlands">Netherlands (Holland, Europe)</option>
				<option value="Nevis">Nevis</option>
				<option value="New Caledonia">New Caledonia</option>
				<option value="New Zealand">New Zealand</option>
				<option value="Nicaragua">Nicaragua</option>
				<option value="Niger">Niger</option>
				<option value="Nigeria">Nigeria</option>
				<option value="Niue">Niue</option>
				<option value="Norfolk Island">Norfolk Island</option>
				<option value="Norway">Norway</option>
				<option value="Oman">Oman</option>
				<option value="Pakistan">Pakistan</option>
				<option value="Palau Island">Palau Island</option>
				<option value="Palestine">Palestine</option>
				<option value="Panama">Panama</option>
				<option value="Papua New Guinea">Papua New Guinea</option>
				<option value="Paraguay">Paraguay</option>
				<option value="Peru">Peru</option>
				<option value="Phillipines">Philippines</option>
				<option value="Pitcairn Island">Pitcairn Island</option>
				<option value="Poland">Poland</option>
				<option value="Portugal">Portugal</option>
				<option value="Puerto Rico">Puerto Rico</option>
				<option value="Qatar">Qatar</option>
				<option value="Republic of Montenegro">Republic of Montenegro</option>
				<option value="Republic of Serbia">Republic of Serbia</option>
				<option value="Reunion">Reunion</option>
				<option value="Romania">Romania</option>
				<option value="Russia">Russia</option>
				<option value="Rwanda">Rwanda</option>
				<option value="St Barthelemy">St Barthelemy</option>
				<option value="St Eustatius">St Eustatius</option>
				<option value="St Helena">St Helena</option>
				<option value="St Kitts-Nevis">St Kitts-Nevis</option>
				<option value="St Lucia">St Lucia</option>
				<option value="St Maarten">St Maarten</option>
				<option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
				<option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
				<option value="Saipan">Saipan</option>
				<option value="Samoa">Samoa</option>
				<option value="Samoa American">Samoa American</option>
				<option value="San Marino">San Marino</option>
				<option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
				<option value="Saudi Arabia">Saudi Arabia</option>
				<option value="Senegal">Senegal</option>
				<option value="Serbia">Serbia</option>
				<option value="Seychelles">Seychelles</option>
				<option value="Sierra Leone">Sierra Leone</option>
				<option value="Singapore">Singapore</option>
				<option value="Slovakia">Slovakia</option>
				<option value="Slovenia">Slovenia</option>
				<option value="Solomon Islands">Solomon Islands</option>
				<option value="Somalia">Somalia</option>
				<option value="South Africa">South Africa</option>
				<option value="Spain">Spain</option>
				<option value="Sri Lanka">Sri Lanka</option>
				<option value="Sudan">Sudan</option>
				<option value="Suriname">Suriname</option>
				<option value="Swaziland">Swaziland</option>
				<option value="Sweden">Sweden</option>
				<option value="Switzerland">Switzerland</option>
				<option value="Syria">Syria</option>
				<option value="Tahiti">Tahiti</option>
				<option value="Taiwan">Taiwan</option>
				<option value="Tajikistan">Tajikistan</option>
				<option value="Tanzania">Tanzania</option>
				<option value="Thailand">Thailand</option>
				<option value="Togo">Togo</option>
				<option value="Tokelau">Tokelau</option>
				<option value="Tonga">Tonga</option>
				<option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
				<option value="Tunisia">Tunisia</option>
				<option value="Turkey">Turkey</option>
				<option value="Turkmenistan">Turkmenistan</option>
				<option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
				<option value="Tuvalu">Tuvalu</option>
				<option value="Uganda">Uganda</option>
				<option value="Ukraine">Ukraine</option>
				<option value="United Arab Erimates">United Arab Emirates</option>
				<option value="United Kingdom">United Kingdom</option>
				<option value="United States of America">United States of America</option>
				<option value="Uraguay">Uruguay</option>
				<option value="Uzbekistan">Uzbekistan</option>
				<option value="Vanuatu">Vanuatu</option>
				<option value="Vatican City State">Vatican City State</option>
				<option value="Venezuela">Venezuela</option>
				<option value="Vietnam">Vietnam</option>
				<option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
				<option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
				<option value="Wake Island">Wake Island</option>
				<option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
				<option value="Yemen">Yemen</option>
				<option value="Zaire">Zaire</option>
				<option value="Zambia">Zambia</option>
				<option value="Zimbabwe">Zimbabwe</option>
			</select>
		</fieldset>
			
			</form>
		</div>
		
		<script type="text/javascript">
		
				var homeurl = "<?php echo home_url() ; ?>";
				
				var username = "<?php if(isset($_SESSION['username'])){echo $_SESSION['username'];} else{echo "" ;} ?>";
				
					
				var dir = 'members/'+username+'/profile';
				var fileextension = [".png", ".jpg"];
				var images = new Array();
				var images_tags = new Array();
				var count = 0;
				var parsed;
				
				getProfileImage();
				
				function viewprofile(){
					location.href = "http://www.Afroglobalinvestors.com/?page_id=54&username="+username;
				}
			
				function getProfileImage(){
					$.ajax({
						url: dir,
						success: function getimages(data) {
							//List all png file names in the page
							$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () { // 
								var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );

								var filename = dir + "/" + filename;

								images[count] = new Image();
								images[count].src = filename;
								count++;
								
		 				});
							
							profilevalues();
						},
						
						error: function(xhr, textStatus, error){
							  alert(textStatus+" in profile");
						}
						
					});
				}
				
				function profilevalues() {

					$.ajax({
						type: "POST",
						url: 'backend/profile_values.php',
						data: {username: username},   //got to put this value dynamically

						success: function (obj, textstatus) { 
							  
							  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
							  fillform(parsed);
							  							  
							  
						},
						
						error: function(xhr, textStatus, error){
							  alert(textStatus+" in profile 1");
						}
					});
				
				}
				
				
				function changed(){
					document.getElementById("changed").innerHTML = "true";
				
				}
				
				function removealert(){
					document.getElementById("account-response").style.display = "none";
					document.getElementById("response").style.display = "none";
				}
		
				
				
				function fillform(parsed) {
				  	
					getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
					
					editor.setValue(parsed['profile']);
					document.getElementById('picture').value =  parsed['imagetag'];
					
					document.getElementById('name').value = parsed['username'];
					document.getElementById('myemail').value =parsed['email'];
					
					
					document.getElementById('facebook').value = parsed['facebook'];
					document.getElementById('twitter').value = parsed['twitter'];
					document.getElementById('website').value = parsed['website'];
					
					
					document.getElementById('addr1').value = parsed['addr1'];
					document.getElementById('addr2').value = parsed['addr2'];
					document.getElementById('city').value = parsed['city'];
					
					
					document.getElementById('region').value = parsed['region'];
					document.getElementById('postalcode').value = parsed['postalcode'];
					document.getElementById('country').value = parsed['country'];
					
					
					document.getElementById("slide").style.display = "";
					
					document.getElementById("slide").src = images[0].src;
					
					
					

				}
				
				$(document).ready(
					function(){
						
						$("#newpassword").on("keypress keyup keydown", function() {
							var pass = $(this).val();
							$("#new-password-strength").text(checkPassStrength(pass));
							
							
							var score = parseInt(scorePassword(pass));
							var rgb1 = (100 - score) / 100 * 255;
							var rgb2 = score /100 * 255;
							
							document.getElementById('new-password-score').style.backgroundColor = "rgb("+rgb1+","+rgb2+",0)";
						});
									
					}
				);
				
					
			
			 function readURL(input) {
				var i;
				
				if (input.files && input.files[0]) {
					
					for(i=0;i<input.files.length;i++)
					{
						var reader = new FileReader();
						var position = i;
						var count = 0;
						
						//note the name must be taken straight fromt the e somehow
						reader.onload = function (e) {
							
							var elem = document.createElement("img");
							
							elem.setAttribute("src", e.target.result);
							elem.setAttribute("height", "130");
							elem.setAttribute("width", "130");
							elem.setAttribute("style", "padding: 5px");
							elem.setAttribute("class", "img-upload");
							elem.setAttribute("name",input.files[count].name);
							
							document.getElementById("slide").style.display = "";
							
							
							document.getElementById("slide").src = e.target.result ;
					
							
							count = count + 1;
							
						};

						reader.readAsDataURL(input.files[i]);
					}
					
				}
			}
			</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar sb-right">
		
		<div class="sb-widget"><h4 class="widget-title">About yourself</h4>
			<ul class="nav">
				<li>Investors would like to know more about you. Your career, your studies and Highlights etc.</li>
				<li>Try and keep it short</li>
          </ul>
		  
		  <div class="sb-widget"><h4 class="widget-title">Example</h4>
			<p> Afro is a student at the University . . . <br> He is studying Computer Science and . . . <br> Afro's passion for technology has . . . .<br> His interests lie in music, art . . . . </p>
		  </div>
		  
		</div>
		
		<div class="sb-widget"><h4 class="widget-title">The picture</h4>
			<ul class="nav">
				<li>A picture is worth a million words</li>
				<li>Add a short story line to make it all the more interesting: Where was it taken, what was the event etc</li>
          </ul>
		</div>
		
		<div class="sb-widget" id="sidebar-right" >
			<h4 style="margin-left:1px"><i>Featured Companies </i></h4>
			<div id="sidebar-right-companies"> </div>
			
		</div>
		
	</aside>
	
	<div class="previous" id="previous" style="display:block;">
		<a href="#page" title="scroll up">
			<img src="images/up.png"></img>
		</a>
	
	</div>
	
	<div class="next" id="next" style="display:block;">
		<a href="#page" title="scroll down">
			<img src="images/down.png"></img>
		</a>
	</div>
	
</div>

<?php get_footer(); ?>

<?php else : ?>
 
	<div class="saveinfo" id="saveinfo" style="margin-top:100px;">
		<p id = "response" style="font-size:18px;">
			You are not authorized to access this page. Please login first
		</p> 
	</div>
	
<?php endif; ?>