<?php /* Template Name: ProjectFunding */ ?>
<?php get_header(); ?>

<?php if (login_check($mysqli) == true) : ?>

<div class="wrapper clearfix">
	<nav class="main-nav">
		<ul class="nav">
		
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=40">Project Basics</a></li>
			<li class=""><a href="http://www.Afroglobalinvestors.com/?page_id=43">Project Details</a></li>
			<li class="active"><a href="http://www.Afroglobalinvestors.com/?page_id=38">Project Funding</a></li>
			
			<?php if (isset($_SESSION["project"])) : ?>
				<button onclick="deleteproject()" style="" name="delete" title="delete the project" id="delete">delete</button>
				
				<a class="a-button" href="<?php echo 'http://www.Afroglobalinvestors.com/?page_id=50&project='.$_SESSION['project']; ?>">view</a>
				
				<button onclick="submitproject()" title="please save first before submitting" name="submit" id="submit">submit</button>
				
				<button name="save" id="save"  onclick="return projrformhash(document.getElementById('searchform'),
																				document.getElementById('searchform').funding,
																			   document.getElementById('searchform').equity,
																			   document.getElementById('searchform').equityvalue,
																			   document.getElementById('searchform').debt,
																			   document.getElementById('searchform').debtinterest,
																			   document.getElementById('searchform').donations,
																			   '<?php echo $_SESSION["project"]; ?>');"> Save </button>		
				
																			   
			<?php else : ?>
				<button name="save" id="save" type="submit" form="searchform" onclick="createproject()">save</button>
			<?php endif; ?>															   

		</ul>
	
	</nav>
			
	<section class="content <?php mh_content_class(); ?>">
	
		<div class="entry sb-widget">
			<div class="box alert">
				<p>Please fill in below</p>
			</div>
			
			<?php if (isset($_SESSION["post-response"])) : ?>
				<div class="saveinfo" id="saveinfo" > <p id = "response"><?php echo $_SESSION["post-response"];
					unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
				</div>
			<?php else : ?>
			<?php endif; ?>
			
			<form  id="searchform" >
			
			<h4 class="widget-title">Funding goal</h4>
			
			<fieldset class="entry-field">
				<input type="numeric" placeholder="e.g 5000 ($USD)" name="funding" id="funding" rows="20" maxlength="11" />	
				<p class="check_project" name="check_project" id="check_project" style="float: left;margin-left:0px;margin-top:5px;color:#800000;font-weight:600;"></p>
			</fieldset>
			
			<h4 class="widget-title">Funding Duration</h4>
			
			<fieldset class="entry-field" >
				<p id="mydatepicker">Pick a Date: </p>
				<input type="text" readonly="readonly" name="datepicker" id="datepicker" />
			</fieldset>
			
			<h4 class="widget-title">Funding Details</h4>
			
			<fieldset class="entry-field">
			
				<div id="wysihtml5-toolbar" >
						
					<button data-wysihtml5-command="bold" ><strong>B</strong></button>
					<button data-wysihtml5-command="italic"><i>IT</i></button>
					<button data-wysihtml5-command="insertUnorderedList">list</button>
					<button data-wysihtml5-command="underline">underline</button>	
					<button  data-wysihtml5-command="indent" >indent</button>
					<button  data-wysihtml5-command="outdent" >outdent</button>
					<!-- <button data-wysihtml5-command="insertImage">insert image</button>
					
					
					<div data-wysihtml5-dialog="insertImage">
					  <label>
						Image:
						<input data-wysihtml5-dialog-field="src" value="http://">
					  </label>
					  <button type="button" data-wysihtml5-dialog-action="save">OK</button>
					  <button type="button" data-wysihtml5-dialog-action="cancel">Cancel</button>
					</div> -->
							
				</div>
				
				<textarea class="" cols="20" id="fundingdetails" name="fundingdetails" rows="20" maxlength="4000"></textarea>
				
				<script>
					var editor = new wysihtml5.Editor("fundingdetails", { // id of textarea element
					toolbar:      "wysihtml5-toolbar", // id of toolbar element
					parserRules:  wysihtml5ParserRules // defined in parser rules set 
					});
					
					
				</script>
				
			</fieldset>
			
			<h4 class="widget-title">Deal Type</h4>
			
			<fieldset class="entry-field" class ="rewards" id ="rewards" style="display: block">
				
				<div style="background: #111;margin-top:5px" class="project-bar">
					<button type="button" class="dormant" id="deal-equity">Equity</button>
					<button type="button" class="dormant" id="deal-debt">Debt</button>
					<button type="button" class="dormant"  id="deal-donations">Donations</button>
				</div>
				
				<!-- <ul class="deals-nav" id="deals-nav" >
					<li><a class="" href="#" id="deal-equity">Equity</a></li>
				    <li><a class=""  href="#"  id="deal-debt">Debt</a></li>
				   <li><a class="" href="#"  id="deal-donations">Donations</a></li>
		
				</ul> -->
				
				<label class="deals-label" style ="float:left;display:none;width:100%" name="equity-label" id="equity-label"> enter the amount to be raised by equity </label>
				
				<input class="deals-input" type="text" style="display:none;" name="equity" id="equity" placeholder="e.g. 5000 " maxlength="11"> </input>
				
				<label class="deals-label" style="display:none;width:100%" name="equity-price-label" id="equity-price-label"> enter the price per share </label>
				
				<input class="deals-input" type="text" style="display:none;" name="equityvalue" id="equityvalue" placeholder="e.g. 50 " maxlength="11"> </input>
				
				<label class="deals-label" style ="float:left;display:none;width:100%" name="debt-label" id="debt-label">enter the amount to be raised by debt</label>
				
				<input class="deals-input" type="text" style="display:none;" name="debt" id="debt" placeholder="e.g. 5000 " maxlength="11"> </input>
				
				<label class="deals-label" style="display:none;width:100%" name="debt-interest-label" id="debt-interest-label">enter the interest rate</label>
				
				<input class="deals-input" type="text" style="display:none;" name="debtinterest" id="debtinterest" placeholder="e.g. 11 " maxlength="3"> </input>
				
				<label class="deals-label" style ="float:left;display:none;width:100%" name="donations-label" id="donations-label">enter the amount to be raised by donations</label>
				
				<input class="deals-input" type="text" style="display:none;" name="donations" id="donations" placeholder="e.g. 5000 " maxlength="11"> </input>
			
				
			</fieldset>
						
			</form>
			
		</div>
		
		<script type="text/javascript">
			var projectname = "<?php if(isset($_SESSION['project'])){echo $_SESSION['project'];} else{echo "" ;} ?>";
			
			var homeurl = "<?php echo home_url() ; ?>";
			
			if (projectname != ""){
				getProjectDetails();
				
			}else{
				location.href='http://www.Afroglobalinvestors.com/?page_id=38';
			}
			
			function getProjectDetails(){
				$.ajax({
					type: "POST",
					url: 'backend/getProject.php',

					data: {projectname: projectname},

					success: function (obj, textstatus) { 
						  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
						  
						  fillform(parsed);
						   
					},
					
					error: function(xhr, textStatus, error){
						  alert("the err "+error);
					}
				});
			}
			
			//regex for testing that funding is a number
			re = /^[0-9]*$/; 
			
			$("#funding").on("keypress keyup keydown", function() {
				var money = document.getElementById("funding").value;
				
				if(!re.test(money)) { 
				
					document.getElementById("check_project").innerHTML = "please enter a numerical value";

				} else {
					document.getElementById("check_project").innerHTML = "";
				}

			});
			
			function removealert(){
				document.getElementById("response").style.display = "none";
			}
		
			function gotoproject(){
			
				location.href = 'http://www.Afroglobalinvestors.com/?page_id=50&project='+"<?php echo $_SESSION['project']; ?>";
			}
			
			function deleteproject(){
				
				if (projectname != ""){
					var r = confirm("You are about to delete the project "+projectname );
					
					if (r == true) {
						
						$.ajax({
							type: 'POST',
							url: "./backend/deleteProject.php",
							data: {project:projectname},
							async: false,
							success: function (obj, textstatus) {
								parsed = jQuery.parseJSON(obj);
								
								deleteresponse(parsed);
								
							}
							
						});
						
					}
				}

			}
			
			function deleteresponse(parsed){
			
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {response:parsed['message'],unset:"project"},
					async: false,
					success: function (obj, textstatus) {
						//alert(parsed["message"]);
						location.href='http://www.Afroglobalinvestors.com/?page_id=108';
					},
					
				});
				
			}
			
			
			function submitproject() {
				var projectname = "<?php if(isset($_SESSION['project'])){echo $_SESSION['project'];} else{echo "" ;} ?>";
				
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				
				var date = yyyy+"-"+mm+"-"+dd;
				
				$.ajax({
					type: 'POST',
					url: "./backend/submitProject.php",
					data: {project:projectname,submitdate:date},
					async: false,
					success: function (obj, textstatus) {
						parsed = jQuery.parseJSON(obj);
						
						submitresponse(parsed);
					
					}
					
					
				});
			}
			
			function submitresponse(parsed){
		
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {response:parsed["message"]},
					async: false,
					success: function (obj, textstatus) {
						
						if(parsed['success'] == 1){
							editSiteMap('http://www.afroglobalinvestors.com/project/'+projectname);
							location.href='http://www.Afroglobalinvestors.com/?page_id=50&project='+projectname;
						}else{
							location.reload();
						}
					
					},
					
				});
			
			}
			
			function fillform(projdetails){
			
				document.getElementById("funding").value = projdetails["project"][0]["fundinggoal"];
				document.getElementById("datepicker").value = projdetails["project"][0]["date"];
				
				document.getElementById("equity").value = projdetails["project"][0]["equity"];
				document.getElementById("equityvalue").value = projdetails["project"][0]["equityvalue"];
				
				document.getElementById("debt").value = projdetails["project"][0]["debt"];
				document.getElementById("debtinterest").value = projdetails["project"][0]["debtinterest"];
				
				document.getElementById("donations").value = projdetails["project"][0]["donations"];
				
				editor.setValue(projdetails["project"][0]["fundingdetails"]);
				
				if (projdetails["project"][0]["submitted"] == "yes"){
					document.getElementById("submit").style.display = "none";
					document.getElementById("delete").style.display = "none";					
				}
			}
			
		</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	<aside class="sidebar sb-right">
		<div class="sb-widget"><h4 class="widget-title" >Funding goal</h4>
			<ul class="nav">
				<li>Enter the total amount to be raised for all funding types</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" >Funding duration</h4>
			<ul class="nav">
				<li>A project cannot run for less than 30 days</li>
				<li>This is the duration for publishing on the main projects list (see About projects for more details)</li>
				<li>The project starts on the day of submission</li>
			</ul>
		</div>
		
		<div class="sb-widget" style="width:100%"><h4 class="widget-title" >Funding details</h4>
			<ul class="nav">
				<li>This is perhaps the most crucial information to get funding</li>
				<li>Create in bullet form how investors/donors can fund the project</li>
				<li>Points to include
					<br><i style="color:#800000;font-weight:600;">Partners in the project e.g. companies</i>
					<br><i style="color:#E66432;font-weight:600;">Is it a registered company or in an association?</i>
					<br><i style="color:#800000;font-weight:600;">Do you have credible recommendations e.g. recognised organisations?</i>
					<br><i style="color:#E66432;font-weight:600;">Provide contact information which is valid and up to date. Skype is a good start</i>
					<li>This list is far from being exhaustive. Put any other information you think is relevant</li>
					<li>There is no short-cut, investors/donors want to be sure they are paying to a legitimate project</li>
				</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" >Equity</h4>
			<ul class="nav">
				<li>Enter the total amount to be raised by equity and the price for each share</li>
				<li>e.g $5000 at $5 per share means 1000 shares available</li>
				<li>Shares are one of the quickest way to get funding</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" >Debt</h4>
			<ul class="nav">
				<li>Enter the total amount to be raised by debt and the interest rate you are offering (compound interest)</li>
				<li>Aside from equity debt is one of the best ways to raise funding</li>
			</ul>
		</div>
		
		<div class="sb-widget"><h4 class="widget-title" >Donations</h4>
			<ul class="nav">
				<li>A donation leaves no obligation to the funder</li>
			</ul>
		</div>
		
		
	</aside>
</div>

<?php get_footer(); ?>

<?php else : ?>
 
	<div class="saveinfo" id="saveinfo" style="margin-top:50px;">
		<p id = "response" style="font-size:18px;">
			You are not authorized to access this page. Please login first
		</p> 
	</div>
	
<?php endif; ?>