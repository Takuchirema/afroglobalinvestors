<?php /* Template Name: About */ ?>
<?php 
	include_once("analyticstracking.php");
	
	get_header(); 
	
?>
<div class="wrapper clearfix" id="wrapper">
	
	<div class="about-content" id="about-content">
		<div style="border-bottom:3px solid rgba(210,105,30,0.7);padding-bottom:10px;">
			<p>
			
			Are you a part of the African Diaspora looking to invest back into your home country, but you don’t even have the slightest clue where to start? OR an investor looking to invest in the expanding and resourceful African Continent? This is the platform for you to invest or donate to African projects as well as the rightful platform to get professional advice on how best you can go about it, spanning from bespoke strategy formulation, your required investment horizon, your desired rate of return (considering your risk appetite et cetera), how realistic your intentions are among other things.<br><br>

			Afroglobalinvestors is a platform for investors and donors to meet with project owners in the whole of the African continent which are just emerging,  making a difference to the communities, countries and regions they are in. As startups these projects face funding difficulties and thus the need of a platform such as this for interested investors to meet promising, innovative and inspired projects in Africa. <br><br>

			Are you the founder of a project in your African community? OR are you running a company in Africa that needs funding? OR do you have an idea which you think is being barred to materialise because of lack of finance? This is the right platform for you to meet with potential investors and partners for your project. Sign up now and showcase your project to the world. The steps are easy check on your right for more details.<br><br>

			Are there interesting projects and/or initiatives in your African community, country or region that you think they need to be known to the world? OR are you a writer interested in business and finance articles? Then write an article in one easy step and publish it on Afroglobalinvestors.<br><br>
			
			</p>
		</div>
		
		
		
		<div class="entry sb-widget">
			<h4 id="contact-us" class="widget-title" Style="font-size:18px;margin-left:35%;width:15%;">Contact us</h4>
			
			<?php if (isset($_SESSION["post-response"])) : ?>
				<div class="saveinfo" id="saveinfo" style="margin-top:10px;"> <p id = "response"><?php echo $_SESSION["post-response"];
					unset($_SESSION['post-response']);?> <a class="remove" onclick="removealert()"></a> </p> 
				</div>
			<?php else : ?>
			<?php endif; ?>
		
			<form  id="searchform" enctype="text/plain">
				<fieldset class="entry-field" style="">
					Name:<br>
					<input type="text" name="name" placeholder="your name"><br>
					E-mail:<br>
					<input type="text" name="email" placeholder="your email"><br>
					Comment:<br>
					<textarea type="text" name="message" placeholder="your message" cols="5" rows="5" > </textarea><br><br>
					<button type="button" onclick="return sendemail(document.getElementById('searchform'));" value="Send" style="border-radius:4px;">Send</button>
					<button type="reset" value="Reset" style="border-radius:4px;">Reset</button>

				</fieldset>
				
			</form>
			
		</div>
		
	</div>
	
	
	
	<aside class="sidebar " style="margin:20px;background:#E66432;border-radius:5px;color:#fff;width:25%;">
		<h4 style="margin:10px;color:#fff;"><i> Sign Up and Explore Africa </i></h4>
		<ul class="nav">
			<li>Raise funds for your project</li><br>
			<li>Advertise your company</li><br>
			<li>Write and read articles on African finance</li><br>
			<li>Agriculture, mining, tourism and much more</li><br>
		</ul>
		
		<div  style="border-bottom:2px solid #800000;margin-bottom:30px;padding-bottom:10px;width:50%;margin-left:20%;"></div>
		
		<button type="button" onclick="document.getElementById('nav-signup').click();" style="margin-left:35%;background:#800000;border-radius:4px;margin-bottom:10px;">Sign up</button>
		
	</aside>
	
	<script type="text/javascript">	
		function removealert(){
			document.getElementById("response").style.display = "none";
		}

	</script>
	
	

</div>
<?php get_footer(); ?>