<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package accesspress_parallax
 */
include_once './backend/includes/functions.php';

sec_session_start();

include_once './backend/includes/db_connect.php';

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title id="seo-title" ><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/jquery-ui.css">

<script src="<?php echo get_template_directory_uri(); ?>/js/application.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/mainpage/wow.js"></script>

<!-- the scripts for rich text -->
<script src="<?php echo get_template_directory_uri(); ?>/js/advanced.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/wysihtml5-0.3.0.min.js"></script>

<!-- for validating the inputs -->
<script src="<?php echo get_template_directory_uri(); ?>/js/sha512.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/forms.js"></script>

<!-- scripts for the datepicker-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.maxlength.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/global.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/password-strength-meter.js"></script>

 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
 
<link rel="stylesheet" id="dashicons-css" href="<?php echo get_template_directory_uri(); ?>/style_access.css" type="text/css" media="all">
<link rel="stylesheet" id="dashicons-css" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" media="all">
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body class = 'home blog logged-in admin-bar parallax-on customize-support'>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
    $(function() {
        $( "#datepicker" ).datepicker({
			minDate: +30, //maxDate: "+90D",
			dateFormat: 'yy-mm-dd' 
		});
		
        
    });
	
	function removeHeaderAlert(){
		document.getElementById("signup-header-response").style.display = "none";
		document.getElementById("login-header-response").style.display = "none";
	}
</script>


<div id="page" class="hfeed site">
	<header id="masthead" class="logo-side">
		<div class="mid-content clearfix">
		<div id="site-logo">
					<h1 class="site-title"><a href="http://www.Afroglobalinvestors.com/" rel="home">afroglobalinvestors</a></h1>
					<h2 class="site-description">Investments in Africa</h2>
		</div>

		<nav id="site-navigation" class="main-navigation">
		
		<div class="menu-toggle" id="menu" >Menu</div>
			
			<ul class="nav">
			
				<li class="current"><a href="http://www.Afroglobalinvestors.com">Home</a></li>
				 <?php if (login_check($mysqli) == true) : ?>
					<li class="current"><a href="http://www.Afroglobalinvestors.com/?page_id=58" ><?php echo $_SESSION['username']; ?></a></li>
					<li class="current"><a href="http://www.Afroglobalinvestors.com/?page_id=86" id="nav-learn">Learn</a></li>
					<li class="current"><a href="./backend/includes/logout.php"  >Log Out</a></li>
				<?php else : ?>
					<li class="current"><a href="#header-signup" id="nav-signup">Sign up</a></li>
					<li class="current"><a href="#header-login" id="nav-login">Log In</a></li>
					<!--<li class="current"><a href="http://www.Afroglobalinvestors.com/?page_id=73" id="nav-login">Write an article</a></li> -->
					<li class="current"><a href="http://www.Afroglobalinvestors.com/?page_id=86" id="nav-learn">Learn</a></li>					

				<?php endif; ?>
				
				
			</ul>
			
			<div class="jquery-popup" id="header-signup" title="Create your Account">
				
				<div class="login-error"></div>
				
				<div class="form-container clearfix" >
				
				<div class="saveinfo"  > <p id = "signup-header-response" style="display:none;"><a style="color:#fff;" id="signup-response"></a><a class="remove" onclick="removeHeaderAlert()"></a></p> </div>
				  
				  <form id="form-user-signup" class="form-user-login vform">
					<h3>Signup up with email <a id="close_signup" class="close-popup" onclick="" >X</a> </h3>
					
					<label for="username" class="sr-only">Username</label>
					<input name="username" id="username" class="required username" placeholder="User name" type="text" maxlength="55">
					<label for="email" class="sr-only">E-Mail</label>
					<input name="email" id="email" class="required email" placeholder="Email Address" type="email" maxlength="255">
					<label for="password" class="sr-only">Password</label>
					<input title="Use at least 6 characters. Strength is based on unique characters. Avoid repetition and mix password (special characters, uppercase and numbers)." name="password" id="password" class="required" value="" placeholder="Password" type="password">
					
					<div style="">
						<div class="color" style="display:inline-block;" >
							<label style="float:left;">Strength:</label>
							<div class="figure" id="strength_human" style="float:right;margin-left:5px;"></div>    
						</div>
						
						<div class="password-strength-meter" id="strength_score"></div>
					</div>

					<label for="confirmpwd" class="sr-only">Confirm Password</label>
					<input name="confirmpwd" id="confirmpwd" class="required" value="" placeholder="Password" type="password">

					<!-- in the global script is where the listener to this submit button is-->
					<!-- <input id="login" name="login" value="Log In" type="submit"> -->
					<input id="signup" type="button" value="Signup" onclick="return regformhash(this.form,
																			   this.form.username,
																			   this.form.email,
																			   this.form.password,
																			   this.form.confirmpwd);">
				  </form><!-- end form-user-login -->
				  
				</div><!-- end .form-container -->
			</div><!-- end header-login -->
			
			<div class="jquery-popup" id="header-login" title="Login To Your Account">
				
				
				<div class="form-container clearfix">
				  <!-- <div class="form-user-social">
					<h3>Log in via Social</h3> --> <!-- just words on top of the division -->
					
					<!-- <button class="button-social button-linkedin js-linkedin-connect">Log in with LinkedIn 
						<img src="Crowdfunder%20_%20Investment%20and%20Equity%20Crowdfunding%20Platform_files/icon-button-linkedin.png" alt="">
					</button>
					
					<button class="button-social button-facebook fbRegister">Log in with Facebook
						<img src="Crowdfunder%20_%20Investment%20and%20Equity%20Crowdfunding%20Platform_files/icon-button-facebook.png" alt="">
					</button>
					
					<form action="/user/signup" method="post" id="form">
					  <span style="display: none"> --> <!-- a span is usually a single line where as a div is a block od whole widgets -->
				  <!--       <input id="facebookId" name="FacebookId" type="text">
						<input id="token" name="FacebookToken" type="text">
					  </span>
					</form> --><!-- end form -->
					
				 <!--  </div> --><!-- end form-user-social -->
				  
				  <div class="saveinfo" > <p id = "login-header-response" style="display:none;"><a style="color:#fff;" id="login-response"></a><a class="remove" onclick="removeHeaderAlert()"></a></p> </div>
				  
				  <form id="form-user-login" class="form-user-login vform">
					<h3>Log in with email<a id="close_login" class="close-popup" onclick="" >X</a></h3>
					<!-- <label for="username" class="sr-only">Username</label>
					<input name="username" id="username" class="required username" placeholder="User name" type="text">
					-->
					<label for="email" class="sr-only">E-Mail</label>
					<input name="email" id="email" class="required email" placeholder="Email Address" type="email">
					<label for="password" class="sr-only">Password</label>
					<input name="password" id="password" class="required" value="" placeholder="Password" type="password">
					<a href="http://www.Afroglobalinvestors.com/?page_id=106" class="forgot-password" >Forgot your password?</a>
					<!-- in the global script is where the listener to this submit button is-->
					<!-- <input id="login" name="login" value="Log In" type="submit"> -->
					<input id="login" type="button" value="Login" onclick="return formhash(this.form, this.form.password,this.form.email);">
				  </form><!-- end form-user-login -->
				  
				</div><!-- end .form-container -->
				<p class="login-newuser"><a href="http://www.Afroglobalinvestors.com/afroglobal_signin.html">Create an account</a></p>
			</div><!-- end header-login -->
			
		</nav><!-- #site-navigation -->
	
		</div>
		
			<div class="social-icons" style="margin-top: 0px;">
	
			</div>

	<script>
		$(document).ready(function() {
			
			$("#password").on("keypress keyup keydown", function() {
				var pass = $(this).val();
				$("#strength_human").text(checkPassStrength(pass));
				//$("#strength_score").text(scorePassword(pass));
				
				var score = parseInt(scorePassword(pass));
				var rgb1 = (100 - score) / 100 * 255;
				var rgb2 = score /100 * 255;
				/*  calc( (100 - attr(data-val)) / 100 * 0 ),
				calc( attr(data-val) / 100 * 0), */
				
				document.getElementById('strength_score').style.backgroundColor = "rgb("+rgb1+","+rgb2+",0)";
			});
			
		});
		
		
	</script>
	
	
</header><!-- #masthead -->
	
