<?php /* Template Name: UserArticles */ ?>
<?php get_header(); ?>
<div class="wrapper clearfix" id="wrapper">
	<nav class="main-nav" style="width:100%;">
		<ul class="nav">
		
			<li class="" ><a href="" id="user-profile"></a></li>
			<li class="" ><a href="" id="user-projects">Projects</a></li>
			<li class="active" ><a href="" id="user-articles">Articles</a></li>
			
		</ul>
	
	</nav>
	
	<aside class="sidebar projects-sb-left left" id="sidebar-left">
		<h4 style="margin-bottom:10px;" ><i>Latest African Markets </i></h4>
		
		<!-- start feedwind code -->
		<script type="text/javascript">document.write('\x3Cscript type="text/javascript" src="' + ('https:' == document.location.protocol ? 'https://' : 'http://') + 'feed.mikle.com/js/rssmikle.js">\x3C/script>');
		</script>
		
		<script type="text/javascript">(function() {var params = {rssmikle_url: "http://feeds.feedburner.com/africanmarkets/business",rssmikle_frame_width: "300",rssmikle_frame_height: "800",frame_height_by_article: "0",rssmikle_target: "_blank",rssmikle_font: "Arial, Helvetica, sans-serif",rssmikle_font_size: "14",rssmikle_border: "off",responsive: "off",rssmikle_css_url: "",text_align: "left",text_align2: "left",corner: "off",scrollbar: "on",autoscroll: "on",scrolldirection: "up",scrollstep: "3",mcspeed: "20",sort: "Off",rssmikle_title: "on",rssmikle_title_sentence: "",rssmikle_title_link: "",rssmikle_title_bgcolor: "#800000",rssmikle_title_color: "#FFFFFF",rssmikle_title_bgimage: "",rssmikle_item_bgcolor: "#FFFFFF",rssmikle_item_bgimage: "",rssmikle_item_title_length: "55",rssmikle_item_title_color: "#E66432",rssmikle_item_border_bottom: "on",rssmikle_item_description: "on",item_link: "off",rssmikle_item_description_length: "150",rssmikle_item_description_color: "#666666",rssmikle_item_date: "gl1",rssmikle_timezone: "Etc/GMT",datetime_format: "%b %e, %Y %l:%M:%S %p",item_description_style: "text+tn",item_thumbnail: "full",item_thumbnail_selection: "auto",article_num: "15",rssmikle_item_podcast: "off",keyword_inc: "",keyword_exc: ""};feedwind_show_widget_iframe(params);})();
		</script>
				
		<div style="font-size:10px; text-align:center; width:300px;">
		<a href="http://feed.mikle.com/" target="_blank" style="color:#CCCCCC;">RSS Feed Widget</a><!--Please display the above link in your web page according to Terms of Service.-->
		</div><!-- end feedwind code -->
		
	</aside>
			
	<section class="articles-container" id="articles-container">
	
			<div class="box alert" id="noarticles">
				<p id="no-articles" >No articles</p>
			</div>
			
		
		<script type="text/javascript">
			var homeurl = "<?php echo home_url() ; ?>";
			
			var username = "<?php echo $_GET['username']; ?>" ;

			
			var parsed;
			var images = new Array();
			var countarticles = 0;
			
			metadata();
			
			seturls();
			
			function metadata(){
				document.getElementById("seo-title").innerHTML = username +" - Articles | Afroglobalinvestors";
				
				var metadesc = document.createElement("meta");
				metadesc.setAttribute("content","View all articles created by "+username);
				metadesc.setAttribute("name","description");
				metadesc.setAttribute("itemprop","description");
				
				document.getElementsByTagName("head")[0].appendChild(metadesc);
			}
				
			function seturls(){
				document.getElementById("user-profile").innerHTML = "About "+username;
				document.getElementById("user-profile").href = "http://www.Afroglobalinvestors.com/?page_id=54&username="+username;
				document.getElementById("user-projects").href = "http://www.Afroglobalinvestors.com/?page_id=97&username="+username;
				document.getElementById("user-articles").href = "http://www.Afroglobalinvestors.com/?page_id=99&username="+username;
			
			}
		
			//take the articles the user has created
			$.ajax({
				type: "POST",
				url: 'backend/getMyArticles.php',
				data: {username: username,approved:'yes'},

				success: function (obj, textstatus) { 
					  parsed = jQuery.parseJSON(obj);//JSON.parse(obj);
					  
					  fillarticles();
					  
				},
				
				error: function(xhr, textStatus, error){
					  alert("the err "+error);
				}
			});
			
			
			 function fillarticles(){
				if (parsed['articles'].length > 0)
				{document.getElementById("no-articles").innerHTML = ""; }
				 loop:
				for (i=0; i<parsed['articles'].length; i++)
				{	
					getImage(parsed['articles'][i]['articlename'],parsed['articles'][i]['company'],i);
					
				}
			}
		  
			
			function getImage(articlename,company,index)
			{
				var dir = "newsarticles/"+articlename+"/images/mainimage";
				var fileextension = [".jpg",".png"];
				var count = 0;
				
				$.ajax({
					url: dir,
					success: function getimages(data) {
						//Select all <a> elements with a href attribute that ends with ".org":
						//$("a[href$='.org']")
						
						$(data).find("a[href$='"+ (fileextension[1]) + "'], a[href$='"+ (fileextension[0]) + "']").each(function () {
						//$(data).find("a:contains(" + (fileextension[1]) + "), a:contains(" + (fileextension[0]) + ")").each(function () {
							var filename = this.href.substring (this.href.lastIndexOf ( '/' ) + 1 );

							var filename = dir + "/" + filename;

							images[count] = new Image();
							images[count].src = filename;
							count++;
						});
	 
						finished(index,articlename,company);
					},
					error: function showPorject(data) {
						finished(index,articlename);
					}
					
				});
				
				function finished(index,articlename,company){
					
					//********** make the height of the container match the projects *******
					var articlenum = parsed['articles'].length - 1;
					var img = new Image();
					img.onload = function() {

					if (countarticles == (articlenum)) {
						document.getElementById('wrapper').style.minHeight = document.getElementById('articles-container').clientHeight + 200+ 'px';
						}
						countarticles++;
					}
					img.src = images[0].src;
					
					//**************************************
					
										
					//create the a element to put the thingsy in 
					var a = document.createElement("a");
					a.setAttribute("class","blog-list-project wow fadeInDown");
					a.setAttribute("data-wow-delay","0.5s");
					a.setAttribute("style","-webkit-animation-delay: 0.5s; -webkit-animation-name: none; padding-top: 1cm");
					a.setAttribute("href","http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename);
					
					//create the img div
					var imgDiv = document.createElement("div");
					imgDiv.setAttribute("class","blog-image");
					
					if (images[0])
						imgDiv.appendChild(images[0]);
					
					//create the text div
					var txtDiv = document.createElement("div");
					txtDiv.setAttribute("class","blog-excerpt");
					 
					txtDiv.innerHTML = "<h3> "+parsed['articles'][index]['articlename']+"</h3> <h4 class=\"posted-date\"><i class=\"fa fa-calendar\"></i>"+parsed['articles'][index]['submitteddate']+"</h4> "+parsed['articles'][index]['articletag']+ "<br><a class=\"blogoption\" href=\"http://www.Afroglobalinvestors.com/?page_id=78&article="+articlename+"\">Read More&nbsp;&nbsp;<i class=\"fa fa-angle-right\"></i> </a>";
					
					//then put everything together
					a.appendChild(imgDiv);
					a.appendChild(txtDiv);
					
					document.getElementById("articles-container").appendChild(a);
					
					//reset images
					images = new Array();
					
					if (index == parsed['articles'].length -1) {
						//alert('so');
						getProjects("no","yes","no","no","no","no","backend/getprojects.php",homeurl);
					} else{
					}
				}
			}
			
			function editarticle(articlename){
				
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"article",name:articlename},
					async: false,
					success: function (obj, textstatus) {
						//alert(obj);
						location.href='http://www.Afroglobalinvestors.com/?page_id=73';
					
					},
					
				});
		
			}
			
			function createarticle(){
				//first unset all article variables that are lingering about
				<?php try { unset($_SESSION['article']); }catch(Exception $ex){} ?>
				<?php try { unset($_SESSION['company']); }catch(Exception $ex){} ?>
				
				location.href='http://www.Afroglobalinvestors.com/?page_id=73'
			}
			
			function createcompany(newcompany){
				alert('in co');
				if (newcompany =='new') {
				//first unset all article variables that are lingering about
				<?php try { unset($_SESSION['article']); }catch(Exception $ex){} ?>
				//then set a variable for company
				}
				alert("sending variable");
				$.ajax({
					type: 'POST',
					url: "./backend/setVariable.php",
					data: {variable:"company",name:"yes"},
					async: false,
					success: function (obj, textstatus) {
						alert(obj);

						location.href='http://www.Afroglobalinvestors.com/?page_id=40';
					
					},
					
				});
				
			}
			

			</script>
		
		<?php if (category_description()) { ?>
			<section class="cat-desc">
				<?php echo category_description(); ?>
			</section>
		<?php } ?>
		
	</section>
	
	<aside class="sidebar projects-sb-right right" id="sidebar-right">
		
		<h4 style="margin-left:20px"><i>Featured Companies </i></h4>
		<div id="sidebar-right-companies"> </div>
				  
	</aside>
	
	
	
	
</div>
<?php get_footer(); ?>