=== Facebook By Weblizar ===
Contributors: weblizar
Donate link: http://www.weblizar.com/
Tags: facebook, facebook like, facebook widget, facebook page, facebook share, facebook share button, Like, like button, Share, share button, facebook fanbox, facebook page, page, plugin, posts, publish Facebook, sidebar, social, Social Plugins, facebook embed, facebook events, Facebook feed, Like Button Widget,
Requires at least: 3.0
Tested up to: 3.9.1
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Facebook Widget For WordPress
== Description ==

Facebook by [Weblizar](http://www.weblizar.com/) plugin is very simple and easy facebook widget plugin to display your facebook page live stream & friends on WordPress blog.

Using **Facebook By Weblizar** facebook widget publish your facbook page post & show your facebook page fans.


== Features ==
* Facebook Widget
* Facebook Widget Settings
* Facebook Page Like Button
* Facebook Page Live Stream
* Facebook Page Fan Faces
* Facebook Page Border Option
* Facebook Page Custom Height & Width Option

= Docs & Support =

You can find [docs](http://weblizar.com/facebook-plugin-by-weblizar/), [FAQ](http://www.weblizar.com/faq/) and more detailed information about Testimonial on [Weblizar](http://www.weblizar.com). If you were unable to find the answer to your question on the FAQ or in any of the documentation, you should check the [support forum](http://weblizar.com/facebook-plugin-by-weblizar/) on WordPress.org.

= We Need Your Support =

It is really hard to continue development and support for this free plugin without contributions from users like you. If you are enjoying using our Testimonial plugin and find it useful, then please consider [__Making a Donation__](http://www.weblizar.com). Your donation will help us to encourage and support the plugin's continued development and better user support.


= Translators =

Please contribute to translate our plugin.  Contact at `lizarweb (at) gmail (dot) com`.

== Installation ==

1. Upload the entire `facebook-by-weblizar` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Go to Widgets in Appearance Menu and activate widget.

== Frequently Asked Questions ==

Please use WordPress support forum to ask any query regarding any issue.

== Screenshots ==

1. Facebook Widget Preview
2. Facebook Widget Settings
3. Facebook Widget Preview On Site

== Changelog ==

For more information, see Weblizar(http://wwww.weblizar.com/).

= 0.1 =

* Feature - Facebook Widget 
* Feature - Facebook Widget Settings Panel
