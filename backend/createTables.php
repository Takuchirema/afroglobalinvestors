<?php 

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

$query = "CREATE TABLE IF NOT EXISTS `articlecomments` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
        `articlename` VARCHAR(55) NOT NULL, 
        `comment` VARCHAR(1000) NOT NULL, 
        `postdate` DATE NOT NULL, 
		`postername` VARCHAR(55) NOT NULL,
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `articleimages` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
        `article` VARCHAR(55) NOT NULL, 
        `name` VARCHAR(55) NOT NULL, 
        `tag` VARCHAR(255) NOT NULL, 
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `login_attempts` ( 
	`user_id` INT(11) NOT NULL, 
	`time` VARCHAR(30) NOT NULL 
	)";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `members` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
        `addr1` VARCHAR(60) NOT NULL, 
		`addr2` VARCHAR(60) NOT NULL, 
		`city` VARCHAR(35) NOT NULL, 
		`country` VARCHAR(40) NOT NULL, 
        `email` VARCHAR(255) NOT NULL, 
		`facebook` VARCHAR(50) NOT NULL, 
		`imagetag` VARCHAR(255) NOT NULL, 
        `password` CHAR(128) NOT NULL, 
		`postalcode` INT(11) NOT NULL, 
		`profile` TEXT(4000) NOT NULL, 
        `region` VARCHAR(55) NOT NULL, 
		`salt` TEXT NOT NULL, 
		`twitter` VARCHAR(50) NOT NULL, 
		`username` VARCHAR(55) NOT NULL, 
        `website` VARCHAR(255) NOT NULL, 
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `newsarticles` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
        `approved` VARCHAR(5) NOT NULL, 
		`article` TEXT(7000) NOT NULL, 
		`articlecategory` VARCHAR(20) NOT NULL, 
		`articlename` VARCHAR(55) NOT NULL, 
        `articletag` VARCHAR(255) NOT NULL, 
		`author` VARCHAR(55) NOT NULL, 
		`submitted` VARCHAR(5) NOT NULL, 
        `submitteddate` DATE NOT NULL, 
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `projectcomments` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
        `projectname` VARCHAR(55) NOT NULL, 
        `comment` VARCHAR(1000) NOT NULL, 
        `postdate` DATE NOT NULL, 
		`postername` VARCHAR(55) NOT NULL,
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `projectimages` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
        `project` VARCHAR(55) NOT NULL, 
        `name` VARCHAR(55) NOT NULL, 
        `tag` VARCHAR(255) NOT NULL, 
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `projects` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
		`projectabout` TEXT(7000) NOT NULL, 
		`projectcategory` VARCHAR(20) NOT NULL, 
		`projectname` VARCHAR(55) NOT NULL, 
        `projecttag` VARCHAR(255) NOT NULL, 
		`projectleader` VARCHAR(55) NOT NULL, 
		`projectregion` VARCHAR(55) NOT NULL, 
		`projectcountry` VARCHAR(40) NOT NULL, 
		`company` VARCHAR(5) NOT NULL, 
		`date` DATE NOT NULL, 
        `debt` INT(11) NOT NULL,
		`debtinterest` INT(3) NOT NULL, 
		`donations` INT(11) NOT NULL, 
        `equity` INT(11) NOT NULL,
		`equityvalue` INT(11) NOT NULL, 
		`fundingdetails` TEXT(4000) NOT NULL, 
        `fundinggoal` INT(11) NOT NULL,
		`link1` VARCHAR(55) NOT NULL, 
		`link2` VARCHAR(55) NOT NULL, 
        `link3` VARCHAR(55) NOT NULL,
		`submitted` VARCHAR(5) NOT NULL, 
        `submitteddate` DATE NOT NULL,
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

$query = "CREATE TABLE IF NOT EXISTS `projectupdates` (
        `id` INT(10) NOT NULL AUTO_INCREMENT, 
        `projectname` VARCHAR(55) NOT NULL, 
        `updatevalue` VARCHAR(1000) NOT NULL, 
        `date` DATE NOT NULL, 
		`projectleader` VARCHAR(55) NOT NULL,
        PRIMARY KEY (`id`)
    )";

//execute query
try {
	$stmt   = $db->prepare($query);
	$result = $stmt->execute();
}
catch (PDOException $ex) {
	// For testing, you could use a die and message. 
	//die("Failed to run query: " . $ex->getMessage());
	
	//or just use this use this one:
	$response["success"] = 0;
	$response["message"] = "Something went wrong in creating the table ".$ex;
	die(json_encode($response));
}

//http://stackoverflow.com/questions/20958/list-of-standard-lengths-for-database-fields
//https://blog.bufferapp.com/the-ideal-length-of-everything-online-according-to-science
$response["success"] = 1;
$response["message"] = "Tables Created!!";
echo json_encode($response);

?>