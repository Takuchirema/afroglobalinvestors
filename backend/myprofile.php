<?php
/*

*/
$_SESSION["profile-saved"] = "";

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

require_once './htmlpurifier/library/HTMLPurifier.auto.php';

$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);

$error_msg = "";
$username = $_POST['name'];
$post = False;


$response["success"] = 1;
$response["message"] = "changes successfully saved";

if ($_POST['about'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
	$_POST['about'] = $purifier->purify($_POST['about']);
	
	//echo $_POST['about'];
	
	$query = "UPDATE members SET profile = :profile WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':profile' => $_POST['about'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
		
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting profile about";
        die(json_encode($response));
    }
	
} 


if ($_FILES["fileToUpload"]["name"]) {
	$post = True;
	//echo 'in pic upload';
	$target_dir = "../members/";
	$target_file = $target_dir .$username.'/profile/'. basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	
	// Check if file already exists
	if (file_exists($target_file)) {
		//echo "Sorry, file already exists.";
		$uploadOk = 0;
		
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 1000000) { //research more on the file size
		//echo "Sorry, your file is too large.";
		$uploadOk = 0;
		$response["success"] = 0;
        $response["message"] = "Sorry, your profile picture file is too large. Max is 100kb";
        die(json_encode($response));
	}
	
	$filetypes = array("jpg", "JPG", "png", "PNG","gif", "GIF", "jpeg", "JPEG");
	// Allow certain file formats
	//if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	//&& $imageFileType != "gif" ) {
	if (!in_array($imageFileType, $filetypes)) {
		//echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
		//or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed for profile picture";
        die(json_encode($response));
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		//echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		try{ //delete all the files currently inside we only allow one picture per profile 
			//***************done when sure file will be uploaded*************************
			$fileToDelete = glob($target_dir .$username.'/profile/*');
			
			foreach($fileToDelete as $file){ 
			if(is_file($file))
				unlink($file); 
		}
		
		}catch(Exception $e)
		{}
		
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} else {
			//echo "Sorry, there was an error uploading your file.";
		}
	}
	
}else{
	//echo 'pic not set';
}

if ($_POST['myemail'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
	$_POST['myemail'] = filter_input(INPUT_POST, 'myemail', FILTER_SANITIZE_EMAIL);
	
	$query = "UPDATE members SET email = :email WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':email' => $_POST['myemail'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting new email";
        die(json_encode($response));
    }
	
}

if (isset($_POST['p'])) {

	$post = True;
	
	$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    // Create salted password 
	//$password = crypt($password,"$6$".$random_salt);
    $password = hash('sha512', $_POST['p']. $random_salt);
	
	$query = "UPDATE members SET password = :password,salt=:salt WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':password' => $password,
		':username' => $username, 			//in the mean time. but we will use session username later
        ':salt' => $random_salt
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured putting new password";
        die(json_encode($response));
    }
	
}

if ($_POST['facebook'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['facebook'] = filter_input(INPUT_POST, 'facebook', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET facebook = :facebook WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':facebook' => $_POST['facebook'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting facebook";
        die(json_encode($response));
    }
	
}

if ($_POST['twitter'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['twitter'] = filter_input(INPUT_POST, 'twitter', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET twitter = :twitter WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':twitter' => $_POST['twitter'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting twitter";
        die(json_encode($response));
    }
	
}

if ($_POST['website'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['website'] = filter_input(INPUT_POST, 'website', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET website = :website WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':website' => $_POST['website'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting website url";
        die(json_encode($response));
    }
	
}

if ($_POST['addr1'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['addr1'] = filter_input(INPUT_POST, 'addr1', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET addr1 = :addr1 WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':addr1' => $_POST['addr1'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting addr1";
        die(json_encode($response));
    }
	
}

if ($_POST['addr2'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['addr2'] = filter_input(INPUT_POST, 'addr2', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET addr2 = :addr2 WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':addr2' => $_POST['addr2'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting add2";
        die(json_encode($response));
    }
	
}

if ($_POST['city'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['city'] = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET city = :city WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':city' => $_POST['city'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting city";
        die(json_encode($response));
    }
	
}

if ($_POST['postalcode'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
	$_POST['postalcode'] = filter_input(INPUT_POST, 'postalcode', FILTER_SANITIZE_NUMBER_INT);
	
	$query = "UPDATE members SET postalcode = :postalcode WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':postalcode' => $_POST['postalcode'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting postal code";
        die(json_encode($response));
    }
	
}

if (isset($_POST['country'])) {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['country'] = filter_input(INPUT_POST, 'country', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET country = :country WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':country' => $_POST['country'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting country";//.$ex;
        die(json_encode($response));
    }
	
}

if ($_POST['region'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['region'] = filter_input(INPUT_POST, 'region', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET region = :region WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':region' => $_POST['region'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting region";
        die(json_encode($response));
    }
	
}



if ($_POST['name'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['name'] = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET username = :newuser WHERE username = :olduser"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':newuser' => $_POST['name'],
		':olduser' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting new username";
        die(json_encode($response));
    }
	
}

if ($_POST['picture'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['picture'] = filter_input(INPUT_POST, 'picture', FILTER_SANITIZE_STRING);
	
	$query = "UPDATE members SET imagetag = :imagetag WHERE username = :username"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
		':imagetag' => $_POST['picture'],
		':username' => $username  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "An error occured posting 'about image' ";
        die(json_encode($response));
    }
	
}

if ($post){
	// echoing JSON response
	echo json_encode($response);
}



?>