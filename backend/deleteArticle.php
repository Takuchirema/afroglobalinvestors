<?php

/*
*/

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

$response = array();

if ($_POST['article'] != '') {
	
	
	$query = "DELETE FROM newsarticles WHERE articlename= :articlename";

    //Update query
    $query_params = array(
        ':articlename' => $_POST['article']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occurred while trying to delete the article";
        die(json_encode($response));
    }
	
	$query = "DELETE FROM articleimages WHERE article= :articlename";

    //Update query
    $query_params = array(
        ':articlename' => $_POST['article']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occurred while trying to delete the article images";
        die(json_encode($response));
    }
	
	//then the entire folder for the article
	try{
		$dirPath = "../newsarticles/".$_POST['article'];
		
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
				$path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
		}
		rmdir($dirPath);
		
	}catch(Exception $e)
	{
		//echo '********************error in deleting '.$e;
		$response["success"] = 0;
        $response["message"] = "Sorry an error occurred while trying to delete the article images".$e;
        die(json_encode($response));
	}
	
	$response["success"] = 1;
	$response["message"] = "article has been successfully deleted";
	echo json_encode($response);
	
}

?>