<?php

/*

*/

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

if ($_POST['comment'] != '') {

	// Sanitize and validate the data passed in
    $_POST['comment'] = filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_STRING);
		
	$query = "INSERT INTO articlecomments (articlename,postername,postdate,comment) VALUES ( :articlename,:postername,:postdate,:comment) ";

    //Update query
    $query_params = array(
        ':articlename' => $_POST['articlename'],
		':postername' => $_POST['postername'],
		':postdate' => $_POST['postdate'],
		':comment' => $_POST['comment']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occurred while submitting your comment";
        die(json_encode($response));
    }
	
	 $response["success"] = 1;
	 $response["message"] = "Your comment has been submitted";
	 echo json_encode($response);
	
}

?>