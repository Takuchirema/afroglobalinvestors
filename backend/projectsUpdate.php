<?php
//The script is for checking for expired projects and setting them to expired

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore, let's require the connection to happen:
*/
require("./includes/config.inc.php");

$query ="";

$response = array();


	//order by date submitted 
	$query = "Select * FROM projects WHERE submitted = 'yes' ORDER BY date ASC";
	
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error!";
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();

	$currentdate = strtotime(date('Y-m-d'));
	
	if ($rows) {
		
		//echo "rows in";
		foreach ($rows as $row) {
			$enddate = strtotime($row["date"]);
			if ($currentdate > $enddate){
				
				//order by date submitted 
				$query = "UPDATE projects SET submitted = 'expired' WHERE projectname = :projectname";
				
				$query_params = array(
					':projectname' => $row['projectname']					
				);
				
				//execute query
				try {
					$stmt   = $db->prepare($query);
					$result = $stmt->execute($query_params);
					
				}
				catch (PDOException $ex) {
					$response["success"] = 0;
					$response["message"] = "Database Error!";
					die(json_encode($response));
				}
				
			}else {
				break;
			}
		}
		
		$response["success"] = 1;
		$response["message"] = "Projects updated";
		echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Projects Available";
		die(json_encode($response));
	}
	

?>