<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

if ($_POST['projectname'] != ''){
	//initial query
	$query = "Select * FROM projects WHERE projectname=:projectname";

	//Update query
    $query_params = array(
		':projectname' => $_POST['projectname'] 		
        
    );
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error! ".$ex;
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["project"]   = array();
		
		foreach ($rows as $row) {
			$project = array();
			$project["projecttag"][] = $row["projecttag"];
			$project["projectname"][] = $row["projectname"];
			$project["date"][] = $row["date"];
			$project["projectleader"][] = $row["projectleader"];
			$project["projectcategory"][] = $row["projectcategory"];
			$project["projectcountry"][] = $row["projectcountry"];
			$project["projectregion"][] = $row["projectregion"];
			$project["fundinggoal"][] = $row["fundinggoal"];
			$project["fundingdetails"][] = $row["fundingdetails"];
			$project["equity"][] = $row["equity"];
			$project["equityvalue"][] = $row["equityvalue"];
			$project["debt"][] = $row["debt"];
			$project["debtinterest"][] = $row["debtinterest"];
			$project["projectabout"][] = $row["projectabout"];
			$project["submitted"][] = $row["submitted"];
			$project["donations"][] = $row["donations"];
			$project["link1"][] = $row["link1"];
			$project["link2"][] = $row["link2"];
			$project["link3"][] = $row["link3"];
			$project["company"][] = $row["company"];
			
			
			array_push($response["project"], $project);
			
		}
		
		// echoing JSON response
		echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}

}

?>
