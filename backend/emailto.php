<?php 
$response = array();

$myemail = 'support@Afroglobalinvestors.com';//<-----Put Your email address here.
if(empty($_POST['name'])  ||
   empty($_POST['email']) ||
   empty($_POST['message']))
{
	$response['success'] = 0;
    $response['message'] = "Please enter all fields";
	die(json_encode($response));
}

$name = $_POST['name'];

$email_address = $_POST['email'];

$message = $_POST['message']."

Reply-To: $email_address
";

if (!preg_match(
"/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i",
$email_address))
{
	$response['success'] = 0;
    $response['message'] = "Please enter a valid email address";
	die(json_encode($response));
}

$to = $myemail;

$email_subject = "Contact form submission: $name";

$email_body = "You have received a new message. ".
" Here are the details: Message \n\n $message";

$headers = "From: Afroglobalinvestors ";

/* $email_subject = "Contact form submission: ".$name;
$email_body = "You have received a new message. ".
" Here are the details:
Name: ".$name."
Email: ".$email_address."
Message: ".$message;

$headers = "From: ".$myemail."
			Reply-To: ".$email_address;
 */			
$mail = mail($to,$email_subject,$email_body,$headers);
//redirect to the 'thank you' page

if($mail){
	$response['success'] = 1;
	$response['message'] = "Thank you for your email! We will get back to you shortly";
	echo json_encode($response);
}else{
	$response['success'] = 0;
	$response['message'] = "Sorry your email was not sent due to a network error. Please try again later";
	echo json_encode($response);
}


?>