<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

if ($_POST['username'] != ''){
	//initial query
	if (isset($_POST['approved']) && $_POST['approved'] == "no"){
		$query = "Select * FROM newsarticles WHERE author=:username";
	}else if (isset($_POST['approved']) && $_POST['approved'] == "yes"){
		$query = "Select * FROM newsarticles WHERE author=:username AND approved='yes'";
	}else{
		$query = "Select * FROM newsarticles WHERE author=:username";
	}

	//Update query
    $query_params = array(
		':username' => $_POST['username'] 			//in the mean time. but we will use session username later
        
    );
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error! ".$ex;
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["articles"]   = array();
		
		foreach ($rows as $row) {
			$article = array();
			$article["articletag"][] = $row["articletag"];
			$article["articlename"][] = $row["articlename"];
			$article["submitteddate"][] = $row["submitteddate"];
			
			array_push($response["articles"], $article);
			
		}
		
		// echoing JSON response
		echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}

}

?>
