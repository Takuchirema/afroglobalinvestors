<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

if (!empty($_POST))
{
	
	//initial query
	$query = "Select * FROM projectcomments WHERE projectname=:projectname";
	
	$query_params = array(
        ':projectname' => $_POST['projectname']
    );

	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "database error plus: ".$ex->getMessage();
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();
	
	
	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["comments"]   = array();
		
		foreach ($rows as $row) {
			$comment             = array();
			$comment["comment"]  = $row["comment"];
			$comment["postdate"]  = $row["postdate"];
			$comment["postername"]  = $row["postername"];
			
			array_push($response["comments"], $comment);
		}
		
		// echoing JSON response
		echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}

}

?>
