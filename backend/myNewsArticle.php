<?php

/* Must be at the top of the page otherwise all session variables are unset */

include_once './includes/functions.php';

sec_session_start();

require_once './htmlpurifier/library/HTMLPurifier.auto.php';

$config = HTMLPurifier_Config::createDefault();
$purifier = new HTMLPurifier($config);


/* ************************************************************************ */

//dont want to report any errors or warnings they will be caught by try statement
error_reporting(0);

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

$error_msg = "";
$username = $_SESSION['username'];
$title = "";

$post = False;

$response["success"] = 1;
$response["message"] = "changes successfully saved";

if (isset($_POST['edit'])){
	$post = True;
	$title = $_POST['edit'];
}
elseif ($_POST['title'] != '') {
	
	// Sanitize and validate the data passed in
    $_POST['title'] = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
	
	$post = True;
	$title = $_POST['title'];
	
	
	$query = "INSERT INTO newsarticles (articlename,submitted,approved) VALUES ( :title,'no','no') ";
	
    //Update query
    $query_params = array(
        ':title' => $_POST['title']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
		
		$query = "UPDATE newsarticles SET author = :author WHERE articlename = :title";

		//Update query
		$query_params = array(
			':author' => $username,
			':title' => $title  			//in the mean time. but we will use session username later
			
		);
	  
		//execute query
		try {
			$stmt   = $db->prepare($query);
			$result = $stmt->execute($query_params);
			
			//the article has registered successfully and so we add a file for it
			if (!file_exists('../newsarticles/'.$title.'/images/mainimage')) {
				mkdir('../newsarticles/'.$title.'/images/mainimage', 0777, true);
				
				//put an index file in there too. Copy from left to right folder
				copy('../newsarticles/index.php', '../newsarticles/'.$title.'/index.php');
			}
		}
		catch (PDOException $ex) {
			// For testing, you could use a die and message. 
			//die("Failed to run query: " . $ex->getMessage());
			
			//or just use this use this one:
			$response["success"] = 0;
			$response["message"] = "An error occured while creating your article".$ex;
			die(json_encode($response));
		}

    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
		$response["message"] = "An error occured while creating your article".$ex;
        die(json_encode($response));
    }
	
} 

$Files = array_filter($_FILES["fileToUpload"]["name"]);

if (!empty($Files)) {

//echo '<pre>'; print_r($_FILES["fileToUpload"]); echo '</pre>';
//echo 'files there';

$post = True;
//echo $_FILES["fileToUpload"]["name"];
//$files= $_FILES['fileToUpload']['name'][1];
//echo "the files: ".$files;

for ($i = 0; $i < count($_FILES['fileToUpload']['name']); $i++) {
	//echo 'in pic upload';
	$target_dir = "../newsarticles/";
	$main_file = $target_dir .$title.'/images/mainimage/'. basename($_FILES["fileToUpload"]["name"][$i]);
	
	//check if its main image
	if ($_POST['main-image'] == $_FILES["fileToUpload"]["name"][$i]){
		//echo "imgs ".$_POST['main-image']." ".$_FILES["fileToUpload"]["name"][$i];
		//echo 'main image '.$_POST['main-image'];
		$target_file = $target_dir .$title.'/images/mainimage/'. basename($_FILES["fileToUpload"]["name"][$i]);
		
	} else {
		$target_file = $target_dir .$title.'/images/'. basename($_FILES["fileToUpload"]["name"][$i]);
	}

	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	
	
	// Check if file already exists
	if (file_exists($target_file) || file_exists($main_file)) {
		//echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"][$i] > 1000000) { //research more on the file size
		//echo "Sorry, your file is too large.";
		$uploadOk = 0;
		$response["success"] = 0;
		$response["message"] = "Sorry, one of your picture file sizes is too large. Max is 100kb";
        die(json_encode($response));

	}
	$filetypes = array("jpg", "JPG", "png", "PNG","gif", "GIF", "jpeg", "JPEG");
	// Allow certain file formats
	//if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	//&& $imageFileType != "gif" ) {
	if (!in_array($imageFileType, $filetypes)) {
		//echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
		
		$response["success"] = 0;
		$response["message"] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed for pictures";
        die(json_encode($response));

	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		//echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	
		//delete the current main image
		if ($_POST['main-image'] == $_FILES["fileToUpload"]["name"][$i]){
			//echo "again imgs ".$_POST['main-image']." ".$_FILES["fileToUpload"]["name"][$i];
			//then delete the previous mainimage
			try{
				$fileToDelete = glob($target_dir .$title.'/images/mainimage/*');
				
				foreach($fileToDelete as $file){ 
				if(is_file($file))
					unlink($file); 
				}
				
			}catch(Exception $e)
			{
				//echo '********************error in deleting '.$e;
			}
		}
		
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"][$i], $target_file)) {
			//echo "The file ". basename( $_FILES["fileToUpload"]["name"][$i]). " has been uploaded.";
			
			try{
				//then upload the information of the picture
				picInfo($_FILES["fileToUpload"]["name"][$i],$title,$db);
			}catch (Exception $ex)
			{
				//echo "picInfo ".$ex;
			}
			
		} else {
			//echo "Sorry, there was an error uploading your file.";
			$response["success"] = 0;
			$response["message"] = "Sorry, there was an error uploading your file. ";
			die(json_encode($response));

		}
	}
	
}
	
}else{
	//echo 'pic not set';
}

if ($_POST['main-image']){ //if there is a main-image exchange them places
	
	$target_dir = "../newsarticles/";
	
	$source = $target_dir .$title.'/images/mainimage/';
	// Get array of all source files
	$files = scandir($source);
	
	$destination = $target_dir .$title.'/images/';
	
	if(!file_exists($source.$_POST['main-image']) && file_exists($destination.$_POST['main-image'])){		
		// Cycle through all source files
		foreach ($files as $file) {
		  //echo "file is ".$source.$file;
		  rename($source.$file,$destination.$file);
		}
		
		rename($destination.$_POST['main-image'],$target_dir .$source.$_POST['main-image']);
	}
}


//now if there are delete items in the delete array delete these images
if ($_POST['delimages']){
	$post = True;
	foreach($_POST['delimages'] as $delete){
		//echo "*************".$delete;
		//then delete the previous mainimage
		try{
			$fileToDelete = $target_dir .$title.'/images/'.$delete;
			unlink($fileToDelete); 
			
			//then delete them from the database
			$query = "DELETE FROM articleimages WHERE name=:name ";
			
			//Update query
			$query_params = array(
				':name' => $delete
			);
			
			try {
				$stmt   = $db->prepare($query);
				$result = $stmt->execute($query_params);
			
			}
			catch (PDOException $ex) {
				//echo "file could not be removed from database";
			}
			
		}catch(Exception $e)
		{
			//echo '********************error in deleting '.$e;
		}
	}
}
	
function picInfo($name,$article,$db) { //function to set empty fields of the picture information

	$query = "INSERT INTO articleimages (name,article) VALUES ( :name,:article) ";

	//Update query
	$query_params = array(
		':name' => $name,
		':article' => $article
	);
	
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	
	}
	catch (PDOException $ex) {
		// For testing, you could use a die and message. 
		//die("Failed to run query: " . $ex->getMessage());
		
		//or just use this use this one:
		$response["success"] = 0;
		$response["message"] = "Sorry, there was an error uploading article image values".$ex;
		die(json_encode($response));
	}

}

$pics = array_filter($_POST["imagetext"]);

if (!empty($pics)) {
	$post = True;
	
	//print_r($_POST['imagetext']);
	foreach ($_POST['imagetext'] as $key => $value) {
	
		// Sanitize and validate the data passed in
		$_POST['imagetext'] = filter_input(INPUT_POST, 'imagetext', FILTER_SANITIZE_STRING);
	
		$query = "UPDATE articleimages SET tag = :tag WHERE article = :title AND name = :name"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

		//Update query
		$query_params = array(
			':title' => $title,
			':name' => $key,  			//get the key of element
			':tag' => $value
 		);
	  
		//execute query
		try {
			$stmt   = $db->prepare($query);
			$result = $stmt->execute($query_params);
		}
		catch (PDOException $ex) {
			// For testing, you could use a die and message. 
			//die("Failed to run query: " . $ex->getMessage());
			
			//or just use this use this one:
			//$response["success"] = 0;
			//$response["message"] = "Sorry, there was an error posting the introduction";
			//die(json_encode($response));
			echo $ex;
		}
		
			
	}
	//echo $_POST['imagetext'];

}



if ($_POST['introduction'] != '') {
	
	// Sanitize and validate the data passed in
    $_POST['introduction'] = filter_input(INPUT_POST, 'introduction', FILTER_SANITIZE_STRING);
	
	$post = True;
	
	$query = "UPDATE newsarticles SET articletag = :introduction WHERE articlename = :title"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':introduction' => $_POST['introduction'],
		':title' => $title  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry, there was an error posting the introduction".$ex;
        die(json_encode($response));
    }
	
}

if ($_POST['category'] != '') {
	
	// Sanitize and validate the data passed in
    $_POST['category'] = filter_input(INPUT_POST, 'category', FILTER_SANITIZE_STRING);
	
	$post = True;
	
	$query = "UPDATE newsarticles SET articlecategory = :category WHERE articlename = :title"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':title' => $title,
		':category' => $_POST['category']  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry, there was an error uploading category".$ex;
        die(json_encode($response));
    }
	
}

if ($_POST['about'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
	$_POST['about'] = $purifier->purify($_POST['about']);
	
	$query = "UPDATE newsarticles SET article = :about WHERE articlename = :title"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':title' => $title,
		':about' => $_POST['about']  			//in the mean time. but we will use session username later
        
    );
  
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry, there was an error uploading 'about' article.".$ex;
        die(json_encode($response));
    }
	
}


if ($_POST['datepicker'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['datepicker'] = filter_input(INPUT_POST, 'datepicker', FILTER_SANITIZE_STRING);
	
	//echo 'put date';
	$query = "UPDATE newsarticles SET submitteddate = :datepicker WHERE articlename = :title"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':title' => $title,
		':datepicker' => $_POST['datepicker']  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry, there was an error uploading date of end".$ex;
        die(json_encode($response));
    }
	
}

if ($_POST['link1'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['link1'] = filter_input(INPUT_POST, 'link1', FILTER_SANITIZE_STRING);
	
	
	$query = "UPDATE newsarticles SET link1 = :link1 WHERE articlename = :title"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':title' => $title,
		':link1' => $_POST['link1']  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
       $response["message"] = "Sorry, there was an error uploading link1.".$ex;
        die(json_encode($response));
    }
	
}

if ($_POST['link2'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['link2'] = filter_input(INPUT_POST, 'link2', FILTER_SANITIZE_STRING);
	
	
	$query = "UPDATE newsarticles SET link2 = :link2 WHERE articlename = :title"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':title' => $title,
		':link2' => $_POST['link2']  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
       $response["message"] = "Sorry, there was an error uploading link2.";
        die(json_encode($response));
    }
	
}

if ($_POST['link3'] != '') {
	$post = True;
	
	// Sanitize and validate the data passed in
    $_POST['link3'] = filter_input(INPUT_POST, 'link3', FILTER_SANITIZE_STRING);
	
	
	$query = "UPDATE newsarticles SET link3 = :link3 WHERE articlename = :title"; //"INSERT INTO members ( profile ) VALUES (:profile) WHERE username = :username ";

    //Update query
    $query_params = array(
        ':title' => $title,
		':link3' => $_POST['link3']  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
       $response["message"] = "Sorry, there was an error uploading link3.".$ex;
        die(json_encode($response));
    }
	
}


if ($post){
	// echoing JSON response
	echo json_encode($response);
}

?>