<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

if (!empty($_POST))
{
	
	//initial query
	$query = "SELECT * FROM members WHERE username =:username";
	
	$query_params = array(
        ':username' => $_POST['username']
    );

	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "database error plus: ".$ex->getMessage();
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
	
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["imagetag"]   = $rows[0]["imagetag"];
		$response["profile"]   = $rows[0]["profile"];
		$response["twitter"]   = $rows[0]["twitter"];
		$response["facebook"]   = $rows[0]["facebook"];
		$response["website"]   = $rows[0]["website"];
		$response["username"]   = $rows[0]["username"];
		$response["email"]   = $rows[0]["email"];
		$response["addr1"]   = $rows[0]["addr1"];
		$response["addr2"]   = $rows[0]["addr2"];
		$response["city"]   = $rows[0]["city"];
		$response["region"]   = $rows[0]["region"];
		$response["postalcode"]   = $rows[0]["postalcode"];
		$response["country"]   = $rows[0]["country"];
		

		
		// echoing JSON response
		echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available! this is it";
		die(json_encode($response));
	}

}

?>
