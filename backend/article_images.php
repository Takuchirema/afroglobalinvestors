<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

if (!empty($_POST))
{	
	// Sanitize and validate the data passed in
    $_POST['articlename'] = filter_input(INPUT_POST, 'articlename', FILTER_SANITIZE_STRING);
	
	//initial query
	$query = "Select * FROM articleimages WHERE article=:articlename";
	
	$query_params = array(
        ':articlename' => $_POST['articlename']
    );

	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "database error plus: ".$ex->getMessage();
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();
	
	
	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["images"]   = array();
		
		foreach ($rows as $row) {
			//$images             = array();
			//$images[$row["name"]]  = $row["tag"];
			$response['images'][$row["name"]] = $row["tag"];
 			
			//array_push($response["images"], $images);
		}
		
		// echoing JSON response
		echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}

}

?>
