<?php
	require("./includes/config.inc.php");
	
	$query = "SELECT password, salt FROM members WHERE username = :username"; 

    //Update query
    $query_params = array(
		':username' => $_POST['username']  			//in the mean time. but we will use session username later
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = 'invalid username';
        die(json_encode($response));
    }
	
	//fetching all the rows from the query
    $row = $stmt->fetch();
	
	$salt = $row['salt'];
	// hash the password with the unique salt.
    $password = hash('sha512', $_POST['password'].$salt);
	
	if ($row['password'] == $password)
	{
		$response["success"] = 1;
        $response["message"] = 'okay';
        die(json_encode($response));
	} else
	{
		$response["success"] = 0;
        $response["message"] = 'wrong password '.$password." ".$row['password'];
        die(json_encode($response));
	}

?>
