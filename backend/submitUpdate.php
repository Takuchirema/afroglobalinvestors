<?php

/*

*/

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

if ($_POST['update'] != '') {
	
	// Sanitize and validate the data passed in
    $_POST['update'] = filter_input(INPUT_POST, 'update', FILTER_SANITIZE_STRING);
	
	$query = "INSERT INTO projectupdates (projectname,projectleader,date,updatevalue) VALUES (:projectname,:projectleader,:date,:updatevalue)";

    //Update query
    $query_params = array(
        ':projectname' => $_POST['projectname'],
		':projectleader' => $_POST['projectleader'],
		':date' => $_POST['date'],
		':updatevalue' => $_POST['update']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occurred while submitting the update. Please resubmit it";
        die(json_encode($response));
    }
	
	$response["success"] = 1;
	$response["message"] = "Your update has been successfully posted";
	echo json_encode($response);
	
}

?>