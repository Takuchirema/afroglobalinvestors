<?php
	require("./includes/config.inc.php");
	
	$query = "SELECT * FROM members WHERE email = :email"; 

    //Update query
    $query_params = array(
		':email' => $_POST['email']  			
        
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = 'An error occurred validating the email. Pleas try again';
        die(json_encode($response));
    }
	
	//fetching all the rows from the query
    $rows = $stmt->fetch();
	
	if ($rows)
	{
		$response["success"] = 1;
        $response["message"] = 'Email is available';
		$response["username"] = $rows["username"];
		/* foreach ($rows as $row) {
			$response["username"] = $row["username"];
		} */
		
        die(json_encode($response));
	} else
	{
		$response["success"] = 0;
        $response["message"] = 'The email address provided has not been found. Please check and try again';
        die(json_encode($response));
	}

?>
