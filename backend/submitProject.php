<?php

/*
*/

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

if ($_POST['project'] != '') {
	
	
	$query = "SELECT * FROM projects WHERE projectname= :projectname";

    //Update query
    $query_params = array(
        ':projectname' => $_POST['project']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occured submitting your project";
        die(json_encode($response));
    }
	
	//check to see that all has been set as it is supposed to be
	$rows = $stmt->fetch();
    foreach ($rows as $key=>$val ) {
		if ( ($val == "" || $val =="0000-00-00") && $key == "projectabout"){
		
			$response["success"] = 0;
			$response["message"] = "Please fill in and SAVE the 'Tell us more about your project' section on 'Project details' page before submitting your project for visitors to see";
			die(json_encode($response));

		}else if ( ($val == "" || $val =="0000-00-00") && $key == "projectcategory"){
		
			$response["success"] = 0;
			$response["message"] = "Please fill in and SAVE the 'Select Project category' section on 'Project Basics' page before submitting your project for visitors to see";
			die(json_encode($response));

		}else if ( ($val == "" || $val =="0000-00-00") && ($key == "projectregion" || $key == "projectcountry") ){
		
			$response["success"] = 0;
			$response["message"] = "Please fill in and SAVE the 'Project Location' section on 'Project Basics' page before submitting your project for visitors to see";
			die(json_encode($response));

		}else if ( ($val == "" || $val =="0000-00-00") && $key == "fundingdetails"){
		
			$response["success"] = 0;
			$response["message"] = "Please fill in and SAVE the 'Funding Details' section on 'Project Funding' page before submitting your project for visitors to see";
			die(json_encode($response));

		}else if ( ($val == "" || $val =="0" || $val ==0) && $key == "fundinggoal"){
		
			$response["success"] = 0;
			$response["message"] = "Please fill in and SAVE the 'Funding Goal' section on 'Project Funding' page before submitting your project for visitors to seet";
			die(json_encode($response));

		}else if ( ($val == "" || $val =="0000-00-00") && $key == "projecttag"){
		
			$response["success"] = 0;
			$response["message"] = "Please fill in and SAVE the 'Introductory phrase' section on 'Project Basics' page before submitting your project for visitors to see";
			die(json_encode($response));

		}else if ( ($val == "" || $val =="0000-00-00") && $key == "date"){
		
			$response["success"] = 0;
			$response["message"] = "Please fill in and SAVE the 'Funding Duration' section on 'Project Funding' page before submitting your project for visitors to see";
			die(json_encode($response));

		}

		
		if ($key == "date"){ //check to see if the dates are corresponding
			$subdate=strtotime($rows["submitteddate"]);
			$enddate=strtotime($val);
			
			if($subdate > $enddate)
			{
				$response["success"] = 0;
				$response["message"] = "The project end date cannot be before submission / start date";
				die(json_encode($response));
			}
		
		}
    }
	
	//if there are no unfilled fields mark the project as submitted
	//submitted has 3 values; yes, no and expired.
	$query = "Update projects SET submitted = :submitted,submitteddate=:date WHERE projectname= :projectname";

    //Update query
    $query_params = array(
        ':projectname' => $_POST['project'],
		':submitted' => 'yes',
		':date' => $_POST['submitdate']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
		
		$response["success"] = 1;
        $response["message"] = "Project Successfully submitted";
        die(json_encode($response));
		
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occured submitting your project";
        die(json_encode($response));
    }
	
}

?>