<?php

/*
*/

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

if ($_POST['article'] != '') {
	
	
	$query = "SELECT * FROM newsarticles WHERE articlename= :articlename";

    //Update query
    $query_params = array(
        ':articlename' => $_POST['article']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occured submitting your article";
        die(json_encode($response));
    }
	
	//check to see that all has been set as it is supposed to be
	$rows = $stmt->fetch();
	
    foreach ($rows as $key=>$val ) {
		if  (($val == "") && ($key == "article")) {
			$response["success"] = 0;
			$response["message"] = "Please provide content for your article before submitting";
			die(json_encode($response));
		}
	
    }
	
	//if there are no unfilled fields mark the article as submitted
	//submitted has 3 values; yes, no and expired.
	$query = "Update newsarticles SET submitted = :submitted,submitteddate=:date WHERE articlename= :articlename";

    //Update query
    $query_params = array(
        ':articlename' => $_POST['article'],
		':submitted' => 'yes',
		':date' => $_POST['submitdate']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
		
		$response["success"] = 1;
        $response["message"] = "article Successfully submitted";
        die(json_encode($response));
		
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occured submitting your article";
        die(json_encode($response));
    }
	
}

?>