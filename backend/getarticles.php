<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

$query ="";

$response = array();

if ((isset($_POST['submitted']) && $_POST['submitted'] =="yes") || isset($_POST['approved']) ) {
	//order by date submitted 
	if (isset($_POST['approved']) && $_POST['approved'] =="yes"){
	
		$query = "Select * FROM newsarticles WHERE submitted = 'yes' AND approved = 'yes' ORDER BY submitteddate DESC";
		
	}else if (isset($_POST['approved']) && $_POST['approved'] =="no") {
		$query = "Select * FROM newsarticles WHERE submitted = 'yes' AND approved = 'no' ORDER BY submitteddate DESC";
	
	}else {
		$query = "Select * FROM newsarticles WHERE submitted = 'yes' ORDER BY submitteddate DESC";
	}
	
	
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error!";
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["articles"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["articles"][] = $row["articlename"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}

} 

if (isset($_POST['approved']) && $_POST['approved'] =="yes") {
	//order by date submitted 
	$query = "Select * FROM newsarticles WHERE approved = 'yes' ORDER BY submitteddate DESC";
	
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error!";
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["articles"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["articles"][] = $row["articlename"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}

} 


if (isset($_POST['all'])) {
	
	//order by date submitted 
	$query = "Select * FROM newsarticles ";
	
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error!";
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["articles"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["articles"][] = $row["articlename"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}
}

if (isset($_POST['category']) && $_POST['category'] != "no") {
	
	$query = "Select * FROM newsarticles WHERE articlecategory=:category "; //type is going to be company or article
	
	 $query_params = array(
        ':category' => $_POST['category']
    );
  
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error! ".$_POST['iscompany']."  ".$ex;
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["category"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["category"][] = $row["articlename"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}
}

echo json_encode($response);


?>
