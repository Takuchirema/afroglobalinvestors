<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

$query ="";

$response = array();

if (isset($_POST['submitted']) && $_POST['submitted'] =="yes") {
	//order by date submitted 
	$query = "Select * FROM projects WHERE submitted = 'yes' ORDER BY submitteddate DESC";
	
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error!";
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["projects"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["projects"][] = $row["projectname"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}

} 

if (isset($_POST['companies']) && $_POST['companies'] =="yes") {
	
	//order by date submitted 
	$query = "Select * FROM projects WHERE company = 'yes' ORDER BY submitteddate DESC";
	
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error!";
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["companies"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["companies"][] = $row["projectname"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}
}

if (isset($_POST['all'])) {
	
	//order by date submitted 
	$query = "Select * FROM projects ";
	
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute();
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error!";
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["projects"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["projects"][] = $row["projectname"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}
}

if (isset($_POST['category']) && $_POST['category'] != "no" && $_POST['category'] != "yes") {
	
	$query = "Select * FROM projects WHERE projectcategory=:category AND company=:iscompany"; //type is going to be company or project
	
	 $query_params = array(
        ':category' => $_POST['category'],
		':iscompany' => $_POST['iscompany']
    );
  
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error! ".$_POST['iscompany']."  ".$ex;
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["category"]   = array();
		//echo "rows in";
		foreach ($rows as $row) {
			$response["category"][] = $row["projectname"];
		}
		
		// echoing JSON response
		//echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Post Available!";
		die(json_encode($response));
	}
}

echo json_encode($response);


?>
