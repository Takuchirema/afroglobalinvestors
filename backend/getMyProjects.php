<?php

/*
Our "config.inc.php" file connects to database every time we include or require
it within a php script.  Since we want this script to add a new user to our db,
we will be talking with our database, and therefore,
let's require the connection to happen:
*/
require("./includes/config.inc.php");

if ($_POST['username'] != ''){
	//initial query
	if (isset($_POST['submitted']) && $_POST['submitted'] == "no" ){
		$query = "Select * FROM projects WHERE projectleader=:username";
	}else if (isset($_POST['submitted']) && $_POST['submitted'] == "yes" ){
		$query = "Select * FROM projects WHERE projectleader=:username AND submitted='yes'";
	} else{
		$query = "Select * FROM projects WHERE projectleader=:username";
	}

	//Update query
    $query_params = array(
		':username' => $_POST['username'] 			//in the mean time. but we will use session username later
        
    );
	//execute query
	try {
		$stmt   = $db->prepare($query);
		$result = $stmt->execute($query_params);
	}
	catch (PDOException $ex) {
		$response["success"] = 0;
		$response["message"] = "Database Error! ".$ex;
		die(json_encode($response));
	}

	// Finally, we can retrieve all of the found rows into an array using fetchAll 
	$rows = $stmt->fetchAll();


	if ($rows) {
		$response["success"] = 1;
		$response["message"] = "Post Available!";
		$response["projects"]   = array();
		
		foreach ($rows as $row) {
			$project = array();
			$project["projecttag"][] = $row["projecttag"];
			$project["projectname"][] = $row["projectname"];
			$project["date"][] = $row["date"];
			$project["company"][] = $row["company"];
			
			array_push($response["projects"], $project);
			
		}
		
		// echoing JSON response
		echo json_encode($response);
		
		
	} else {
		$response["success"] = 0;
		$response["message"] = "No Projects to show";
		die(json_encode($response));
	}

}

?>
