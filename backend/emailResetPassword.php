<?php 
require("./includes/config.inc.php");

$response = array();

$myemail = $_POST['email'];//<-----Put Your email address here.

$name = 'Afroglobalinvestors';

$message = '

Dear '.$_POST['username'].',

Please use the following new password to access your account: '.$_POST['newpassword'].' 

You will be able to change it once you are logged in. 

If you continue to face difficulties please contact us on support@Afroglobalinvestors.com .

Thank you.

Afroglobalinvestors team';


$to = $myemail;

$email_subject = "Reset password";

$email_body = "You have received a new message. ".
" Here are the details:".
"Message \n $message";

$headers = "From: Afroglobalinvestors ";

$mail = mail($to,$email_subject,$email_body,$headers);
//redirect to the 'thank you' page

if($mail){ //change the password of the user
	
	$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
		
	//$random_salt=intval($random_salt);
	// Create salted password 
	//$password = crypt($password,"$6$".$random_salt);
	$password = hash('sha512', $_POST['p'] . $random_salt);
	
	$query = "UPDATE members SET password =:newpassword , salt=:salt WHERE email = :email"; 

    //Update query
    $query_params = array(
		':email' => $_POST['email'], 			//in the mean time. but we will use session username later
        ':newpassword' => $password,
		':salt' =>$random_salt
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
		
		$response['success'] = 1;
		$response['message'] = "Please check your email address for further instructions.";
		echo json_encode($response);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = 'Please check your email address for further instructions.';
        die(json_encode($response));
    }
	
	
	
}else{
	$response['success'] = 0;
	$response['message'] = "Sorry your password could not be changed due to network error. Please try again";
	echo json_encode($response);
}

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}


?>