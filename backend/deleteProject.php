<?php

/*
*/

include_once './includes/db_connect.php';
include_once './includes/config.inc.php';

$response = array();

if ($_POST['project'] != '') {
	
	
	$query = "DELETE FROM projects WHERE projectname= :projectname";

    //Update query
    $query_params = array(
        ':projectname' => $_POST['project']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occurred while trying to delete the project".$ex;
        die(json_encode($response));
    }
	
	$query = "DELETE FROM projectimages WHERE project= :projectname";

    //Update query
    $query_params = array(
        ':projectname' => $_POST['project']
    );
  
	//execute query
    try {
        $stmt   = $db->prepare($query);
        $result = $stmt->execute($query_params);
    }
    catch (PDOException $ex) {
        // For testing, you could use a die and message. 
        //die("Failed to run query: " . $ex->getMessage());
        
        //or just use this use this one:
        $response["success"] = 0;
        $response["message"] = "Sorry an error occurred while trying to delete the project images".$ex;
        die(json_encode($response));
    }
	
	//then the entire folder for the project
	try{
		$dirPath = "../projects/".$_POST['project'];
		
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
				$path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
		}
		rmdir($dirPath);
		
	}catch(Exception $e)
	{
		//echo '********************error in deleting '.$e;
		$response["success"] = 0;
        $response["message"] = "Sorry an error occurred while trying to delete the project images".$e;
        die(json_encode($response));
	}
	
	$response["success"] = 1;
	$response["message"] = "Project has been successfully deleted";
	echo json_encode($response);
	
}

?>